-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN LINKEDGEODATA SPARQL
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table if exists linkedgeodata_sparql;
create table linkedgeodata_sparql (
    nome text default null, -- nome not name
    nodetype varchar(100),
    typevalue varchar(100),
    ontologylink text,
    city varchar(100),
    street varchar(100),
    housenumber varchar(100),
    country varchar(100),
    wkt4326 text
);
copy linkedgeodata_sparql from 'C:\trajectory\linkedgeodata_data.tsv' delimiter '\t' csv; -- DOCH! you can't use \t, use a tab instead!
alter table linkedgeodata_sparql add column geom geometry(Geometry,4326);
update linkedgeodata_sparql set geom = ST_GeomFromText(wkt4326, 4326);
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END LINKEDGEODATA SPARQL
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN AREE STATISTICHE
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table if exists aree_statistiche;
create table aree_statistiche (
  areaid integer primary key,
  nome varchar(50), -- nome not name
  ncontribuenti integer,
  imponibileirpef double precision,
  imponibileaddizionaleirpef double precision,
  imponibileirpefprocapite double precision,
  imponibileirpefdesc integer,
  wkt4326 text,
  geojson text
);
copy aree_statistiche from 'C:\trajectory\aree_statistiche_UTF8_LF.csv' with header encoding 'utf8' delimiter';' csv;
create index aree_statistiche_gix on aree_statistiche using gist(geom);
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END AREE STATISTICHE
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- DATABASE CREATION / ALT 1
-- Uncomment this section to populate alldatafiltered
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- copy (select * from alldata where accuracy >= '0' and accuracy < '999999') to 'C:\trajectory\alldata_cleaned_UTF8_CRLF.csv' csv encoding 'utf8' delimiter ';';
-- drop table if exists alldata;
-- create table alldata (
--     timest integer,
--     cuebiqid char(64),
--     param1 char(3),
--     latitude double precision,
--     longitude double precision,
--     -- accuracy char(7),
--     accuracy integer,
--     datetime char(20),
--     frequencycount integer,
--     frequencypercent double precision,
--     deviceid integer
-- );
-- populate table from csv
-- data.csv contains noisy accuracy (e.g. "\N" or negative values) as string
-- copy alldata from 'C:\trajectory\data.csv' with header delimiter ';' csv; --75,000,000 tuples
-- we need to filter noise, as we have multiple pings with same deviceid, timestamp and accuracy
-- select best accuracy for the couple (deviceid, timest)
-- 10 min
-- drop table if exists temp_bestaccuracy;
-- create table temp_bestaccuracy as (
--      select deviceid, timest, min(accuracy) accuracy
--      from alldata
--      group by deviceid, timest
-- );
-- alter table temp_bestaccuracy add primary key (deviceid, timest);
-- select ctid corresponding to (deviceid, timest, best accuracy)
-- 15 min
-- drop table if exists temp_bestaccuracyctid;
-- create table temp_bestaccuracyctid as (
--      select min(ad.ctid) rownumber
--      from alldata ad, temp_bestaccuracy x
--      where  x.deviceid = ad.deviceid and x.timest = ad.timest and x.accuracy = ad.accuracy
--      group by x.deviceid, x.timest
-- );
-- alter table temp_bestaccuracyctid add primary key (rownumber);
-- filter data 15 min
-- drop table if exists alldatafiltered;
-- create table alldatafiltered as (
--     select ad.timest, ad.deviceid, ad.latitude, ad.longitude, ad.accuracy,
--       ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326) as geom,
--       ST_AsText(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as wkt4326,
--       ST_AsGeojson(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as geojson
--     from alldata ad, temp_bestaccuracyctid x
--     where ad.ctid = x.rownumber
-- );
-- alter table alldatafiltered add primary key (deviceid, timest);
-- copy alldatafiltered to 'C:\trajectory\alldatafiltered_UTF8_CRLF.csv' with header delimiter ';' csv;
-- drop table if exists temp_bestaccuracy;
-- drop table if exists temp_bestaccuracyctid;

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- DATABASE CREATION / END ALT 1 / BEGIN ALT 2
-- Uncomment this section to load alldatafiltered from csv
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table if exists alldatafiltered;
create table alldatafiltered (
    timest integer,
    deviceid integer,
    latitude double precision,
    longitude double precision,
    accuracy integer,
    geom geometry(Point, 4326),
    wkt4326 varchar(100),
    geojson varchar(100),
    primary key (deviceid, timest)
);
copy alldatafiltered from 'C:\trajectory\alldatafiltered_UTF8_CRLF.csv' with header delimiter ';' csv;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- DATABASE CREATION / END ALT 2
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

alter table alldatafiltered
    add column significativeid integer,
    add column points integer,
    add column distinctpoints integer;

-- 25min
with sd as (
    select deviceid, row_number() over (order by points desc) rowid, points, distinctpoints
    from (
        select deviceid, count (*) as points, count(distinct (latitude::varchar || longitude::varchar)) as distinctpoints
        from alldatafiltered
        group by deviceid
    ) significative_devices
) update alldatafiltered set
     significativeid = sd.rowid,
     points = sd.points,
     distinctpoints = sd.distinctpoints
     from sd
     where sd.deviceid = alldatafiltered.deviceid;
create index alldatafiltered_significativeid_idx on alldatafiltered (significativeid);
cluster alldatafiltered using alldatafiltered_significativeid_idx;
select * from alldatafiltered limit 1;
-- create significative data, a table where only the top 10 people are added
drop table if exists  significativedata;
create table significativedata as (
  select *
  from alldatafiltered
  where significativeid < 11
  order by significativeid asc, timest asc
);
alter table significativedata add primary key (significativeid, timest);
select min(significativeid), min(deviceid), min(points), min(distinctpoints) from significativedata group by significativeid order by significativeid;

-- DEBUG PURPOSE! THESE TABLES SHOULD BE AUTOMATICALLY GENERATED
-- create table for user profiles
drop table if exists entity cascade;
create table entity (
    entityid integer primary key,
    edescription text
);

drop table if exists context;
create table context (
    contextid serial primary key,
    entityid integer references entity,
    whenuri text,
    whereuri text,
    unique (entityid, whenuri, whereuri)
);

drop table if exists features;
create table features (
    featureid serial primary key,
    fname varchar(100) not null,
    fvalue text not null,
    contextid integer references context,
    accuracy double precision default 1
);

-- DEBUG PURPOSE! THESE TABLES SHOULD BE AUTOMATICALLY GENERATED
insert into entity values (1, 'generated_user1');
insert into entity values (4, 'generated_user4');

insert into context(entityid, whenuri, whereuri) values (1, 'always', 'everywhere');
insert into context(entityid, whenuri, whereuri) values (4, 'always', 'everywhere');

insert into features(fname, fvalue, contextid) values ('livein', ST_PointFromText('POINT(11.372449 44.505005)', 4326), 1);
insert into features(fname, fvalue, contextid) values ('workin', ST_PointFromText('POINT(11.333137 44.499220)', 4326), 1);
insert into features(fname, fvalue, contextid) values ('workin', ST_PointFromText('POINT(11.310307 44.503271)', 4326), 2);
insert into features(fname, fvalue, contextid) values ('livein', ST_PointFromText('POINT(11.345219 44.491498)', 4326), 2);
insert into features(fname, fvalue, contextid) values ('livein', 'uriToEnrichedStayPoint', 2);

-- DEBUG PURPOSE: THIS TABLE IS GENERATE BY NICOLA
drop table if exists  temporarypoints;
create table temporarypoints as
    select significativeid, st_setsrid(min(geom), 4326) as geom, 'livein' as fname
    from significative_enriched_data group by significativeid;
insert into temporarypoints values (11, st_setsrid(st_makepoint(10, 10), 4326), 'livein');
insert into temporarypoints values (1, st_setsrid(ST_AsText('0101000020E61000000007488F3A8D26401A6F2BBD36424640'), 4326), 'workin');
-- END DEBUG PURPOSE: THIS TABLE IS GENERATE BY NICOLA

-- Enrich with data from OSM at first
drop table if exists staypoints_enriched_tmp;
create table staypoints_enriched_tmp as
  select ot.significativeid, ot.geom, osmid, osmgeom, rank, fname, areaid, cityid
  from temporarypoints ot left outer join (
    select *
    from (
      select significativeid, geom, way as osmgeom, osm_id as osmid, st_distance(geom, way) * 111195 as distance,
          rank() over (partition by significativeid, geom order by st_distance(geom, way) * 111195 asc) as rank,
          info.areaid, info.cityid
      from  temporarypoints t, planet_osm_point osm, islocatedin_point info
      where (amenity != '' or shop != '' or housenumber != '')
              and st_distance(geom, way) * 111195 < 50 and osm_id = info.osmid -- change accuracy here! (50 atm)
      order by significativeid asc, geom asc, distance asc
    ) x
    where rank < 4
  ) y on ot.geom = y.geom and ot.significativeid = y.significativeid;
-- Enrich points which are not close to any OSM node
drop table if exists staypoints_enriched;
create table staypoints_enriched as
  select x.*, c.gid as cityid
  from  (
      select significativeid, s.geom, osmid, osmgeom, 1 as rank, fname, a.areaid
      from staypoints_enriched_tmp s left outer join aree_statistiche a on st_contains(a.geom, s.geom)
    ) x left outer join com2016_wgs84 c
  on st_contains(c.geom, x.geom);
drop table if exists staypoints_enriched_tmp;
select * from staypoints_enriched;

insert into features(fname, fvalue, contextid)
  select 'averageincome', imponibileirpefprocapite, contextid
  from features, aree_statistiche
  where fname = 'livein' and ST_Contains(aree_statistiche.geom, fvalue);

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- LOAD BIG DATA
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- drop table if exists bigdata;
-- create table bigdata (
--     deviceid varchar(10) default ('none'),
--     timest timestamp,
--     latitude double precision,
--     longitude double precision,
--     accuracy integer default (20)
-- );
-- drop table if exists tmp;
-- create table tmp (
--     timest timestamp,
--     latitude double precision,
--     longitude double precision
-- );
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\mf_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'mf' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\eg_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'eg' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\mg_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'mg' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\sr_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'sr' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\nn_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'nn' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\ac_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'ac' where deviceid = 'none';
--
-- delete from tmp;
-- copy tmp from 'C:\trajectory\at_location.csv' with header delimiter ',' csv;
-- insert into bigdata(timest, latitude, longitude) select * from tmp group by timest, latitude, longitude;
-- update bigdata set deviceid = 'at' where deviceid = 'none';
--
-- -- alter table bigdata add primary key (deviceid, timest);
-- drop table tmp;
-- select deviceid, count(*) from bigdata group by deviceid;
--
-- drop table if exists temp_bestaccuracy;
-- create table temp_bestaccuracy as (
--      select deviceid, timest, min(accuracy) accuracy
--      from bigdata
--      group by deviceid, timest
-- );
-- alter table temp_bestaccuracy add primary key (deviceid, timest);
--
-- drop table if exists temp_bestaccuracyctid;
-- create table temp_bestaccuracyctid as (
--      select min(ad.ctid) rownumber
--      from bigdata ad, temp_bestaccuracy x
--      where  x.deviceid = ad.deviceid and x.timest = ad.timest and x.accuracy = ad.accuracy
--      group by x.deviceid, x.timest
-- );
--
-- drop table if exists bigdatafiltered;
-- create table bigdatafiltered as (
--     select ad.timest, ad.deviceid, ad.latitude, ad.longitude, ad.accuracy,
--        ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326) as geom,
--        ST_AsText(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as wkt4326,
--        ST_AsGeojson(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as geojson
--     from bigdata ad, temp_bestaccuracyctid x
--     where ad.ctid = x.rownumber
-- );
-- drop table if exists temp_bestaccuracy;
-- drop table if exists temp_bestaccuracyctid;
-- alter table bigdatafiltered add primary key (deviceid, timest);
--
-- copy bigdatafiltered to 'C:\trajectory\bigdatafiltered_UTF8_CRLF.csv' with header encoding 'utf8' delimiter ';' csv;
--
--
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- DATABASE CREATION / END ALT 1 / BEGIN ALT 2
-- Uncomment this section to load alldatafiltered from csv
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table if exists bigdatafiltered;
create table bigdatafiltered (
    timest timestamp,
    deviceid integer,
    latitude double precision,
    longitude double precision,
    accuracy integer,
    geom geometry(Point, 4326),
    wkt4326 varchar(100),
    geojson varchar(100),
    primary key (deviceid, timest)
);
copy bigatafiltered from 'C:\trajectory\bigdatafiltered_UTF8_CRLF.csv' with header delimiter ';' csv;

alter table bigdatafiltered
    add column significativeid integer,
    add column points integer,
    add column distinctpoints integer;

with sd as (
    select deviceid, row_number() over (order by points desc) rowid, points, distinctpoints
    from (
        select deviceid, count (*) as points, count(distinct (latitude::varchar || longitude::varchar)) as distinctpoints
        from bigdatafiltered
        group by deviceid
    ) significative_devices
) update bigdatafiltered set
     significativeid = sd.rowid,
     points = sd.points,
     distinctpoints = sd.distinctpoints
     from sd
     where sd.deviceid = bigdatafiltered.deviceid;

drop table if exists bigsignificativedata;
create table bigsignificativedata as (
  select extract(epoch from timest)::integer as timest, deviceid, latitude, longitude, accuracy, geom, wkt4326, geojson, significativeid, points, distinctpoints
  from bigdatafiltered
);
alter table bigsignificativedata add primary key (significativeid, timest);

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- Site tables creation
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table if exists site_credentials_table cascade;
create table site_credentials_table (
   userid bigserial,
   username varchar(20),
   passcode varchar(50),
 	active boolean,
   primary key (username)
);

-- CHANGE THE DEFAULT PASSWORD BELOW
insert into  site_credentials_table (username, passcode, active) values ('trajectoryweb', 'defaultpassword', True);
drop table if exists site_filter_order cascade;
create table site_filter_order (
    orderid bigserial primary key,
    positionnum int,
    positionkey varchar,
    username varchar,
    groupid varchar,
	   foreign key (username) references  site_credentials_table(username),
    unique (positionkey, positionnum, username, groupid)
);

drop table if exists site_query_history cascade;
create table site_query_history (
    queryid bigserial primary key,
    positionnum int,
    title varchar,
    queryparameters varchar,
    username varchar,
	 foreign key (username) references  site_credentials_table(username)
);

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- CREATE HIERARCHIES POINT -> STAT AREA -> COMUNE
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
alter table com2016_wgs84 add column uri text, add column geojson text;
update com2016_wgs84 set uri = 'http://dbpedia.org/resource/Bologna' where comune = 'Bologna';
update com2016_wgs84 set uri = 'http://dbpedia.org/resource/Forlì' where comune = 'Forlì';
update com2016_wgs84 set uri = 'http://dbpedia.org/resource/Cesena' where comune = 'Cesena';
alter table planet_osm_point add primary key (osm_id);
alter table planet_osm_point add column geojson text;
update planet_osm_point set geojson = st_asgeojson(way);
update com2016_wgs84 set geojson = st_asgeojson(geom);
create index planet_osm_point_gix on planet_osm_point using gist(way);
create index com2016_wgs84_gix on com2016_wgs84 using gist(geom);
-- copy planet_osm_point to 'C:\trajectory\planet_OSM_point_CRLF_UTF8.csv' with header delimiter ';' encoding 'utf8' csv;
-- copy com2016_wgs84 to 'C:\trajectory\com2016_wgs84_CRLF_UTF8.csv' with header delimiter ';' encoding 'utf8' csv;

drop table if exists islocatedin_statarea2comune;
create table islocatedin_statarea2comune (
   comuneid bigint references com2016_wgs84,
   areaid int references aree_statistiche,
   primary key (comuneid, areaid)
);
insert into islocatedin_statarea2comune(areaid, comuneid)
    select a.areaid, c.gid as comuneid from com2016_wgs84 c, aree_statistiche a where st_intersects(a.geom, c.geom);

drop table if exists islocatedin_point;
create table islocatedin_point (
   osmid bigint references planet_osm_point primary key,
   areaid int references aree_statistiche,
   cityid int references com2016_wgs84
);
insert into islocatedin_point
  select osmid, areaid, cityid
  from (
    select x.*, gid as cityid
    from (
      select osm_id as osmid, areaid, way from planet_osm_point osm, aree_statistiche a where st_contains(a.geom, way)
    ) x, com2016_wgs84 c
    where st_contains(c.geom, way)
  ) y;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN - CREATE HIERARCHIES POINT
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- EXPORT USEFUL DATA WITHOUT GEOM (ONLY WITH GEOJSON)
alter table planet_osm_point add column latitude double precision, add column longitude double precision, add column geojson text;
update planet_osm_point set geojson = st_asgeojson(way), latitude = st_y(way), longitude = st_x(way);
alter table com2016_wgs84 add column geojson text;
update com2016_wgs84 set geojson = st_asgeojson(geom);

create table exportableplanetosm as
    select osm_id, "addr:housename" as housename, "addr:housenumber" as housenumber, "addr:interpolation" as addrinterpolation,
           amenity, building, denomination, leisure, office, place, shop, sport, tourism, latitude, longitude, geojson
           from planet_osm_point
           where "addr:housename" != '' or "addr:housenumber" != '' or "addr:interpolation" != '' or
           amenity != '' or building != '' or denomination != '' or leisure != '' or
           office != '' or place != '' or shop != '' or sport != '' or tourism != '' ;
copy exportableplanetosm to 'C:\trajectory\planet_OSM_point_UTF8_CRLF.csv' with header delimiter ';' encoding 'utf8' csv;
select * from exportableplanetosm;
drop table exportableplanetosm;

create table exportablecom2016_wgs84 as select * from com2016_wgs84;
alter table exportablecom2016_wgs84 drop column geom;
copy exportablecom2016_wgs84 to 'C:\trajectory\com2016_wgs84_UTF8_CRLF.csv' with header delimiter ';' encoding 'utf8' csv;
select * from exportablecom2016_wgs84;
drop table exportablecom2016_wgs84;

create table exportablearee_statistiche as select * from aree_statistiche;
alter table exportablearee_statistiche drop column geom;
copy exportablearee_statistiche to 'C:\trajectory\aree_statistiche_UTF8_CRLF.csv' with header delimiter ';' encoding 'utf8' csv;
select * from exportablearee_statistiche;
drop table exportablearee_statistiche;
