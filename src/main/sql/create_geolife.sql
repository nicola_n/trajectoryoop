drop table if exists geoliferaw;
create table geoliferaw (
    deviceid integer, -- total 182
    latitude double precision,
    longitude double precision,
    uselessfield integer, -- all 0
    altitudefeet double precision,
    uselessdate double precision,
    datestring varchar(10),
    timestdaily varchar(8)
);
copy geoliferaw from 'C:\trajectory\geolife2_gps_data_trajectory_UTF8_LF.csv' with encoding 'utf8' delimiter ',' csv;
drop table if exists geolife;
create table geolife as (
    select extract(epoch from (datestring || ' ' || timestdaily)::timestamp) as timest,
             deviceid, latitude, longitude, altitudefeet, ST_SetSRID(ST_MakePoint(longitude, latitude), 4326) as geom
    from geoliferaw
    group by deviceid, latitude, longitude, uselessfield, altitudefeet, uselessdate, timest
);
select * from geolife limit 10;
alter table geolife add primary key (timest, deviceid);
update geolife set wkt4326 = ST_AsText(geom);
update geolife set geojson = ST_AsGeoJSON(geom);
