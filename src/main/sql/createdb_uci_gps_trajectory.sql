drop table if exists uci_gps_tracks;
create table uci_gps_tracks (
    trackid integer,
    androidid integer,
    speedkmh double precision,
    duration double precision,
    distancekm double precision,
    trafficrating smallint, -- 3- good, 2- normal, 1- bad
    busrating smallint, -- 1- The amount of people inside the bus is little, 2- The bus is not crowded, 3- The bus is crowded
    weatherrating smallint, --  2- sunny, 1- raining
    carorbus smallint, -- 1 - car, 2-bus
    linha text,
    constraint uci_gps_tracks_pkey primary key (trackid)
);
copy uci_gps_tracks from 'C:\trajectory\go_track_tracks.csv' with delimiter ',' header  csv;
drop table if exists uci_gps_trackpoints;
create table uci_gps_trackpoints (
    pointid integer,
    latitude double precision,
    longitude double precision,
    trackid integer,
    timest timestamp,
    constraint uci_gps_trackpoints_pkey primary key (pointid)
);
copy uci_gps_trackpoints from 'C:\trajectory\go_track_trackspoints.csv' with header delimiter ',' csv;
-- This table has been created already
-- alter table uci_gps_trackpoints add column wkt4326 text, add column geom geometry(geometry, 4326), add column geojson text;
-- update uci_gps_trackpoints set geom = ST_SetSRID(ST_MakePoint(longitude, latitude),4326);
-- update uci_gps_trackpoints set wkt4326 = ST_AsText(geom);
-- update uci_gps_trackpoints set geojson = ST_AsGeoJSON(geom);
-- copy (
-- 	select ut.trackid, timest, androidid, latitude, longitude, speedkmh, duration, distancekm, trafficrating, busrating, weatherrating, carorbus, linha
-- 	    from uci_gps_tracks ut, uci_gps_trackpoints up
-- 	    where ut.trackid = up.trackid
-- 	    group by (ut.trackid, timest, androidid, latitude, longitude, speedkmh, duration, distancekm, trafficrating, busrating, weatherrating, carorbus, linha)
-- 	    order by trackid, androidid, timest asc)
--     to 'C:\trajectory\go_track_merged_UTF8_CRLF.csv' with delimiter ';' encoding 'utf8' header csv;
drop table if exists uci_gps_trajecory;
create table uci_gps_trajecory (
    trackid integer,
    timest timestamp,
    androidid integer,
    latitude double precision,
    longitude double precision,
    speedkmh double precision,
    duration double precision,
    distancekm double precision,
    trafficrating smallint, -- 3- good, 2- normal, 1- bad
    busrating smallint, -- 1- The amount of people inside the bus is little, 2- The bus is not crowded, 3- The bus is crowded
    weatherrating smallint, --  2- sunny, 1- raining
    carorbus smallint, -- 1 - car, 2-bus
    linha text,
    constraint uci_gps_trajecory_pkey primary key (trackid, timest, androidid)
);
copy uci_gps_trajecory from 'C:\trajectory\go_track_merged_UTF8_CRLF.csv' with delimiter ';' encoding 'utf8' header csv;