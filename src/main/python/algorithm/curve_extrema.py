"""Elaboration of stay points using CurveExtrema algorithm."""
import datetime
import logging

import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras

from src.main.python.algorithm import algorithm_common
from src.main.python.algorithm.algorithm_common import AlgorithmUtils
from src.main.python.algorithm.algorithm_common import ConnectionControlledExecution
from src.main.python.algorithm.algorithm_common import SpatialAlgorithm
from src.main.python.mydata import main


class CurveExtremaAlgorithm(SpatialAlgorithm):
    """Curve Extrema Algorithm wrapper class."""

    def __init__(self, dist_thr, window_size, min_value, max_value, parameter_name, type_id, id_name, min_accuracy, max_speed, droptable,
                 significative_id, start_date, end_date, input_table_name, output_table_name,
                 cluster_reference_table, runentry_table, relativepath, dist_function):
        """Curve Extrema algorithm params initialization."""
        self.dist_thr = dist_thr
        self.window_size = window_size
        self.min_value = min_value
        self.max_value = max_value
        self.parameter_name = parameter_name
        self.type_id = type_id

        super().__init__(id_name=id_name, min_accuracy=min_accuracy, max_speed=max_speed, droptable=droptable, significative_id=significative_id,
                         start_date=start_date, end_date=end_date, input_table_name=input_table_name, output_table_name=output_table_name,
                         cluster_reference_table=cluster_reference_table, runentry_table=runentry_table, relativepath=relativepath,
                         dist_function=dist_function)

    def compute_neighborhood(self, sequence):
        """Compute neighborhood of points in a sequence and returns the neighborhood subsequences."""
        points_number = len(sequence) / self.window_size
        start_point = sequence[0]
        current_sub_sequence = [start_point, ]
        for p in sequence[1:]:
            distance_from_start_point = self.dist_function((p["latitude"], p["longitude"]), (start_point["latitude"], start_point["longitude"]))
            if distance_from_start_point <= self.dist_thr and len(current_sub_sequence) < points_number:
                current_sub_sequence.append(p)
            else:
                if len(current_sub_sequence) > 1 or points_number == 1:
                    yield current_sub_sequence
                start_point = p
                current_sub_sequence = [start_point, ]
        if len(current_sub_sequence) > 1 or points_number == 1:
            yield current_sub_sequence

    def normalize_sequences(self, sequences):
        """Normalize sequence in way to reduce error due data accuracy."""
        seq = []
        for sequence in sequences:
            previous_centroid = pd.Series()
            for neighborhoodsequence in self.compute_neighborhood(sequence=sequence):
                centroid = algorithm_common.calculate_centroid(neighborhoodsequence, previous_centroid)
                previous_centroid = pd.Series(centroid)
                centroid["timespent"] = neighborhoodsequence[-1]["timest"] - neighborhoodsequence[0]["timest"]
                centroid["hits"] = len(neighborhoodsequence)
                seq.append(centroid)
            if seq:
                yield seq
                seq = []

    def raw_staypoints_curve_extrema(self, rows):
        """Curve extrema algorithm to discovery staypoints as points list."""
        stay_points = []
        for sequence in self.normalize_sequences(algorithm_common.filter_function(self.min_value, self.max_value, self.parameter_name, rows)):
            current_stay_point = []
            for i in range(1, len(sequence)):
                point = sequence[i]
                previousPoint = sequence[i - 1]
                if point["speed"] <= self.max_speed:
                    current_stay_point.append(previousPoint)
                else:
                    if current_stay_point:
                        stay_points.append(current_stay_point)
                        current_stay_point = []
            if len(sequence) > 1:
                last_point = sequence[-1]
                if last_point["speed"] <= self.max_speed:
                    current_stay_point.append(last_point)
            if current_stay_point:
                stay_points.append(current_stay_point)
        return stay_points

    def curve_extrema_algorithm(self, rows):
        """Curve extrema algorithm to discovery staypoints as points StayPointList object."""
        clusters_dataframe = pd.DataFrame(columns=algorithm_common.ce_columns)
        # TODO complete cluster reference
        # cluster_references_dataframe = pd.DataFrame(columns=algorithm_common.ce_cluster_references_columns)
        start_time = datetime.datetime.today()
        previous_centroid = pd.Series()
        last_id = -1
        df_id = 0
        elaborated_points = 0
        for staypoint in self.raw_staypoints_curve_extrema(rows):
            significative_id = staypoint[0]["significativeid"]
            # TODO here I reset the previous centroid on new significativeid,
            # maybe I should only not flatten the sequences during the normalization
            if last_id != significative_id:
                previous_centroid = pd.Series()
            last_id = significative_id
            timespent = staypoint[-1]["timest"] - staypoint[0]["timest"] + staypoint[-1]["timespent"]
            centroid = algorithm_common.calculate_centroid(staypoint, previous_centroid)
            timest = centroid["timest"]
            centroid_latitude = centroid["latitude"]
            centroid_longitude = centroid["longitude"]
            centroid_distdiff = centroid["distdiff"]
            significative_id = centroid["significativeid"]
            staypointhits = sum([x["hits"] for x in staypoint])
            clusters_dataframe.loc[df_id] = [df_id, significative_id, timest, timespent, centroid_latitude, centroid_longitude, centroid_distdiff,
                                             staypointhits, self.type_id]
            elaborated_points += staypointhits
            # TODO complete cluster reference
            # if self.create_cluster_reference:
            #     for point in cluster.get_points():
            #         cluster_references_dataframe.loc[len(cluster_references_dataframe)] = [cluster_id, point['latitude'], point['longitude'],

            previous_centroid = pd.Series(centroid)
            df_id += 1
        elapsedtime = (datetime.datetime.today() - start_time).total_seconds()

        self.run_entry_my_data = self.create_run_entry(total_points=elaborated_points, execution_time=elapsedtime)
        self.clusters_dataframe = clusters_dataframe
        # TODO complete cluster reference
        # self.cluster_references_dataframe = cluster_references_dataframe

    def create_mydata_from_dataframe(self, runid):
        """Create the staypoints list (MyData object) from a DataFrame."""
        for k, v in zip(algorithm_common.ce_columns, algorithm_common.ce_dtype):
            self.clusters_dataframe[k] = self.clusters_dataframe[k].astype(v)
        stay_points_list = main.MyData(self.output_table_name)
        stay_points_list.add('id', 'integer', self.clusters_dataframe["id"], pk=True)
        stay_points_list.add('significativeid', 'integer', self.clusters_dataframe["significativeid"])
        stay_points_list.add('timest', 'integer', self.clusters_dataframe["timest"])
        stay_points_list.add('timespent', 'integer', self.clusters_dataframe["timespent"])
        stay_points_list.add('latitude', 'double', self.clusters_dataframe["latitude"])
        stay_points_list.add('longitude', 'double', self.clusters_dataframe["longitude"])
        stay_points_list.add('distdiff', 'double', self.clusters_dataframe["distdiff"])
        stay_points_list.add('staypointhits', 'integer', self.clusters_dataframe["staypointhits"])
        stay_points_list.add('typeid', 'integer', self.clusters_dataframe["typeid"], pk=True)
        stay_points_list.add('runid', 'autoincrement', np.repeat(runid, len(self.clusters_dataframe)), pk=True, fk=True, ftable=self.runentry_table)
        return stay_points_list

    def create_run_entry(self, total_points, execution_time):
        """
        Create the run MyData object.

        :param total_points: total points
        :param execution_time: total elapsed time
        :return: run
        """
        run = super().create_run_entry(total_points, execution_time)
        run.add('distthr', 'integer', [self.dist_thr, ], unique=True)
        run.add('windowsize', 'integer', [self.window_size, ], unique=True)
        run.add('minvalue', 'varchar', [str(self.min_value or 'null'), ], unique=True)
        run.add('maxvalue', 'varchar', [str(self.max_value or 'null'), ], unique=True)
        run.add('parametername', 'varchar', [str(self.parameter_name or 'null'), ], unique=True)
        run.add('typeid', 'integer', [self.type_id, ], unique=True)
        return run

    def get_stay_points_from_db(self):
        """Create stay points."""
        with open('../../../../credentials_postgress.txt', 'r') as file:
            logging.debug('Trying to connect...')
            connectionstring = file.read()
            with psycopg2.connect(connectionstring) as conn:
                logging.warning('Connected!')
                with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                    sql_query = "select significativeid, latitude, longitude, timest from " \
                                "{} where accuracy <= {} order by significativeid, timest".format(self.input_table_name, self.min_accuracy)
                    cur.execute(sql_query)
                    return self.curve_extrema_algorithm(cur)

    def get_ce_search_query_where_clause(self):
        """Return ce runentry base search query where clause."""
        return self.get_spatial_search_query_where_clause() + ' and distthr = {} and windowsize = {} and minvalue = \'{}\' and' \
                                                              ' maxvalue = \'{}\' and parametername = \'{}\' and typeid = {}' \
                                                              .format(self.dist_thr, self.window_size, self.min_value or 'null', self.max_value or 'null',
                                                                      self.parameter_name or 'null', self.type_id)

    def get_runentry_search_query(self):
        """Return curve extrema algorithm runentry search query."""
        return 'select * from {} where {};'.format(self.runentry_table, self.get_ce_search_query_where_clause())

    def run_algorithm(self):
        """Insert previously created stay points into the db."""
        with ConnectionControlledExecution(self.credential_file) as conn_ce:
            # check for run not already launched
            if not self.droptable:
                conn_ce.cur.execute(self.get_runentry_search_query())
                found = conn_ce.cur.fetchone()
                if found:
                    raise Exception(self.get_ce_search_query_where_clause() + ' already on table {}'.format(self.runentry_table))

            sql_query = 'select significativeid, latitude, longitude, timest from {}' \
                        ' where accuracy <= {} and significativeid = {} and timest between {} and {} order by timest' \
                        .format(self.input_table_name, self.min_accuracy, self.significative_id, self.start_date, self.end_date)
            conn_ce.cur.execute(sql_query)
            self.curve_extrema_algorithm(conn_ce.cur)

            # upload of run_entry_my_data
            run_entry_csv_file = self.relativepath + 'data/runentryobj_stay_points_ce.csv'
            algorithm_common.upload_my_data_object_to_db(my_data=self.run_entry_my_data, csvfile_name=run_entry_csv_file, droptable=self.droptable,
                                                         cur=conn_ce.cur, columns=AlgorithmUtils.get_ce_runentry_columns())
            conn_ce.cur.execute(self.get_runentry_search_query())
            found = conn_ce.cur.fetchone()
            runid = None
            if found:
                runid = found['runid']
                logging.info('runid: {}'.format(runid))
            else:
                raise Exception('something went wrong during runentry table {} upload with parameters {} on table'
                                .format(self.runentry_table, self.get_ce_search_query_where_clause()))

            stay_points_list = self.create_mydata_from_dataframe(runid=runid)
            stay_points_list_csv_file = self.relativepath + 'data/significative_stay_points_ce.csv'
            algorithm_common.upload_my_data_object_to_db(my_data=stay_points_list, csvfile_name=stay_points_list_csv_file, droptable=self.droptable,
                                                         cur=conn_ce.cur, columns=algorithm_common.ce_columns + ("runid",))
            # TODO complete cluster reference table on CE
            # if self.cluster_reference_table is not None:
            #     cluster_reference = self.create_cluster_references_list_from_dataframe(runid=runid)
            #     cluster_reference_ce_csv_file = self.relativepath + 'data/cluster_reference_ce.csv'
            #     algorithm_common.upload_my_data_object_to_db(my_data=cluster_reference, csvfile_name=cluster_reference_ce_csv_file,
            #                                                  droptable=self.droptable, cur=conn_ce.cur,
            #                                                  columns=algorithm_common.ce_cluster_references_columns + ("runid",))
            self.update_geom_column(conn_ce)

    # TODO after last modifications to the algorithm the generation from csv is no longer working,
    # if this method is neeeded it should be upgreaded to the new version, otherwise you can consider to delete it.
    def create_stay_points_from_enriched_csv(self, csv_file_name):
        """Create stay points from csv."""
        filename = csv_file_name
        names = main.get_names(filename)
        rows = pd.read_csv(filename, delimiter=';', header=0, names=names)

        def rows_gen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rows_gen()
        self.curve_extrema_algorithm(generator)
        stay_points_list = self.create_mydata_from_dataframe(runid=1)
        np.savetxt(csv_file_name + '.csv', stay_points_list.getData(), delimiter=';', fmt='%s')
        return stay_points_list

    # TODO after last modifications to the algorithm the generation from csv is no longer working,
    # if this method is neeeded it should be upgraded to the new version, otherwise you can consider to delete it.
    def create_stay_points_csv_from_db(self):
        """Create stay points from csv."""
        stay_points_list = self.get_stay_points_from_db()
        np.savetxt('../../../data/significative_stay_points5000.csv', stay_points_list.getData(), delimiter=';', fmt='%s')
        return stay_points_list


if __name__ == '__main__':
    # logging.basicConfig(level=logging.DEBUG)
    minValue = None
    maxValue = None
    typeid = 1100
    parameter = None
    speedthreshold = 0.28
    distThr = 20
    windowSize = 10
    minAccuracy = 25
    significativeid = 1
    droptable = True
    startTime = datetime.datetime.today()
    inputTableName = "significative_enriched_data"
    outputTableName = 'ce_cluster_stay_points_significative'  # algorithm_common.ce_tableName
    dist_function = algorithm_common.haversine_distance
    logging.warning('started at: ' + str(startTime))
    # 2016 01 01
    start_date = 1451606400
    # 2016 11 01
    end_date = 1477958400
    cluster_reference_table = None
    runentry_table = AlgorithmUtils.ce_runentry_tableName
    relativepath = "../../../"
    cea = CurveExtremaAlgorithm(
        min_value=minValue, max_value=maxValue, parameter_name=parameter, max_speed=speedthreshold, dist_thr=distThr,
        window_size=windowSize, type_id=typeid, id_name="timest", min_accuracy=minAccuracy, significative_id=significativeid,
        input_table_name=inputTableName, output_table_name=outputTableName, droptable=droptable, start_date=start_date, end_date=end_date,
        cluster_reference_table=cluster_reference_table, runentry_table=runentry_table, relativepath=relativepath, dist_function=dist_function)
    cea.run_algorithm()
    endTime = datetime.datetime.today()
    logging.warning('ended at: ' + str(endTime))
    logging.warning('total time: ' + str(endTime - startTime))
