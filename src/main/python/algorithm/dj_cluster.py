"""Elaboration of stay points using Density Join Cluster algorithm."""
import datetime
import logging

import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras
from haversine import haversine

from src.main.python.algorithm import algorithm_common
from src.main.python.algorithm.algorithm_common import AlgorithmUtils
from src.main.python.algorithm.algorithm_common import Cluster
from src.main.python.algorithm.algorithm_common import ConnectionControlledExecution
from src.main.python.algorithm.algorithm_common import SimpleCluster
from src.main.python.algorithm.algorithm_common import SpatialAlgorithm
from src.main.python.mydata import main


class DJClusterAlgorithm(SpatialAlgorithm):
    """Density Join Cluster Algorithm wrapper."""

    def __init__(self, eps, min_pts, id_name, min_accuracy, max_speed, droptable, significative_id, start_date, end_date, input_table_name, output_table_name,
                 cluster_reference_table, runentry_table, relativepath, dist_function):
        """Density join cluster algorithm params initialization."""
        self.eps = eps
        self.min_pts = min_pts

        super().__init__(id_name=id_name, min_accuracy=min_accuracy, max_speed=max_speed, droptable=droptable, significative_id=significative_id,
                         start_date=start_date, end_date=end_date, input_table_name=input_table_name, output_table_name=output_table_name,
                         cluster_reference_table=cluster_reference_table, runentry_table=runentry_table, relativepath=relativepath,
                         dist_function=dist_function)

    def recursive_compute_neighborhood(self, points, elements, cluster_key_set):
        """Recursively compute neighborhood."""
        while len(points) > 0:
            # intermidiate log for veeery long sequences
            if len(points) % 10000 == 0:
                logging.debug("remaining {} points".format(len(points)))
            current_point = points.pop()
            for x in points:
                if self.dist_function((x['latitude'], x['longitude']), (current_point['latitude'], current_point['longitude'])) <= self.eps:
                    x['neighborhoodset'].add(current_point[self.id_name])
                    current_point['neighborhoodset'].add(x[self.id_name])
            self.add_cluster_to_dict(current_point, self.min_pts, elements, cluster_key_set)

    def add_cluster_to_dict(self, data, minpts, elements, cluster_dict):
        """Create cluster reference that are going to be merged."""
        dimension = sum([elements[x]['repetition'] for x in data['neighborhoodset']]) + data['repetition']
        if dimension >= minpts:
            cluster_dict.add(data[self.id_name])

    def create_clusters(self, elements, cluster_key_set):
        """Create clusters object from key reference, merging recursively."""
        checked = set()

        def descend(result, neighborhood):
            while True:
                if len(neighborhood) == 0:
                    return result
                else:
                    n = neighborhood.pop()
                    if n in checked:
                        neighborhood = neighborhood.difference(checked)
                    else:
                        checked.add(n)
                        cluster_key_set.discard(n)
                        result = result.union([n])
                        neighborhood = neighborhood.union(elements[n]['neighborhoodset']).difference(checked)

        clusters = []
        while len(cluster_key_set) > 0:
            el = cluster_key_set.pop()
            res = set()
            res.add(el)
            clusters.append(
                SimpleCluster(points=[elements[x] for x in descend(res, set(elements[el]['neighborhoodset']))], idName=self.id_name,
                              columnsNames=(self.id_name, 'significativeid', 'latitude', 'longitude', 'repetition')))
        return clusters

    def recursive_dj_cluster(self, points):
        """Create the cluster list with recursive dj cluster algorithm."""
        elements = {}
        cluster_key_set = set()
        points = self.get_list_with_neighborhood_set_from_dict_row(points)
        for x in points:
            elements[x[self.id_name]] = x
        total_points = len(points)
        logging.debug("started neighborhood of {} points".format(total_points))

        self.recursive_compute_neighborhood(points=points, elements=elements, cluster_key_set=cluster_key_set)
        logging.debug("ended neighborhood, starting cretecluster")

        result = self.create_clusters(elements=elements, cluster_key_set=cluster_key_set)
        logging.debug("ended cretecluster")
        return result, total_points

    def get_list_with_neighborhood_set_from_dict_row(self, dict_row):
        """
        Trasform DictRow object into list of dictionary if possible.

        It adds the key neighborhoodset with an empty set as value.
        Dict Row object is slow to iterate, this should increase performance
        """
        if isinstance(dict_row, list):
            if len(dict_row) > 0 and isinstance(dict_row[0], psycopg2.extras.DictRow):
                l = []
                for row in dict_row:
                    d = {}
                    l.append(d)
                    if self.id_name not in row.keys():
                        d[self.id_name] = len(l)
                    for c in row.keys():
                        d[c] = row[c]
                    d["neighborhoodset"] = set()
                return l
            else:
                return dict_row
        else:
            return dict_row

    ###################
    # old implementation section
    # this implementation has low performance due to the DataFrame object structure, but it use less memory than the other impl.
    # at the moment is used for comparing results with new implementation.
    # TODO we can change the information stored from DataFrames to sets and lists to elaborate faster larger datasets.
    ###################
    def compute_neighborhood(self, start_point, points):
        """Compute neighborhood of a point in a points dataframe."""
        # TODO transform this function with cython for speed up https://pandas.pydata.org/pandas-docs/stable/enhancingperf.html
        # TODO add memory structure to reduce to n * log(n) computational cost
        return points[points.apply(lambda p: haversine((p["latitude"], p["longitude"]), (start_point["latitude"], start_point["longitude"])) * 1000 <= self.eps,
                                   axis=1)]

    @staticmethod
    def get_data_frame_from_dict_row(dict_row):
        """
        Trasform DictRow object into list of dictionary if possible.

        Dict Row object is slow to iterate, this should increase performance
        """
        if isinstance(dict_row, list):
            if len(dict_row) > 0 and isinstance(dict_row[0], psycopg2.extras.DictRow):
                l = []
                for row in dict_row:
                    d = {}
                    l.append(d)
                    for c in row.keys():
                        d[c] = row[c]
                df = pd.DataFrame(l)
                for k in algorithm_common.dj_columns_to_select:
                    df[k] = df[k].astype(np.dtype(np.float64))
                return df
            else:
                return pd.DataFrame(dict_row)
        else:
            return pd.DataFrame(dict_row)

    @staticmethod
    def get_list_from_dict_row(dict_row):
        """
        Trasform DictRow object into list of dictionary if possible.

        Dict Row object is slow to iterate, this should increase performance
        """
        if isinstance(dict_row, list):
            if len(dict_row) > 0 and isinstance(dict_row[0], psycopg2.extras.DictRow):
                l = []
                for row in dict_row:
                    d = {}
                    l.append(d)
                    for c in row.keys():
                        d[c] = row[c]
                return l
            else:
                return dict_row

    def sequence_dj_cluster(self, points):
        """Dj Cluster algorithm logic."""
        clusters = pd.Series()
        points = self.get_data_frame_from_dict_row(points)
        for i in range(len(points)):
            point = points.iloc[i]
            neighborhood = self.compute_neighborhood(point, points)
            if sum(neighborhood['repetition']) >= self.min_pts:
                # now = datetime.datetime.today()
                # logging.debug('starting cluster creation at: ' + str(now))
                cluster = Cluster(neighborhood, self.id_name)
                # now2 = datetime.datetime.today()
                # logging.debug('ended cluster creation and starting join time: ' + str(now2 - now))
                clusters = Cluster.join_clusters(clusters, cluster)
                # now3 = datetime.datetime.today()
                # logging.debug('ended cluster creation time: ' + str(now3 - now2))
            else:
                # Label p as noise
                pass
        return clusters
    ##################
    # end of old implementation
    ##################

    def density_join_cluster(self, sequences):
        """Iterate dj cluster algorithm on points sequences."""
        for points in sequences:
            yield self.recursive_dj_cluster(points)

    def density_join_cluster_extract_max_cluster(self, generator):
        """
        Select the centroid of the max cluster by dimension for each points sequence.

        TODO this function is not used at the moment, if it's not necessary consider to delet it.
        """
        df = pd.DataFrame(columns=algorithm_common.dj_columns)
        df_id = 0

        for clusters, t_points in self.density_join_cluster(generator):
            if len(clusters) > 0:
                max_dimension = max([x.get_dimension() for x in clusters])
                max_cluster = None
                for c in clusters:
                    if c.get_dimension() == max_dimension:
                        max_cluster = c
                        break
                if max_cluster is not None:
                    staypoint = max_cluster.get_centroid()
                    df.loc[df_id] = [df_id, staypoint["significativeid"], staypoint["latitude"], staypoint["longitude"], self.eps, self.min_pts,
                                     self.min_accuracy, max_cluster.get_dimension(), 1]
                    df_id += 1
        return self.create_mydata_from_dataframe(df)

    def dj_cluster_relevance(self, generator):
        """Select the centroid of the max cluster by dimension for each points sequence."""
        clusters_dataframe = pd.DataFrame(columns=algorithm_common.dj_columns)
        cluster_id = 0
        start_time = datetime.datetime.today()

        cluster_references_dataframe = pd.DataFrame(columns=algorithm_common.dj_cluster_references_columns)
        totalpoints = 0
        # TODO change logic, now the algorithm works only on a id at a time
        for clusters, elaborated_points in self.density_join_cluster(generator):
            if len(clusters) > 0:
                relevance = 0
                for cluster in sorted(clusters, key=lambda x: x.get_dimension(), reverse=True):
                    staypoint = cluster.get_centroid()
                    clusters_dataframe.loc[cluster_id] = [cluster_id, staypoint["latitude"], staypoint["longitude"], cluster.get_dimension(), relevance,
                                                          self.significative_id]
                    if self.create_cluster_reference:
                        for point in cluster.get_points():
                            cluster_references_dataframe.loc[len(cluster_references_dataframe)] = [cluster_id, point['latitude'], point['longitude'],
                                                                                                   point['repetition']]
                    relevance += 1
                    cluster_id += 1
                totalpoints += elaborated_points
        elapsedtime = (datetime.datetime.today() - start_time).total_seconds()

        self.run_entry_my_data = self.create_run_entry(total_points=elaborated_points, execution_time=elapsedtime)
        self.clusters_dataframe = clusters_dataframe
        self.cluster_references_dataframe = cluster_references_dataframe

    def create_mydata_from_dataframe(self, runid):
        """Create the staypoints list (MyData object) from a DataFrame."""
        for k, v in zip(algorithm_common.dj_columns, algorithm_common.dj_dtype):
            self.clusters_dataframe[k] = self.clusters_dataframe[k].astype(v)
        stay_points_list = main.MyData(self.output_table_name)
        stay_points_list.add('cluster_id', 'integer', self.clusters_dataframe["cluster_id"], pk=True)
        stay_points_list.add('latitude', 'double', self.clusters_dataframe["latitude"])
        stay_points_list.add('longitude', 'double', self.clusters_dataframe["longitude"])
        stay_points_list.add('dimension', 'integer', self.clusters_dataframe["dimension"])
        stay_points_list.add('relevance', 'integer', self.clusters_dataframe["relevance"])
        stay_points_list.add('significativeid', 'integer', self.clusters_dataframe["significativeid"])
        stay_points_list.add('runid', 'autoincrement', np.repeat(runid, len(self.clusters_dataframe)), pk=True, fk=True, ftable=self.runentry_table)
        return stay_points_list

    def create_run_entry(self, total_points, execution_time):
        """
        Create the run MyData object.

        :param total_points: total points
        :param execution_time: total elapsed time
        :return: run
        """
        run = super().create_run_entry(total_points, execution_time)
        run.add('eps', 'integer', [self.eps, ], unique=True)
        run.add('minpts', 'integer', [self.min_pts, ], unique=True)
        return run

    def create_cluster_references_list_from_dataframe(self, runid):
        """
        Create the cluster reference MyData object.

        It contains the run id, the cluster id, every latitude longitude pair and the pair repetition.
        """
        for k, v in zip(algorithm_common.dj_cluster_references_columns, algorithm_common.dj_cluster_references_dtype):
            self.cluster_references_dataframe[k] = self.cluster_references_dataframe[k].astype(v)
        dj_cluster_references = main.MyData(self.cluster_reference_table)
        dj_cluster_references.add('referenceid', 'autoincrement', None, pk=True)
        dj_cluster_references.add('cluster_id', 'integer', self.cluster_references_dataframe["cluster_id"], fk=True, ftable=self.output_table_name, unique=True)
        dj_cluster_references.add('latitude', 'double', self.cluster_references_dataframe["latitude"], unique=True)
        dj_cluster_references.add('longitude', 'double', self.cluster_references_dataframe["longitude"], unique=True)
        dj_cluster_references.add('p_number', 'integer', self.cluster_references_dataframe["p_number"])
        dj_cluster_references.add('runid', 'autoincrement', np.repeat(runid, len(self.cluster_references_dataframe)), fk=True, ftable=self.output_table_name,
                                  unique=True)

        return dj_cluster_references

    def create_stay_points_with_dj_from_curve_points_csv(self):
        """
        Create stay points using dj cluster from curve extrima points csv.

        TODO this function is no longer used, consider if it should be deleted.
        """
        filename = '../../../data/significative_stay_points.csv'
        rows = pd.read_csv(filename, delimiter=';', names=algorithm_common.ce_columns)

        def rows_gen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rows_gen()
        filtered_generator = algorithm_common.filter_function(11, 11, "typeid", generator)
        self.dj_cluster_relevance(filtered_generator)

        # TODO runid should be retrieved from db
        stay_points_list = DJClusterAlgorithm.create_mydata_from_dataframe(runid="XX")
        stay_points_list.getData().to_csv(path_or_buf='../../../data/significative_stay_points_dj.csv', sep=';', header=False, index=False)
        return stay_points_list

        # TODO this function is necessary to comprare CE to DJ results.
        # Modify this function to recover stay points from db.
    def upload_stay_points_to_db_from_curve_points_csv(self):
        """
        Run Dj cluster algorithm on previoulsy created CE stay points.

        This function comprare CE to DJ results.
        """
        with open('../../../credentials_postgress.txt', 'r') as file:
            logging.debug('Trying to connect...')
            connectionstring = file.read()
            with psycopg2.connect(connectionstring) as conn:
                logging.warning('Connected!')
                with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                    stay_points_list = self.create_stay_points_with_dj_from_curve_points_csv()
                    # stay_points_list.getData().to_csv(path_or_buf='../../../data/significative_stay_points_dj.csv', sep=';', header=False, index=False)
                    cur.execute(stay_points_list.getTableCreation())
                    f = open('../../../data/significative_stay_points_dj.csv', 'r')
                    logging.warning('Copy to table...')
                    cur.copy_from(f, stay_points_list.tableName, sep=';')
                    f.close()
                    cur.execute('alter table ' + stay_points_list.tableName + ' add column geom geometry(Point,4326);\n' +
                                'update ' + stay_points_list.tableName + ' set geom = ST_SetSRID(ST_MakePoint(longitude, latitude),4326)')

    def get_dj_search_query_where_clause(self):
        """Return dj cluster runentry base search query where clause."""
        return self.get_spatial_search_query_where_clause() + ' and eps = {} and minpts = {}'.format(self.eps, self.min_pts)

    def get_runentry_search_query(self):
        """Return curve extrema algorithm runentry search query."""
        return 'select * from {} where {};'.format(self.runentry_table, self.get_dj_search_query_where_clause())

    def run_algorithm(self):
        """Insert previously created stay points into the db."""
        with ConnectionControlledExecution(self.credential_file) as conn_ce:
            # check for run not already launched
            if not self.droptable:
                conn_ce.cur.execute(self.get_runentry_search_query())
                found = conn_ce.cur.fetchone()
                if found:
                    raise Exception(self.get_dj_search_query_where_clause() + ' already on table {}'.format(self.runentry_table))
            # get points from db
            sql_query = 'select significativeid, latitude, longitude, count(*) as repetition from {}' \
                        ' where accuracy <= {} and speed <= {} and significativeid = {} and timest between {} and {}' \
                        ' group by significativeid, latitude, longitude ' \
                        ' order by repetition desc;'.format(self.input_table_name, self.min_accuracy, self.max_speed, self.significative_id,
                                                            self.start_date, self.end_date)
            conn_ce.cur.execute(sql_query)
            # TODO pass filters as parameters
            filtered_generator = algorithm_common.filter_function(min_value=None, max_value=None, parameter=None, rows=conn_ce.cur)
            # go for results
            self.dj_cluster_relevance(generator=filtered_generator)
            # upload of run_entry_my_data
            run_entry_csv_file = self.relativepath + 'data/runentryobj_stay_points_dj.csv'
            algorithm_common.upload_my_data_object_to_db(my_data=self.run_entry_my_data, csvfile_name=run_entry_csv_file, droptable=self.droptable,
                                                         cur=conn_ce.cur, columns=AlgorithmUtils.get_dj_runentry_columns())
            # retrieve runid
            conn_ce.cur.execute(self.get_runentry_search_query())
            found = conn_ce.cur.fetchone()
            runid = None
            if found:
                runid = found['runid']
                logging.info('runid: {}'.format(runid))
            else:
                raise Exception('something went wrong during runentry table {} upload with parameters {} on table'
                                .format(self.runentry_table, self.get_dj_search_query_where_clause()))

            stay_points_list = self.create_mydata_from_dataframe(runid=runid)
            stay_points_list_csv_file = self.relativepath + 'data/significative_stay_points_dj.csv'
            algorithm_common.upload_my_data_object_to_db(my_data=stay_points_list, csvfile_name=stay_points_list_csv_file, droptable=self.droptable,
                                                         cur=conn_ce.cur, columns=algorithm_common.dj_columns + ("runid",))
            if self.cluster_reference_table is not None:
                cluster_reference = self.create_cluster_references_list_from_dataframe(runid=runid)
                cluster_reference_dj_csv_file = self.relativepath + 'data/cluster_reference_dj.csv'
                algorithm_common.upload_my_data_object_to_db(my_data=cluster_reference, csvfile_name=cluster_reference_dj_csv_file,
                                                             droptable=self.droptable, cur=conn_ce.cur,
                                                             columns=algorithm_common.dj_cluster_references_columns + ("runid",))
            self.update_geom_column(conn_ce)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
    startTime = datetime.datetime.today()
    logging.info('started run at : {}'.format(startTime))
    djAlg = DJClusterAlgorithm(eps=23, min_pts=23, id_name="timest", min_accuracy=30, max_speed=0.30, relativepath='../../../', droptable=False,
                               significative_id=1, start_date=1451606400, end_date=1483228800, input_table_name='significative_enriched_data',
                               output_table_name='dj_cluster_stay_points_significative', cluster_reference_table=None,
                               runentry_table=AlgorithmUtils.dj_runentry_tableName, dist_function=None)
    djAlg.run_algorithm()
    logging.info('gracefully ended at: {}'.format(str(datetime.datetime.today())))
    logging.info('total time: {}'.format(str(datetime.datetime.today() - startTime)))
