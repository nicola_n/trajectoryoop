"""Load data to/from database."""
import logging
import os
import sys
from argparse import ArgumentParser
from os.path import join

import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras

from src.main.python.mydata import main


def save_data_to_csv(credentials_filename, data_directory, query="select 1", filename="foo"):
    """Save significative data to csv file."""
    with open(credentials_filename, 'r') as file:
        with psycopg2.connect(file.read()) as conn:
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            outputquery = "copy ({0}) to STDOUT with header delimiter ';' csv".format(query)
            with open(join(data_directory, filename) + '.csv', 'w') as f:
                cur.copy_expert(outputquery, f)


def load_csv_to_postgres(filename, tablename, createtablequery, credentials_filename, data_directory, delimiter=';'):
    """Load csv to postgres database."""
    with open(credentials_filename, 'r') as file:
        logging.debug('Trying to connect...')
        with psycopg2.connect(file.read()) as conn:
            logging.debug('Connected!')
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute(createtablequery)
            f = open(join(data_directory) + filename, 'r')
            logging.debug('Copy to table...')
            cur.copy_from(f, tablename, sep=delimiter)
            f.close()
            logging.debug('Done...')


def enrich(cur, names, table_name, drop_and_create, db, csvdirectory):
    """Enrich data in db."""
    data = pd.DataFrame(np.array(cur.fetchall()), columns=names)
    # logging.debug(data.dtypes)
    for k, v in zip(names, data.convert_objects(convert_numeric=True).dtypes):
        data[k] = data[k].astype(v)
    logging.debug(data.dtypes)
    logging.debug('Enriching data...')
    newdata = main.enrich(data, table_name, dbtype=db)
    logging.debug('Data enriched!')
    np.savetxt(join(csvdirectory, 'enriched-{}.csv'.format(db)), newdata.getData(), delimiter=';', fmt='%s')
    logging.debug('Data saved!')
    if drop_and_create:
        for x in newdata.getTableCreation().split(';'):
            x = x.strip()
            if len(x) > 0:
                try:
                    cur.execute(x)
                except Exception as e:
                    logging.error(e)
    return newdata


def enrich_postgres(tablename, enrichedtablename, credentials_filename, rootDirectory):
    """Enrich postgres database."""
    tablename = tablename.lower()
    enrichedtablename = enrichedtablename.lower()
    """Enrich data in postgres database."""
    with open(credentials_filename, 'r') as file:
        logging.debug('Trying to connect...')
        with psycopg2.connect(file.read()) as conn:
            logging.debug('Connected!')
            cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
            cur.execute('select significativeid, count(*) from {} group by significativeid'.format(tablename))
            logging.debug('Fetching users...')
            drop_and_create = True
            for x in [x for x in cur.fetchall()]:
                logging.debug('----- User: {}, points: {}'.format(x[0], x[1]))
                cur.execute('select * from {} where significativeid = {} order by timest asc'.format(tablename, x[0]))
                names = [x.name for x in cur.description]
                logging.debug('Fetching query result...')
                enrich(cur, names, enrichedtablename, drop_and_create, 'pg', csvdirectory=join(rootDirectory, "data"))
                drop_and_create = False
                with open(join(rootDirectory, "data", 'enriched-pg.csv'), 'r') as f:
                    logging.debug('Copy to table...')
                    cur.copy_from(f, enrichedtablename, sep=';')
                conn.commit()
            logging.debug('Generating segments...')
            cur.execute('alter table {} add column geomlinestring geometry(Linestring,4326)'.format(enrichedtablename))
            cur.execute('update {} set geomlinestring = ST_MakeLine(geomprevious, geom)'.format(enrichedtablename))
            conn.commit()
            logging.debug('Done...')


def enrich_oracle(tablename, enrichedtablename, credentials_filename, rootDirectory):
    """Enrich oracle database."""
    tablename = tablename.upper()
    enrichedtablename = enrichedtablename.upper()
    """Enrich data in oracle database."""
    import cx_Oracle as cx
    with open(credentials_filename, 'r') as file:
        logging.debug('Trying to connect...')
        with cx.connect(file.read()) as conn:
            logging.debug('Connected!')
            cur = conn.cursor()
            cur.execute("select column_name from user_tab_columns where table_name = '{}'".format(tablename))
            names = [x[0].lower() for x in cur.fetchall()]
            cur.execute('select significativeid from {} group by significativeid order by timest'.format(tablename))
            drop_and_create = True
            for user in [x[0] for x in cur.fetchall()]:
                logging.debug('----- User: {}'.format(user))
                # cur.execute('select * from (select * from significativedata order by significativeid asc, timest asc) where rownum <= 10')
                cur.execute('select * from {} where significativeid = {} order by timest asc'.format(tablename, user))
                logging.debug('Fetching query result')
                data = enrich(cur, names, enrichedtablename, drop_and_create, 'oracle', csvdirectory=join(rootDirectory, "data"))
                drop_and_create = False
                sql_insert = 'insert into {} ({}) values ({})'.format(enrichedtablename, ','.join(data.getNamesList()),
                                                                      ','.join([':' + str(x) for x in range(len(data.getNamesList()))]))
                cur.prepare(sql_insert)
                cur.executemany(None, data.getData().values.tolist())
                conn.commit()

            logging.debug('Genereting segments [geometry]...')
            cur.execute('alter table significative_enriched_data add (geom sdo_geometry, geomlinestring sdo_geometry, geojsonlinestring varchar(200))')
            cur.execute('update significative_enriched_data set '
                        'geomlinestring = sdo_geometry(2002, 4326, null, sdo_elem_info_array (1, 2, 1), sdo_ordinate_array (longitude, latitude, plon, plat)),'
                        'geom = SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(longitude, latitude, null), null, null)')
            logging.debug('Genereting segments [geojson]...')
            cur.execute('update significative_enriched_data set geojsonlinestring = dz_json_main.sdo2geojson(geomlinestring)')
            conn.commit()
            logging.debug('Done...')


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s %(asctime)s %(message)s', level=logging.INFO, datefmt='%Y-%m-%d %H:%M:%S')
    logging.info('Started.')
    arg_parser = ArgumentParser()
    arg_parser.add_argument("db", help="Database type", choices=["oracle", "postgres"])
    arg_parser.add_argument("input", help="Input table")
    arg_parser.add_argument("output", help="Output table")
    arg_parser.add_argument("--credentials", help="Credentials filename", default='../../../credentials_postgress.txt')
    # arg_parser.add_argument("-ft", "--fromtime", help="Export data from this date - format YYYY-MM-DD HH:MM:SS")
    # arg_parser.add_argument("-tt", "--totime", help="Export data to this date - format YYYY-MM-DD HH:MM:SS")
    args = arg_parser.parse_args()

    if not args.db:
        arg_parser.error("Missing database")
        sys.exit(1)

    if not args.input:
        arg_parser.error("Missing input table not given")
        sys.exit(1)

    if not args.output or args.input == args.output:
        arg_parser.error("Missing output table")
        sys.exit(1)

    rootDirectory = os.path.dirname(os.path.dirname(os.path.dirname(os.path.dirname(sys.argv[0]))))
    if args.db == 'oracle':
        enrich_oracle(args.input, args.output)
    elif args.db == 'postgres':
        enrich_postgres(tablename=args.input, enrichedtablename=args.output, credentials_filename=args.credentials, rootDirectory=rootDirectory)
        # enrich_postgres('significativedata', 'significative_enriched_data')
        # enrich_postgres('bigsignificativedata', 'bigsignificativedata_enriched')
    logging.info('End.')
