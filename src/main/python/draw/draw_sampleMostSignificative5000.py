"""Draw segmented data."""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from src.main.python import draw
from src.main.python.mydata import main

if __name__ == "__main__":
    filename = '../../../data/sampleMostSignificative5000.csv-enriched.txt'
    names = main.get_names(filename)
    data = pd.read_csv(filename, delimiter=';', skiprows=1, names=names)
    draw.draw(data[main.LON], data[main.LAT], np.zeros(len(data[main.LON])))
    draw.draw_segments(data[main.LON], data[main.LAT], np.zeros(len(data[main.LON])))
    plt.show()

    s = np.logical_or(main.segment_contiguous_time(data[main.TIME], 600), main.segment_user(data[main.DEVICEID]))
    seg = main.compute_id(s)
    draw.draw(data[main.LON], data[main.LAT], seg)
    draw.draw_segments(data[main.LON], data[main.LAT], seg)
    plt.show()
