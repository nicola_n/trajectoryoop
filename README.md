## Trajectory

Towards semantic trajectories.

### Requirements

- Java 8 (for Gradle)
- Python and `pip`
    - On Windows some packages might be not available (e.g., shapely, fiona).
      Install them as [Python wheels](https://pypi.python.org/pypi/wheel).
      You can download them from [here](http://www.lfd.uci.edu/~gohlke/pythonlibs/#fiona).
      Or, if you installed [Anaconda](https://www.continuum.io/downloads) type
      `conda install -c conda-forge geopandas`
      in a Windows shell.

### Interesting to know

0. Add credentials.txt.pgp
    1. gpg --batch --yes --passphrase *passphrasehere* --symmetric credentials.txt
    1. gpg --batch --yes --passphrase *passphrasehere* credentials.txt.gpg
    1. Still db tests fail because we cannot reach server from outside
    1. openssl fails to decrypt due to version issues

### Working on this project

* This project is organized according to the [gitflow](http://nvie.com/posts/a-successful-git-branching-model/) model
* Make separate commits for logically separate changes [(see this guideline)](https://git.kernel.org/pub/scm/git/git.git/tree/Documentation/SubmittingPatches?id=HEAD)
     * Commit's body uses the imperative, present tense: "change", not "changed" or "changes"
* Use [UTF-8](https://en.wikipedia.org/wiki/UTF-8) encoding
     * PyCharm: Settings -> Editor -> File encoding -> Select `Project encoding: UTF-8`
* Use Unix [EOL](https://en.wikipedia.org/wiki/Newline)
     * PyCharm: Settings -> Editor -> Code Style -> Select `Scheme: Project` -> Select `Line separator: Unix (\n)`
* Use 4 spaces instead of `tab (\t)`
    * File > Settings > Editor > Code Style > Java > Tabs and Indents > Untoggle `Use tab character`
    * Be sure of using spaces by enabling visualization of hidden characters
        * Settings -> Editor -> General -> Appearance -> Toggle `Show whitespaces`
* Use [PEP-8](https://www.python.org/dev/peps/pep-0008/) code conventions, check your code with `flake8`

#### Install `cx_Oracle` on linux

- python `cx_Oracle` ([Stakc overflow](https://stackoverflow.com/questions/4307479/install-cx-oracle-for-python) and [GitHub](https://stackoverflow.com/questions/4307479/install-cx-oracle-for-python))
     0. Download instantclient basic and sdk for linux [here (Version 11.2.0.4.0)](http://www.oracle.com/technetwork/topics/linuxx86-64soft-092277.html)
     0. Create `/opt/ora` and unzip file in this directory
     0. Create a file in /etc/profile.d/oracle.sh that includes
        ```
        export ORACLE_HOME=/opt/ora/instantclient_11_2
        export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ORACLE_HOME
        ```
    0. Create a file in /etc/ld.so.conf.d/oracle.conf that includes `/opt/ora/instantclient_11_2`
    0. Execute `sudo ldconfig` and reboot
    0. `cd $ORACLE_HOME` and `ln -s libclntsh.so.11.1 libclntsh.so`
    0. [Download](https://pypi.python.org/pypi/cx_Oracle/5.2.1) cx_Oracle python source (tar.gz)
    0. `sudo env "ORACLE_HOME=/opt/ora/instantclient_11_2" python setup.py install`
    0. Open python shell
        -
        ```
        import cx_Oracle as cx
        conn = cx.connect("user/pwd@host:port/dbname")
        cursor = conn.cursor()
        cursor.execute('select * from dual')
        res = cursor.fetchall()
        print(res)
        ```
    0. See also [this](http://www.juliandyke.com/Research/Development/UsingPythonWithOracle.php), [this](https://kb.informatica.com/solution/23/Pages/59/501657.aspx), and [this](https://docs.oracle.com/cd/B19306_01/server.102/b14219/net12500.htm)
        ```
        -- to find database SID
        select value from v$parameter where name like '%service_name%'; --ORACLE SERVICE NAME
        select instance from v$thread; -- ORACLE SID
        select host_name,instance_name,version from v$instance; -- ORACLE SID
        ```
- or
    0. Add to `/etc/pacman.conf`
        ```
        [oracle]
        SigLevel = Optional TrustAll
        Server = http://linux.shikadi.net/arch/$repo/$arch/
        ```
    0. And `pacman -Sys oracle`
    0. `yaourt -S php-oci8`

#### Configure Oracle Semantic in a DB

- On Oracle machine
    0. `cd C:\app\Administrator\product\11.2.0\dbhome_2\BIN`
    0. Run `sqlplus sys/pwd@dbname as sysdba`, where `dbname` is the sid
    0. Run `@%ORACLE_HOME%\md\admin\catsem.sql`
- Open a connection in sqldeveloper
    0. Test with the following query `SELECT * FROM MDSYS.RDF_PARAMETER;`

#### Extract location history data

0. Clone https://github.com/Scarygami/location-history-json-converter

#### Oracle shp2geom (probably only from server)

Linux

```
setenv clpath $ORACLE_HOME/jdbc/lib/ojdbc5.jar:$ORACLE_HOME/md/jlib/sdoutl.jar:$ORACLE_HOME/md/jlib/sdoapi.jar
java -cp $clpath oracle.spatial.util.SampleShapefileToJGeomFeature -h gis01 -p 1521 -s orcl -u scott -d <password-for-scott> -t shapes -f shapes.shp -r 8307 -g geom
```

Windows
```
java -classpath %ORACLE_HOME%\jdbc\lib\ojdbc5.jar;%ORACLE_HOME%\md\jlib\sdoutl.jar;%ORACLE_HOME%\md\jlib\sdoapi.jar oracle.spatial.util.SampleShapefileToJGeomFeature -h gis01 -p 1521 -s orcl -u scott -d <password-for-scott> -t shapes -f shapes.shp -r 8307 -g geom
```

#### External links

0. [Oracle semantic](http://docs.oracle.com/cd/E24693_01/appdev.11203/e11828/sdo_rdf_concepts.htm)
    0. [Oracle spatial semantic](http://docs.oracle.com/cd/E24693_01/appdev.11203/e11828/sdo_rdf_concepts.htm#CIHHAEIH)
0. [SEM_MATCH Support for Spatial Queries](https://docs.oracle.com/cd/E24693_01/appdev.11203/e11828/sem_spatial_orageo.htm)
    0. [orageo:distance](https://docs.oracle.com/cd/E24693_01/appdev.11203/e11828/sem_spatial_orageo.htm#BABGIFFF)
