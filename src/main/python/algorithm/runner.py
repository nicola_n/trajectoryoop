"""Runner module to run multiple times algorithm with different input values."""
import datetime
import logging
import sys
import traceback

from src.main.python.algorithm import dj_cluster, my_thread_pool
from src.main.python.algorithm.algorithm_common import AlgorithmUtils
from src.main.python.algorithm.algorithm_common import BaseAlgorithm
from src.main.python.algorithm.curve_extrema import CurveExtremaAlgorithm
from src.main.python.algorithm.dj_cluster import DJClusterAlgorithm
from src.main.python.draw.draw_dj_cluster_results import DrawDensityJoinCluster
from src.main.python.draw.draw_curve_extrema import DrawCurveExtrema
from os.path import join


# this functions have to stay outside of the class because "this package requires that the __main__ module be importable by the children"
# see https://docs.python.org/3/library/multiprocessing.html#using-a-pool-of-workers
def my_work_dj_cluster(my_input, invariant_params):
    """Thread dj cluster work function."""
    my_eps, my_min_pts, my_id_name, my_min_accuracy, my_max_speed, my_droptable, my_significative_id, my_start_date, my_end_date = my_input
    my_relative_path, my_input_table_name, my_output_table_name, my_cluster_reference_table, my_runentry_table, my_dist_function = invariant_params
    dja = dj_cluster.DJClusterAlgorithm(eps=my_eps, min_pts=my_min_pts, id_name=my_id_name, min_accuracy=my_min_accuracy, max_speed=my_max_speed,
                                        relativepath=my_relative_path,
                                        droptable=my_droptable, significative_id=my_significative_id, start_date=my_start_date,
                                        end_date=my_end_date, input_table_name=my_input_table_name, output_table_name=my_output_table_name,
                                        dist_function=my_dist_function,
                                        cluster_reference_table=my_cluster_reference_table, runentry_table=my_runentry_table)
    dja.run_algorithm()
    # If i'm here everything went well
    return True


def my_work_ce_cluster(my_input, invariant_params):
    """Thread curve extrema work function."""
    my_dist_thr, my_window_size, my_min_value, my_max_value, my_parameter_name, my_type_id, my_eps, my_min_pts, my_id_name, my_min_accuracy,\
        my_max_speed, my_droptable, my_significative_id, my_start_date, my_end_date = my_input

    my_relative_path, my_input_table_name, my_output_table_name, my_cluster_reference_table, my_runentry_table, my_dist_function = invariant_params
    dja = CurveExtremaAlgorithm(dist_thr=my_dist_thr, window_size=my_window_size, min_value=my_min_value, max_value=my_max_value,
                                parameter_name=my_parameter_name, type_id=my_type_id, id_name=my_id_name, min_accuracy=my_min_accuracy, max_speed=my_max_speed,
                                relativepath=my_relative_path, droptable=my_droptable, significative_id=my_significative_id, start_date=my_start_date,
                                end_date=my_end_date, input_table_name=my_input_table_name, output_table_name=my_output_table_name,
                                dist_function=my_dist_function, cluster_reference_table=my_cluster_reference_table, runentry_table=my_runentry_table)
    dja.run_algorithm()
    # If i'm here everything went well
    return True


class Runner(BaseAlgorithm):
    """Runner class, the class who runs the other algorithms."""

    algorithm_to_run_DJC = "DJC"
    algorithm_to_run_CE = "CE"
    algorithm_to_run_DRAW_CE = "DRCE"
    algorithm_to_run_DRAW_DJ = "DRDJ"

    def __init__(self, runs, algorithm_to_run, log_file, serial_running_param):
        """Params init."""
        self.runs = runs

        # TODO maybe the parameters should be initialized in a proper function
        # Spatial algorithm params
        self.id_name_tuple = None
        self.min_accuracy_tuple = None
        self.speed_tuple = None
        self.relative_path = None
        self.droptable_tuple = None
        self.significativeid_tuple = None
        self.start_date_tuple = None
        self.end_date_tuple = None
        self.input_table_name = None
        self.output_table_name = None
        self.cluster_reference_table = None
        self.runentry_table = None
        self.dist_function = None
        self.eps_tuple = None
        self.min_pts_tuple = None
        self.dist_thr_tuple = None
        self.window_size_tuple = None
        self.min_value_tuple = None
        self.max_value_tuple = None
        self.parameter_name_tuple = None
        self.type_id_tuple = None

        self.png_path = None
        self.csvfilename = None

        self.algorithm_to_run = algorithm_to_run
        self.log_file = log_file
        self.serial_running_param = serial_running_param
        self.credential_file = None
        self.draw_input_table = None

        if (self.algorithm_to_run != Runner.algorithm_to_run_DJC and self.algorithm_to_run != Runner.algorithm_to_run_CE and
                self.algorithm_to_run != Runner.algorithm_to_run_DRAW_CE and self.algorithm_to_run != Runner.algorithm_to_run_DRAW_DJ):
            raise Exception("Didn't set algorithm_to_run properly? ({})".format(algorithm_to_run))

        thisfile = sys.argv[0][0:-9]
        # HACK can't find right path

        self.log_file = join(thisfile, log_file)
        # overwrite of the previous log
        open(self.log_file, 'w').close()

    def set_draw_dj_params(self, significativeid_tuple, credential_file, draw_input_table):
        """Set up draw dj params."""
        self.significativeid_tuple = significativeid_tuple
        self.credential_file = credential_file
        self.draw_input_table = draw_input_table

    def set_draw_ce_params(self, dist_thr_tuple, window_size_tuple, min_value_tuple, max_value_tuple, parameter_name_tuple, speed_tuple, min_accuracy_tuple,
                           png_path, csvfilename):
        """Set up draw ce params."""
        self.dist_thr_tuple = dist_thr_tuple
        self.window_size_tuple = window_size_tuple
        self.min_value_tuple = min_value_tuple
        self.max_value_tuple = max_value_tuple
        self.parameter_name_tuple = parameter_name_tuple
        self.png_path = png_path
        self.speed_tuple = speed_tuple
        self.csvfilename = csvfilename
        self.min_accuracy_tuple = min_accuracy_tuple

    def set_dj_params(self, eps_tuple, min_pts_tuple):
        """Set up dj params."""
        # DJ params
        self.eps_tuple = eps_tuple
        self.min_pts_tuple = min_pts_tuple

        self.assert_params_len()

    def assert_params_len(self):
        """Assert the params len is equal for every tuple."""
        assert (len(self.id_name_tuple) == self.runs)
        assert (len(self.min_accuracy_tuple) == self.runs)
        assert (len(self.speed_tuple) == self.runs)
        assert (len(self.droptable_tuple) == self.runs)
        assert (len(self.significativeid_tuple) == self.runs)
        assert (len(self.start_date_tuple) == self.runs)
        assert (len(self.end_date_tuple) == self.runs)
        if self.algorithm_to_run == Runner.algorithm_to_run_DJC:
            assert (len(self.eps_tuple) == self.runs)
            assert (len(self.min_pts_tuple) == self.runs)
        elif self.algorithm_to_run == Runner.algorithm_to_run_CE:
            assert (len(self.dist_thr_tuple) == self.runs)
            assert (len(self.window_size_tuple) == self.runs)
            assert (len(self.min_value_tuple) == self.runs)
            assert (len(self.max_value_tuple) == self.runs)
            assert (len(self.parameter_name_tuple) == self.runs)
            assert (len(self.type_id_tuple) == self.runs)

    def set_ce_params(self, dist_thr_tuple, window_size_tuple, min_value_tuple, max_value_tuple, parameter_name_tuple, type_id_tuple):
                # CE params
        """Set up ce params."""
        self.dist_thr_tuple = dist_thr_tuple
        self.window_size_tuple = window_size_tuple
        self.min_value_tuple = min_value_tuple
        self.max_value_tuple = max_value_tuple
        self.parameter_name_tuple = parameter_name_tuple
        self.type_id_tuple = type_id_tuple
        self.assert_params_len()

    def set_spatial_parameters(self, cluster_reference_table, dist_function, droptable_tuple, end_date_tuple, id_name_tuple, input_table_name,
                               min_accuracy_tuple, output_table_name, relative_path, runentry_table, significativeid_tuple, speed_tuple, start_date_tuple):
        """Set up spatial params."""
        self.id_name_tuple = id_name_tuple
        self.min_accuracy_tuple = min_accuracy_tuple
        self.speed_tuple = speed_tuple
        self.relative_path = relative_path
        self.droptable_tuple = droptable_tuple
        self.significativeid_tuple = significativeid_tuple
        self.start_date_tuple = start_date_tuple
        self.end_date_tuple = end_date_tuple
        self.input_table_name = input_table_name
        self.output_table_name = output_table_name
        self.cluster_reference_table = cluster_reference_table
        self.runentry_table = runentry_table
        self.dist_function = dist_function

    def serial_running(self):
        """Runner function to run a single instance at a time."""
        # TODO loggin level should be an input parameter
        logging.basicConfig(filename=self.log_file, level=logging.DEBUG)
        failures = 0
        main_start_time = datetime.datetime.today()
        logging.info('starting {} runs at : {}'.format(self.runs, main_start_time))

        for i in range(self.runs):
            run_start_time = datetime.datetime.today()
            if self.algorithm_to_run == Runner.algorithm_to_run_DJC or self.algorithm_to_run == Runner.algorithm_to_run_CE:
                logging.info('started run {} of {} with params idName={}, minAccuracy={},maxSpeed={}, relativepath={}, droptable={}, '
                             'significativeid={}, startDate={}, endDate={}, inputTableName={}, outputTableName={}, cluster_reference_table={}, '
                             'runentry_table={},'
                             .format(i, self.runs, self.id_name_tuple[i], self.min_accuracy_tuple[i], self.speed_tuple[i],
                                     self.relative_path, self.droptable_tuple[i], self.significativeid_tuple[i], self.start_date_tuple[i],
                                     self.end_date_tuple[i], self.input_table_name, self.output_table_name, self.cluster_reference_table, self.runentry_table))

            try:
                if self.algorithm_to_run == Runner.algorithm_to_run_DJC:
                    logging.info("DJ specific params: eps = {}, MinPts = {}".format(self.eps_tuple[i], self.min_pts_tuple[i]))
                    djca = DJClusterAlgorithm(eps=self.eps_tuple[i], min_pts=self.min_pts_tuple[i], id_name=self.id_name_tuple[i],
                                              min_accuracy=self.min_accuracy_tuple[i], max_speed=self.speed_tuple[i], relativepath=self.relative_path,
                                              droptable=self.droptable_tuple[i], significative_id=self.significativeid_tuple[i],
                                              start_date=self.start_date_tuple[i], end_date=self.end_date_tuple[i],
                                              input_table_name=self.input_table_name, output_table_name=self.output_table_name,
                                              cluster_reference_table=self.cluster_reference_table, runentry_table=self.runentry_table,
                                              dist_function=self.dist_function)
                    djca.run_algorithm()

                elif self.algorithm_to_run == Runner.algorithm_to_run_CE:
                    logging.info("CE specific params: dist_thr = {}, window_size = {}, min_value = {}, max_value = {}, parameter_name = {}, type_id = {}"
                                 .format(self.dist_thr_tuple[i], self.window_size_tuple[i], self.min_value_tuple[i], self.max_value_tuple[i],
                                         self.parameter_name_tuple[i], self.type_id_tuple[i]))
                    ce = CurveExtremaAlgorithm(
                        id_name=self.id_name_tuple[i], min_accuracy=self.min_accuracy_tuple[i], max_speed=self.speed_tuple[i], relativepath=self.relative_path,
                        droptable=self.droptable_tuple[i], significative_id=self.significativeid_tuple[i], start_date=self.start_date_tuple[i],
                        end_date=self.end_date_tuple[i], input_table_name=self.input_table_name, output_table_name=self.output_table_name,
                        cluster_reference_table=self.cluster_reference_table, runentry_table=self.runentry_table, dist_function=self.dist_function,
                        dist_thr=self.dist_thr_tuple[i], window_size=self.window_size_tuple[i], min_value=self.min_value_tuple[i],
                        max_value=self.max_value_tuple[i], parameter_name=self.parameter_name_tuple[i], type_id=self.type_id_tuple[i]
                    )
                    ce.run_algorithm()

                elif self.algorithm_to_run == Runner.algorithm_to_run_DRAW_DJ:
                    logging.info("{} specific params: significativeid = {}, draw_input_table = {}".format(self.algorithm_to_run, self.significativeid_tuple[i],
                                                                                                          self.draw_input_table))
                    ddjc = DrawDensityJoinCluster(credential_file=self.credential_file, significativeid=self.significativeid_tuple[i],
                                                  table_name=self.draw_input_table)
                    ddjc.draw_cluster_number()
                    # time.sleep(1)
                    # ddjc.draw_max_cluster_dimension()
                elif self.algorithm_to_run == Runner.algorithm_to_run_DRAW_CE:
                    dce = DrawCurveExtrema(min_value=self.min_value_tuple[i], max_value=self.max_value_tuple[i], parameter=self.parameter_name_tuple[i],
                                           csvfilename=self.csvfilename, dist_thr=self.dist_thr_tuple[i], window_size=self.window_size_tuple[i],
                                           speed_threshold=self.speed_tuple[i], min_accuracy=self.min_accuracy_tuple[i], png_path=self.png_path)
                    dce.example_draw_ce_raw_values_from_5000_csv()
                    dce.example_draw_ce_normalized_values_from_5000_csv()
                    # dce.example_data_draw_ce_stay_points_from_5000_csv()
                else:
                    raise Exception("How did you get here?!? (Didn't set algorithm_to_run properly -> {})".format(self.algorithm_to_run))
            except Exception:
                failures += 1
                exc_type, exc_value, exc_traceback = sys.exc_info()
                logging.error("".join(traceback.format_exception(exc_type, exc_value, exc_traceback)))
                logging.error('ended with errors at: {}'.format(str(datetime.datetime.today())))
            else:
                logging.info('succesfully ended at: {}, elapsed time since start: {}'
                             .format(datetime.datetime.today(), datetime.datetime.today() - main_start_time))
            logging.info('time of this run: {}'.format(str(datetime.datetime.today() - run_start_time)))

        # TODO create graphs with results
        # I need:
        # - map
        # - histograms with cluster dimension,
        # - clusters number
        # - cluster centroid accuracy (to an expected value)
        logging.info('succesfully ended {} of {} runs at : {}, total elapsed time {} '
                     .format(self.runs - failures, self.runs, datetime.datetime.today(), datetime.datetime.today() - main_start_time))
        if failures > 0:
            exit(1)

    def parallel_running(self):
        """Runner function to run multiple instance at a time."""
        logging.basicConfig(filename=self.log_file, level=logging.DEBUG, format='%(asctime)s %(message)s')
        main_start_time = datetime.datetime.today()
        logging.info('starting {} parallel runs at : {}'.format(self.runs, main_start_time))

        mtp = my_thread_pool.MyThreadPool(my_work_dj_cluster)

        variant_params = ()
        if algorithm_to_run == Runner.algorithm_to_run_DJC:
            variant_params = zip(self.eps_tuple, self.min_pts_tuple, self.id_name_tuple, self.min_accuracy_tuple, self.min_accuracy_tuple, self.droptable_tuple,
                                 self.significativeid_tuple, self.start_date_tuple, self.end_date_tuple)
        else:
            pass
        thread_input = (variant_params, self.relative_path, self.input_table_name, self.output_table_name, self.cluster_reference_table, self.runentry_table,
                        self.dist_function)
        failures = 0
        runid = 0
        for result in mtp.do_work(thread_input):
            if not result:
                failures += 1
            else:
                logging.info('run {} succesfully ended at: {}'.format(runid, str(datetime.datetime.today())))
            runid += 1
        logging.info('total elapsed time: {}'.format(str(datetime.datetime.today() - main_start_time)))
        logging.info('succesfully ended {} of {} runs at : {}'
                     .format(self.runs - failures, self.runs, datetime.datetime.today()))
        if failures > 0:
            exit(1)

    def run_algortihm(self):
        """Command executor."""
        if self.serial_running_param:
            self.serial_running()
        else:
            self.parallel_running()


if __name__ == '__main__':
    eps_tuple = (20,)
    min_pts_tuple = (20,)
    id_name_tuple = ("timest", )
    min_accuracy_tuple = (30,)
    significativeid_tuple = (1,)
    start_date_tuple = (1451606400,)
    end_date_tuple = (1483228800,)
    serial_running = False
    runs = 1
    relative_path = '../../../'
    credential_file = relative_path + "credentials.txt"
    input_table_name = 'significative_enriched_data'
    output_table_name = 'dj_cluster_stay_points'
    cluster_reference_table = None
    dist_function = None
    runentry_table = AlgorithmUtils.dj_runentry_tableName
    speed_tuple = (0.30,)
    log_file = "log.txt"
    droptable_tuple = (False,)
    dist_thr_tuple = (20, )
    window_size_tuple = (20, )
    min_value_tuple = (None, )
    max_value_tuple = (None, )
    parameter_name_tuple = (None, )
    type_id_tuple = (1000,)
    csv_file_name = join(relative_path, r"data/sampleMostSignificative5000.csv-enriched.txt")
    png_file_name = join(relative_path, r"data/2017-07-02.png")

    algorithm_to_run = Runner.algorithm_to_run_DJC
    draw_input_table = 'dj_cluster_stay_points_significative'
    if len(sys.argv) > 1:
        with open(sys.argv[1]) as configfile:
            content = configfile.read()
            if content:
                exec(content)

    runner = Runner(runs=runs, algorithm_to_run=algorithm_to_run, log_file=log_file, serial_running_param=serial_running)
    if algorithm_to_run == Runner.algorithm_to_run_DJC:
        runner.set_spatial_parameters(id_name_tuple=id_name_tuple, min_accuracy_tuple=min_accuracy_tuple, speed_tuple=speed_tuple, relative_path=relative_path,
                                      droptable_tuple=droptable_tuple, significativeid_tuple=significativeid_tuple, start_date_tuple=start_date_tuple,
                                      end_date_tuple=end_date_tuple, input_table_name=input_table_name, output_table_name=output_table_name,
                                      cluster_reference_table=cluster_reference_table, runentry_table=runentry_table, dist_function=dist_function)
        runner.set_dj_params(eps_tuple=eps_tuple, min_pts_tuple=min_pts_tuple)
    elif algorithm_to_run == Runner.algorithm_to_run_CE:
        runner.set_spatial_parameters(id_name_tuple=id_name_tuple, min_accuracy_tuple=min_accuracy_tuple, speed_tuple=speed_tuple, relative_path=relative_path,
                                      droptable_tuple=droptable_tuple, significativeid_tuple=significativeid_tuple, start_date_tuple=start_date_tuple,
                                      end_date_tuple=end_date_tuple, input_table_name=input_table_name, output_table_name=output_table_name,
                                      cluster_reference_table=cluster_reference_table, runentry_table=runentry_table, dist_function=dist_function)
        runner.set_ce_params(dist_thr_tuple=dist_thr_tuple, window_size_tuple=window_size_tuple, min_value_tuple=min_value_tuple,
                             max_value_tuple=max_value_tuple, parameter_name_tuple=parameter_name_tuple, type_id_tuple=type_id_tuple)

    elif algorithm_to_run == Runner.algorithm_to_run_DRAW_DJ:
        runner.set_draw_dj_params(significativeid_tuple=significativeid_tuple, credential_file=credential_file, draw_input_table=draw_input_table)
    elif algorithm_to_run == Runner.algorithm_to_run_DRAW_CE:
        runner.set_draw_ce_params(dist_thr_tuple=dist_thr_tuple, window_size_tuple=window_size_tuple, min_value_tuple=min_value_tuple,
                                  max_value_tuple=max_value_tuple, parameter_name_tuple=parameter_name_tuple, speed_tuple=speed_tuple,
                                  csvfilename=csv_file_name, min_accuracy_tuple=min_accuracy_tuple, png_path=png_file_name)
    runner.run_algortihm()
