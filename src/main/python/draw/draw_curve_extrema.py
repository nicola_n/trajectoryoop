"""Draw curve extrema segments data."""
import numpy as np
import pandas as pd

from src.main.python.draw.draw import Draw
from src.main.python.algorithm import algorithm_common, curve_extrema
from src.main.python.mydata import main


class DrawCurveExtrema:
    """Draw class for curve extrema reports."""

    def __init__(self, min_value, max_value, parameter, csvfilename, dist_thr, window_size, speed_threshold, min_accuracy, png_path):
        """Params initialization."""
        self.min_value = min_value
        self.max_value = max_value
        self.parameter = parameter
        self.csvfilename = csvfilename
        self.dist_thr = dist_thr
        self.window_size = window_size
        self.speed_threshold = speed_threshold
        self.min_accuracy = min_accuracy
        self.png_path = png_path

    @staticmethod
    def draw_concat_sequences(filteredValues):
        """Draw sequences concatenating them."""
        exampleDataConcatSequence = {"x": [], "y": [], "id": []}
        lastTimestBase = 0
        previousDistDiff = 0
        id = 0
        for f in filteredValues:
            timestBase = f[0]["timest"]
            for point in f:
                exampleDataConcatSequence["x"].append(point["timest"] - timestBase + lastTimestBase)
                exampleDataConcatSequence["y"].append(point["distdiff"] + previousDistDiff)
                exampleDataConcatSequence["id"].append(id)
                previousDistDiff += point["distdiff"]
            lastTimestBase = f[-1]["timest"] - timestBase + lastTimestBase
            id += 1
        Draw.draw_connected_points(exampleDataConcatSequence["x"], exampleDataConcatSequence["y"], exampleDataConcatSequence["id"])

    @staticmethod
    def draw_x_overlapped_sequences(filteredValues):
        """Draw sequences overlapping them on the x axis."""
        exampleDataConcatSequence = {"x": [], "y": [], "id": []}
        previousDistDiff = 0
        id = 0
        for f in filteredValues:
            timestBase = f[0]["timest"]
            for point in f:
                exampleDataConcatSequence["x"].append(point["timest"] - timestBase)
                exampleDataConcatSequence["y"].append(point["distdiff"] + previousDistDiff)
                exampleDataConcatSequence["id"].append(id)
                previousDistDiff += point["distdiff"]
            id += 1
        Draw.draw_connected_points(exampleDataConcatSequence["x"], exampleDataConcatSequence["y"], exampleDataConcatSequence["id"])

    @staticmethod
    def draw_x_y_overlapped_sequences_by_space(filteredValues):
        """Draw separated sequences on the same graph."""
        exampleDataConcatSequence = {"x": [], "y": [], "id": []}
        id = 0
        for f in filteredValues:
            timestBase = f[0]["timest"]
            previousDistDiff = 0
            for point in f:
                exampleDataConcatSequence["x"].append(point["timest"] - timestBase)
                exampleDataConcatSequence["y"].append(point["distdiff"] + previousDistDiff)
                exampleDataConcatSequence["id"].append(id)
                previousDistDiff += point["distdiff"]
            id += 1
        Draw.draw_connected_points(exampleDataConcatSequence["x"], exampleDataConcatSequence["y"], exampleDataConcatSequence["id"])

    @staticmethod
    def draw_normalized_sequences_on_map(filteredValues):
        """Draw separated sequences on the same graph."""
        exampleDataConcatSequence = {"x": [], "y": [], "id": []}
        id = 0
        for f in filteredValues:
            for point in f:
                exampleDataConcatSequence["x"].append(point["longitude"])
                exampleDataConcatSequence["y"].append(point["latitude"])
                exampleDataConcatSequence["id"].append(id)
            id += 1
        Draw.draw_connected_points(exampleDataConcatSequence["x"], exampleDataConcatSequence["y"], exampleDataConcatSequence["id"], False)

    @staticmethod
    def draw_staypoints(staypoints):
        """Draw separated sequences on the same graph."""
        data = {"x": [], "y": [], "id": []}
        for staypoint in staypoints:
            data["x"].append(staypoint["longitude"])
            data["y"].append(staypoint["latitude"])
            data["id"].append(staypoint["id"])
        Draw.draw(data["x"], data["y"], data["id"])

    @staticmethod
    def example_data_draw_ce_stay_points_from_db():
        """Draw Curve extrema stay points from db."""
        minValue = 1
        maxValue = 4
        parameter = "dayhour"
        speedthreshold = 0.28
        distThr = 20
        windowSize = 20
        typeid = 11
        minAccuracy = 28

        cea = curve_extrema.CurveExtremaAlgorithm(
            min_value=minValue, max_value=maxValue, parameter_name=parameter, max_speed=speedthreshold, dist_thr=distThr,
            window_size=windowSize, type_id=typeid, min_accuracy=minAccuracy, significative_id=None,
            input_table_name=None, output_table_name=algorithm_common.ce_tableName, droptable=None, start_date=None, end_date=None,
            cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)

        stayPointsList = cea.get_stay_points_from_db()
        rows = stayPointsList.getData()

        def rowsGen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rowsGen()
        DrawCurveExtrema.draw_staypoints(generator)
        Draw.draw_img("../../../../data/2017-07-02.png", 11.36939, 11.376495, 44.503346, 44.506693)

    def example_data_draw_ce_stay_points_from_5000_csv(self):
        """Draw Curve extrema stay points from csv."""
        cea = curve_extrema.CurveExtremaAlgorithm(
            min_value=self.min_value, max_value=self.max_value, parameter_name=self.parameter, max_speed=self.speed_threshold, dist_thr=self.dist_thr,
            window_size=self.window_size, type_id=1000, min_accuracy=self.min_accuracy, significative_id=None,
            input_table_name=None, output_table_name=algorithm_common.ce_tableName, droptable=None, start_date=None, end_date=None,
            cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None, id_name="timest")

        stayPointsList = cea.create_stay_points_from_enriched_csv(csv_file_name=self.csvfilename)
        # rows = pd.DataFrame(np.array(pd.read_csv(filename, delimiter=';', skiprows=1, names=names)), columns=names).transpose()
        # stayPointsList = curve_extrema.get_stay_points_from_db(minValueFilter, maxValueFilter, parameterFilter, speedthreshold, distThr, windowSize)
        rows = stayPointsList.getData()

        def rows_gen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rows_gen()
        DrawCurveExtrema.draw_staypoints(generator)
        Draw.draw_img("../../../../data/2017-07-02.png", 11.36939, 11.376495, 44.503346, 44.506693)

    def example_draw_ce_raw_values_from_5000_csv(self):
        """Draw raw values from csv."""
        names = main.get_names(self.csvfilename)
        rows = pd.DataFrame(np.array(pd.read_csv(self.csvfilename, delimiter=';', skiprows=1, names=names)), columns=names)

        def rowsGen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rowsGen()
        filteredValues = [x for x in algorithm_common.filter_function(self.max_value, self.max_value, self.parameter, generator)]
        DrawCurveExtrema.draw_concat_sequences(filteredValues)
        DrawCurveExtrema.draw_x_overlapped_sequences(filteredValues)
        DrawCurveExtrema.draw_x_y_overlapped_sequences_by_space(filteredValues)

    def example_draw_ce_normalized_values_from_5000_csv(self):
        """Draw normalized values from csv."""
        names = main.get_names(self.csvfilename)
        rows = pd.DataFrame(np.array(pd.read_csv(self.csvfilename, delimiter=';', skiprows=1, names=names)), columns=names)

        def rowsGen():
            for i in range(len(rows)):
                yield rows.iloc[i]

        generator = rowsGen()
        sequences = algorithm_common.filter_function(self.min_value, self.max_value, self.parameter, generator)

        cea = curve_extrema.CurveExtremaAlgorithm(
            min_value=None, max_value=None, parameter_name=None, max_speed=None, dist_thr=self.dist_thr,
            window_size=self.window_size, type_id=None, min_accuracy=None, significative_id=None,
            input_table_name=None, output_table_name=algorithm_common.ce_tableName, droptable=None, start_date=None, end_date=None,
            cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None, id_name="timest")

        normalizedValues = [x for x in cea.normalize_sequences(sequences)]
        DrawCurveExtrema.draw_x_y_overlapped_sequences_by_space(normalizedValues)
        DrawCurveExtrema.draw_normalized_sequences_on_map(normalizedValues)
        Draw.draw_img(self.png_path, 11.36939, 11.376495, 44.503346, 44.506693)


if __name__ == "__main__":
    filename = '../../../../data/sampleMostSignificative5000.csv-enriched.txt'
    png_path = "../../../../data/2017-07-02.png"
    dce = DrawCurveExtrema(min_value=1, max_value=4, parameter="dayhour", csvfilename=filename, dist_thr=20, window_size=20, speed_threshold=0.28,
                           min_accuracy=25, png_path=png_path)
    dce.example_draw_ce_raw_values_from_5000_csv()
    dce.example_draw_ce_normalized_values_from_5000_csv()
    dce.example_data_draw_ce_stay_points_from_5000_csv()
