-- get users whose number of ping is above 4th quartile
select deviceid
from (
    select deviceid, ntile(4) over (order by freq) as n
    from (
            select deviceid, count(*) freq
            from alldatafiltered
            group by deviceid
         )
         groupbydevice
    ) quartile
where n = 4;

-- export most significative device to csv
copy (
        select *
        from alldatafiltered
        where id_significative <= 1
      ) to 'C:\trajectory\sampleMostSignificative.csv' with csv delimiter ';';

-- creation of a linestring from 2 points
ST_MakeLine(ST_SetSRID(ST_MakePoint(longitude, latitude),4326), ST_SetSRID(ST_MakePoint(longitude + 0.01, latitude+ 0.01),4326))


select slope, bearing, longitude, latitude, ctid, autoincremented, *
from significative_enriched_data
where latitude between 44.502002728079994 - 0.001 and 44.502002728079994 + 0.001
and longitude between 11.396630917663515 - 0.001 and 11.396630917663515 + 0.001
limit 1000

alter table significative_enriched_data add column autoincremented bigserial


select slope, bearing, longitude, latitude, ctid, autoincremented, *
from significative_enriched_data
where autoincremented between 38165 and 38175

ALTER TABLE aree_statistiche11 ADD COLUMN geom4326 geometry(Geometry,4326);
UPDATE aree_statistiche11 SET geom4326 = ST_Transform(geom, 4326);
-- 32632 ESA (European SA)
-- 23032 ISTAT (ED50 / UTM zone 32N)
-- 4326 Google
-- 25832 ???

select * from aree_statistiche11 where ST_Contains(geom4326, ST_PointFromText('POINT(11.372449 44.505005)', 4326));
select significativeid, redditoimponibileirpef from user_features, aree_statistiche11 where ST_Contains(aree_statistiche11.geom4326, user_features.abitain);
insert into user_profiles(significativeid, redditomedio) select significativeid, redditoimponibileirpef from user_features, aree_statistiche11 where ST_Contains(aree_statistiche11.geom4326, user_features.abitain);
select * from planet_osm_point where housenumber is not null or amenity is not null or ref is not null;

select significativeid, way as poi, 20 as typeid from user_features, planet_osm_point where ST_DWITHIN(way, lavorain, 0.0003) and significativeid = 1
union
select significativeid, lavorain as poi, 2 as typeid from user_features where significativeid = 1
union
select significativeid, way as poi, 10 as typeid from user_features, planet_osm_point where ST_DWITHIN(way, abitain, 0.0003) and significativeid = 1
union
select significativeid, abitain as poi, 1 as typeid from user_features where  significativeid = 1;

select min(redditoimponibileirpef/ncontribuenti), max(redditoimponibileirpef/ncontribuenti), avg(redditoimponibileirpef/ncontribuenti) from aree_statistiche11;
alter table aree_statistiche11 add column redditoimponibileirpefprocapite double precision;
update aree_statistiche11 set redditoimponibileirpefprocapite = redditoimponibileirpef/ncontribuenti;
alter table aree_statistiche11 add column redditoimponibileirpefdesc varchar(20);
update aree_statistiche11
    set redditoimponibileirpefdesc = case when redditoimponibileirpefprocapite < x.v1 then 0
                                          when redditoimponibileirpefprocapite > x.v2 then 2
                                          else 1 end
    from (select (max(redditoimponibileirpefprocapite) - min(redditoimponibileirpefprocapite))*1/3 + min(redditoimponibileirpefprocapite) as v1,
                 (max(redditoimponibileirpefprocapite) - min(redditoimponibileirpefprocapite))*2/3 + min(redditoimponibileirpefprocapite) as v2
          from aree_statistiche11) x;
select * from aree_statistiche11;
