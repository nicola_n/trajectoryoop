"""MyThreadPool and threading related methods."""
from multiprocessing import Pool
import os


class MyThreadPool():
    """Thread pool class for multithreading management."""

    def __init__(self, workFunction, workers=None):
        """Init instance variables."""
        self.work_function = workFunction
        if workers is None:
            self.workers = os.cpu_count()
        else:
            self.workers = workers

    def do_work(self, inputs):
        """Start threading work."""
        pool = Pool(self.workers)
        result = []
        for input in inputs[0]:
            result.append(pool.apply_async(self.work_function, (input, inputs[1:])))

        for x in result:
            yield x.get()
