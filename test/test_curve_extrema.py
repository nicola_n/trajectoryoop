"""Curve Extrema test module."""
import unittest

import test_common as tc

from src.main.python.algorithm import algorithm_common, curve_extrema


class CurveExtremaTestCase(unittest.TestCase):
    """Test class."""

    rows = {0: {'significativeid': 1, 'dayhour': 10, "speed": 0}, 1: {'significativeid': 1, 'dayhour': 11, "speed": 0.5},
            2: {'significativeid': 1, 'dayhour': 13, "speed": 2}, 3: {'significativeid': 1, 'dayhour': 15, "speed": 0},
            4: {'significativeid': 1, 'dayhour': 16, "speed": 0.5}, 5: {'significativeid': 1, 'dayhour': 17, "speed": 2},
            6: {'significativeid': 2, 'dayhour': 10, "speed": 0}, 7: {'significativeid': 2, 'dayhour': 11, "speed": 0},
            8: {'significativeid': 2, 'dayhour': 12, "speed": 1}, 9: {'significativeid': 2, 'dayhour': 14, "speed": 4},
            10: {'significativeid': 2, 'dayhour': 16, "speed": 0}, 12: {'significativeid': 2, 'dayhour': 17, "speed": 1}}

    @staticmethod
    def test_filter_function_with_df():
        """Test class."""
        dfL = tc.CommonTestCase.build_df_from_strings(tc.CommonTestCase.headers, tc.CommonTestCase.c)

        def rowsGen():
            for i in range(len(dfL)):
                yield dfL.iloc[i]

        generator = rowsGen()
        result = [x for x in algorithm_common.filter_function(1, 4, "dayhour", generator)]
        assert (len(result) == 2)
        assert (len(result[0]) == 3)
        assert (len(result[1]) == 3)
        assert (result[0][0]["significativeid"] == 2)
        assert (result[1][0]["significativeid"] == 3)
        assert (result[0][1]["dayhour"] == 2)
        assert (result[1][1]["dayhour"] == 2)
        assert (result[0][0]["latitude"] == 44.5045606)
        assert (result[0][2]["latitude"] == 44.5045606)
        assert (result[1][0]["latitude"] == 44.5115606)
        assert (result[1][2]["latitude"] == 44.5115606)

    @staticmethod
    def test_normalize_sequence_with_df():
        """Test normalize sequence with multiple input DataFrame."""
        test_input = {"s": (tc.CommonTestCase.c, tc.CommonTestCase.c2, tc.CommonTestCase.c3, tc.CommonTestCase.c2),
                      "windows_size": [1, 1, 3, 3]}
        output = {"lat": [[44.5045606, 44.5115606], [44.5045606, 44.5115606], None, [44.5045406, 44.5115406]], "lenresult": [2, 2, 1, 2],
                  "lenresult_0": [1, 1, 3, 3], "lenresult_1": [1, 1, None, 3], "deviceid_0": [2, 2, 2, 2], "deviceid_1": [3, 3, None, 3],
                  "hour_0": [2, 2, 4, 2], "hour_1": [2, 2, None, 2]}
        for i in range(len(test_input["s"])):

            dfL = tc.CommonTestCase.build_df_from_strings(tc.CommonTestCase.headers, test_input["s"][i])

            def rowsGen():
                for g in range(len(dfL)):
                    yield dfL.iloc[g]

            generator = rowsGen()
            cea = curve_extrema.CurveExtremaAlgorithm(
                min_value=None, max_value=None, parameter_name=None, max_speed=None, dist_thr=20,
                window_size=test_input["windows_size"][i], type_id=None, id_name=None, min_accuracy=None, significative_id=None,
                input_table_name=None, output_table_name=None, droptable=None, start_date=None, end_date=None,
                cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
            result = [x for x in cea.normalize_sequences(algorithm_common.filter_function(1, 4, "dayhour", generator))]
            if output["lenresult"][i] is not None:
                assert (len(result) == output["lenresult"][i])
            if output["lenresult_0"][i] is not None:
                assert (len(result[0]) == output["lenresult_0"][i])
            if output["lenresult_1"][i] is not None:
                assert (len(result[1]) == output["lenresult_1"][i])
            if output["deviceid_0"][i] is not None:
                assert (result[0][0]["significativeid"] == output["deviceid_0"][i])
            if output["deviceid_1"][i] is not None:
                assert (result[1][0]["significativeid"] == output["deviceid_1"][i])
            if output["hour_0"][i] is not None:
                assert (result[0][0]["dayhour"] == output["hour_0"][i])
            if output["hour_1"][i] is not None:
                assert (result[1][0]["dayhour"] == output["hour_1"][i])
            if output["lat"][i] is not None:
                epsilon = 0.0000001
                assert (abs(result[0][0]["latitude"] - output["lat"][i][0]) < epsilon)
                assert (abs(result[1][0]["latitude"] - output["lat"][i][1]) < epsilon)

    @staticmethod
    def test_raw_staypoints_curve_extrema_with_df():
        """Test raw staypoints with multiple DataFrame."""
        test_input = {"s": (tc.CommonTestCase.c3, tc.CommonTestCase.c3), "windows_size": [3, 2]}
        output = {"lat": [44.504623, 44.5046237], "lenresult_0": [3, 2]}
        for inputCounter in range(len(test_input["s"])):

            dfL = tc.CommonTestCase.build_df_from_strings(tc.CommonTestCase.headers, test_input["s"][inputCounter])

            def rowsGen():
                for i in range(len(dfL)):
                    yield dfL.iloc[i]

            generator = rowsGen()
            cea = curve_extrema.CurveExtremaAlgorithm(
                min_value=1, max_value=4, parameter_name="dayhour", max_speed=0.28, dist_thr=20,
                window_size=test_input["windows_size"][inputCounter], type_id=None, id_name=None, min_accuracy=None, significative_id=None,
                input_table_name=None, output_table_name=None, droptable=None, start_date=None, end_date=None,
                cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
            result = [x for x in cea.raw_staypoints_curve_extrema(generator)]
            assert (len(result) == 1)
            assert (len(result[0]) == output["lenresult_0"][inputCounter])
            assert (result[0][0]["significativeid"] == 2)
            assert (result[0][0]["dayhour"] == 4)
            epsilon = 0.000001
            assert (abs(result[0][-1]["latitude"] - output["lat"][inputCounter]) < epsilon)

    def test_filter_function(self):
        """Test filter function with df."""
        result = [x for x in algorithm_common.filter_function(11, 16, "dayhour", self.rows.values())]
        assert (result[0][1]["significativeid"] == 1)
        assert (result[1][0]["significativeid"] == 2)
        assert (result[0][1]["dayhour"] == 13)
        assert (result[1][1]["dayhour"] == 12)

    @staticmethod
    def test_curve_extrema_algorithm():
        """Test raw staypoints with multiple DataFrame."""
        typeid = 11
        test_input = {"s": (tc.CommonTestCase.c2, tc.CommonTestCase.c3, ), "windows_size": [3, 3, ]}
        output = {"lenresult": [2, 1], "timespent": [188, 1607], "staypointhits": [3, 6]}
        for inputCounter in range(len(test_input["s"])):

            dfL = tc.CommonTestCase.build_df_from_strings(tc.CommonTestCase.headers, test_input["s"][inputCounter])

            def rowsGen():
                for i in range(len(dfL)):
                    yield dfL.iloc[i]

            generator = rowsGen()
            cea = curve_extrema.CurveExtremaAlgorithm(
                min_value=1, max_value=4, parameter_name="dayhour", max_speed=3, dist_thr=20,
                window_size=test_input["windows_size"][inputCounter], type_id=typeid, id_name=None, min_accuracy=None, significative_id=None,
                input_table_name=None, output_table_name=algorithm_common.ce_tableName, droptable=None, start_date=None, end_date=None,
                cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
            result = cea.curve_extrema_algorithm(generator)
            result = cea.create_mydata_from_dataframe(runid=0).getData()
            assert (len(result) == output["lenresult"][inputCounter])
            assert (result["significativeid"][0] == 2)
            assert (result["timespent"][0] == output["timespent"][inputCounter])
            assert (result["staypointhits"][0] == output["staypointhits"][inputCounter])

            # here I select the column on device id and I subtract each value by the previous one (shift(1)),
            # then I fill the nan values(first one) with 0
            # and I select the dataFrame rows which result is not 0
            # the result is the rows with different significativeid from the previous
            # then I check distdiff on this rows should be 0
            assert(result[(result['significativeid'] - result['significativeid'].shift(1)).fillna(value=0) != 0]['distdiff'] == 0).all()

    @staticmethod
    def test_ce_get_spatial_search_query():
        """Test the spatial search query string."""
        ce = curve_extrema.CurveExtremaAlgorithm(dist_thr=11, window_size=12, min_value=13, max_value=14, parameter_name=15, type_id=16, id_name='1',
                                                 min_accuracy=2, max_speed=3, droptable=None, significative_id=4,
                                                 start_date=5, end_date=6, input_table_name='7', output_table_name='8',
                                                 cluster_reference_table=None, runentry_table='10', relativepath="", dist_function=None)
        expected = 'select * from 10 where significativeid = 4 and accuracy = 2 and speed = 3' \
                   ' and startdate = 5 and enddate = 6 and inputtablename = \'7\' and idname = \'1\' and distthr = 11' \
                   ' and windowsize = 12 and minvalue = \'13\' and maxvalue = \'14\' and parametername = \'15\' and typeid = 16;'
        assert (ce.get_runentry_search_query() == expected)
