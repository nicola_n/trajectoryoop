"""Test db connection."""
import unittest
import psycopg2
import psycopg2.extras
import numpy as np


class TestDb(unittest.TestCase):
    """Test class."""

    def testConnection(self):
        with open("./credentials_postgress.txt", "r") as file:
            connectionstring = file.read()
            with psycopg2.connect(connectionstring) as conn:
                with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                    cur.execute("""SELECT * FROM significativedata WHERE date_part('dayhour', to_timestamp(timest)) BETWEEN 3 AND 4
                                AND id_significative = 1 AND accuracy BETWEEN 1 AND 100""")
                    rows = cur.fetchall()

                    assert(rows[0]["id_significative"] == 1)
                    assert (rows[1]["accuracy"] >= 1 or rows[1]["accuracy"] <= 100)
                    assert (rows[3]["timest"] > 1470000000)

    def testInsert(self):
        tup = ((x, x, str(x)) for x in range(1000))
        with open("./credentials_postgress.txt", "r") as file:
            connectionstring = file.read()
            with psycopg2.connect(connectionstring) as conn:
                with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                    cur.execute("select exists(select * from information_schema.tables where table_name=%s)", 'test_table')
                    if cur.fetchone()[0]:
                        cur.execute("DROP TABLE test_table;")
                    conn.commit()
                    cur.execute("CREATE TABLE test_table (id serial PRIMARY KEY, num integer, data varchar);")
                    # xy = [cur.mogrify("(%s,%s,%s)", x) for x in tup]
                    # args_str = ','.join(cur.mogrify("(%s,%s,%s)", x) for x in tup)
                    # cur.execute("INSERT INTO test_table (id, num, data) VALUES " + args_str)
                    cur.executemany("INSERT INTO test_table VALUES(%s,%s,%s)", tup)
                    conn.commit()

    def testInsertFromCsv(self):
        path = './data/enriched.csv'
        data = np.genfromtxt(path, delimiter=';', usecols=(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11),
                             dtype=(np.float, np.float, np.float, np.float, np.float, np.float, np.float, np.float, np.float,
                                    np.float, np.float, np.float))

        sqlCreateTableString = """CREATE TABLE test_table
        (
          significativeid int NOT NULL,
          timest double precision NOT NULL,
          latitude double precision,
          longitude double precision,
          accuracy double precision,
          id_significative double precision,
          segmentId double precision,
          distInc double precision,
          timeInc double precision,
          distDiff double precision,
          timeDiff double precision,
          speed double precision,
          still double precision,
          geominverted geometry(Point,4326),
          CONSTRAINT test_table_pkey PRIMARY KEY (significativeid, timest)
        )"""

        with open("./credentials_postgress.txt", "r") as file:
            connectionstring = file.read()
            with psycopg2.connect(connectionstring) as conn:
                with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                    cur.execute("select exists(select * from information_schema.tables where table_name=%s)",
                                ('test_table',))
                    if cur.fetchone()[0]:
                        cur.execute("DROP TABLE test_table;")
                    conn.commit()
                    cur.execute(sqlCreateTableString)

                    cur.executemany("""INSERT INTO test_table (significativeid, timest, latitude, longitude, accuracy,
                                    segmentId, distInc, timeInc, distDiff, timeDiff, speed)
                                    VALUES(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)""", data)


if __name__ == "__main__":
    unittest.main()  # run all tests
