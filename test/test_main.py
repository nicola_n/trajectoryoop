"""Test main.py."""
import math
import unittest

import numpy as np
import pandas as pd
from pandas.util.testing import assert_frame_equal

from src.main.python.mydata import main


class SimpleTestCase(unittest.TestCase):
    """Test class."""

    def testTrue(self):
        assert True

    def testNumpy(self):
        """Disabled because the dtype of np array changes on diffent machines (int32 or int64)."""
        # TODO Disabled because the dtype of np array changes on diffent machines (int32 or int64).
        # assert (np.array([0, 1, 2]).dtype == 'int64')
        assert (str(np.array([0, 1, 2])) == '[0 1 2]')
        assert (np.array([0, 1.0, 2]).dtype == 'float64')
        assert (str(np.array([0, 1.0, 2])) == "[ 0.  1.  2.]")
        assert (np.array([0, 1.0, '2']).dtype == '<U32')
        assert (str(np.array([0, 1.0, '2'])) == "['0' '1.0' '2']")

    def testSumOverSegment(self):
        data = np.array([0, 1, 2, 3])
        assert (main.sum_over_segment(data) == np.array([0, 1, 3, 6])).all()

    def testAboveThr(self):
        data = np.array([0, 1, 2])
        assert (main.above_threshold(data, 1) == np.array([False, False, True])).all()

    def testBelowThr(self):
        data = np.array([0, 1, 2])
        assert (main.below_threshold(data, 1) == np.array([True, False, False])).all()

    def testCombineSegmentation(self):
        data = np.array([[0, 0], [0, 1], [0, 300], [0, 301], [1, 302], [1, 401], [2, 402]])
        assert (main.compute_id(np.logical_or(main.segment_contiguous_time(data[:, 1], 10), main.segment_user(data[:, 0]))) ==
                np.array([0, 0, 1, 1, 2, 3, 4])).all()

    def testMillisToHour(self):
        data = np.array([1478647696, 1478524301, 1478546889])
        assert (main.millis_to_hour(data) == np.array([0, 14, 20])).all()

    def testMillisToDay(self):
        data = np.array([1478647696, 1478524301, 1478747009])
        assert (main.millis_to_day(data) == np.array([2, 0, 3])).all()

    def testTimeSegmentation(self):
        data = np.array([[0, 0], [0, 1], [0, 300], [0, 301], [1, 302], [1, 401], [2, 402]])
        assert (main.compute_id(main.segment_contiguous_time(data[:, 1], 10)) == np.array([0, 0, 1, 1, 1, 2, 2])).all()

    def testSegmentUser(self):
        data = np.array([[0, 0], [0, 1], [0, 300], [0, 301], [1, 302], [1, 401], [2, 402]])
        assert (main.compute_id(main.segment_user(data[:, 0])) == np.array([0, 0, 0, 0, 1, 1, 2])).all()

    def testComputeId(self):
        assert (main.compute_id(np.array([True, True, True, False, True, False])) == np.array([0, 1, 2, 2, 3, 3])).all()

    def testGetSegments(self):
        data = np.array([[0, 0], [0, 0], [0, 1], [0, 1], [1, 2], [1, 3]])
        for a, b in zip(main.get_segments(data, data[:, 1], True),
                        [np.array([[0, 0], [0, 0]]), np.array([[0, 1], [0, 1]]),
                         np.array([[1, 2]]), np.array([[1, 3]])]):
            assert (a == b).all()

    def testGetSegments2(self):
        data = np.array([[0, 0], [0, 0], [0, 0]])
        for a, b in zip(main.get_segments(data, data[:, 1], True), np.array([[0, 0], [0, 0], [0, 0]])):
            assert (a == b).all()

    def testComputeFirst(self):
        data = np.array([[0, 0], [0, 0], [0, 1], [0, 1], [1, 2], [1, 3]])
        for a, b in zip(main.get_segments(data, data[:, 1], True),
                        [np.array([1, 0]), np.array([1, 0]), np.array([1]), np.array([1])]):
            assert (main.compute_first(a) == b).all()

    def testComputeLast(self):
        data = np.array([[0, 0], [0, 0], [0, 1], [0, 1], [1, 2], [1, 3]])
        for a, b in zip(main.get_segments(data, data[:, 1], True),
                        [np.array([0, 1]), np.array([0, 1]), np.array([1]), np.array([1])]):
            assert (main.compute_last(a) == b).all()

    def testComputeIndex(self):
        data = np.array([[0, 0], [0, 0], [0, 1], [0, 1], [1, 2], [1, 3]])
        for a, b in zip(main.get_segments(data, data[:, 1], True),
                        [np.array([0, 1]), np.array([0, 1]), np.array([0]), np.array([0])]):
            assert (main.compute_index(a) == b).all()

    def testComputeHaversine(self):
        data = np.array([[45.7597, 4.8422], [48.8567, 2.3508]])
        assert (main.compute_haversine_distance(data[:, 0], data[:, 1]) == np.array([0, 392216.71780659625])).all()

    def testComputeRepeat(self):
        data = np.array([[48.8567, 2.3508], [48.8567, 2.3508], [45.7597, 4.8422], [45.7597, 4.8422], [45.7597, 4.8422],
                         [48.8567, 2.3508]])
        assert (main.compute_repeat(data[:, 0], data[:, 1]) == np.array([1, 2, 1, 0, 3, 1])).all()

    def testComputeRepeat2(self):
        data = np.array([[48.8567, 2.3508], [45.7597, 4.8422], [48.8567, 2.3508]])
        assert (main.compute_repeat(data[:, 0], data[:, 1]) == np.array([1, 1, 1])).all()

    def testComputeTime(self):
        data = np.array([0, 60])
        assert (main.compute_time(data) == np.array([0, 60])).all()

    def testComputeSlope(self):
        assert (main.compute_slope(1 - 3, 0 - 3, 2 - 3, 0 - 3) == 90)
        assert (main.compute_slope(1 - 3, 1 - 3, 2 - 3, 2 - 3) == 45)
        assert (main.compute_slope(0 - 3, 1 - 3, 0 - 3, 2 - 3) == 0)
        assert (main.compute_slope(-1 - 3, 1 - 3, -2 - 3, 2 - 3) == 315)
        assert (main.compute_slope(-1 - 3, 0 - 3, -2 - 3, 0 - 3) == 270)
        assert (main.compute_slope(-1 - 3, -1 - 3, -2 - 3, -2 - 3) == 225)
        assert (main.compute_slope(-2 - 3, -2 - 3, -1 - 3, -1 - 3) == 45)
        assert (main.compute_slope(0 - 3, -1 - 3, 0 - 3, -2 - 3) == 180)
        assert (main.compute_slope(1 - 3, -1 - 3, 2 - 3, -2 - 3) == 135)

    def testComputedSlopes(self):
        data = np.array([[-1, -1], [-1, 0], [0, 0], [1, -1], [1, -2], [0, -3], [-1, -3], [-2, -2], [-2, -1]])
        assert (main.compute_slopes(data[:, 0], data[:, 1]) == np.array([0, 0, 90, 135, 180, 225, 270, 315, 0])).all()

    def testComputeSpeed(self):
        distance = np.array([0, 100, 100])
        time = np.array([0, 10, 20])
        assert (main.compute_speed(distance, time) == np.array([0, 10, 5])).all()

    def testGetHome(self):
        data = np.array([[10, 12], [10, 40], [2, 12], [3, 12], [4, 12], [3, 100], [5, 10]])
        assert (main.getHome(data[:, 0], data[:, 1]) == np.array([0, 0, 1, 1, 1, 0, 0])).all()

    def testGetMorningWork(self):
        data = np.array([[7, 10], [8, 10], [9, 10], [10, 10], [10, 70], [11, 40], [12, 12]])
        assert (main.getMorningWork(data[:, 0], data[:, 1]) == np.array([0, 0, 1, 1, 0, 1, 0])).all()

    def testGetAfternoonWork(self):
        data = np.array([[13, 10], [14, 10], [15, 10], [16, 10], [16, 70], [17, 40], [18, 12]])
        assert (main.getAfternoonWork(data[:, 0], data[:, 1]) == np.array([0, 0, 1, 1, 0, 1, 0])).all()

    def testMyData(self):
        """Base test on MyData object."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = np.array([1])
        d.add('deviceId', 'double', first)
        d.add('segmentId', 'double', second)
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        assert_frame_equal(d.getData(), df)
        assert(d.tableName == "tablename")

    def test_MyData_getTableCreation_with_one_pk(self):
        """Test the table creation sql with one primary key."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = np.array([1])
        d.add('deviceId', 'double', first, pk=True)
        d.add('segmentId', 'double', second)
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        assert (val.split(' primary key')[0] == 'drop table if exists tablename cascade ;create table tablename '
                                                '(deviceId double precision,segmentId double precision,')
        assert (val.split(' primary key')[1] == "(deviceId));")

    def test_MyData_getTableCreation_with_two_pk(self):
        """Test the table creation sql with two primary keys."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = np.array([1])
        d.add('deviceId', 'double', first, pk=True)
        d.add('segmentId', 'double', second, pk=True)
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        assert (val.split(' primary key')[0] == 'drop table if exists tablename cascade ;create table tablename '
                                                '(deviceId double precision,segmentId double precision,')
        assert (val.split(' primary key')[1] == "(deviceId,segmentId));")

    def test_MyData_getTableCreation_with_two_unique_pk(self):
        """Test the table creation sql with two primary keys."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = np.array([1])
        d.add('deviceId', 'double', first, pk=True, unique=True)
        d.add('segmentId', 'double', second, pk=True, unique=True)
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        assert (val.split(' primary key')[0] == 'drop table if exists tablename cascade ;create table tablename '
                                                '(deviceId double precision,segmentId double precision,')
        assert (val.split(' primary key')[1] == "(deviceId,segmentId), unique(deviceId,segmentId));")

    def test_MyData_getTableCreation_with_one_pk_and_one_fk(self):
        """Test the table creation sql with two primary keys."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = np.array([1])
        third = np.array([1])
        d.add('deviceId', 'double', first, pk=True)
        d.add('segmentId', 'double', second)
        d.add('third', 'integer', third, fk=True, ftable='othertable')
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ;third           ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision,third integer')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        df['third'] = third
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        expect = 'drop table if exists tablename cascade ;create table tablename (deviceId double precision,segmentId ' \
                 'double precision,third integer,'
        assert (val.split(' primary key')[0] == expect)
        assert (val.split(' primary key')[1] == "(deviceId), foreign key (third) references othertable(third));")

    def test_MyData_getTableCreation_with_one_pk_and_one_fk_None_Data(self):
        """Test the table creation sql with one autoincrement primary key, one foreign key and none data."""
        d = main.MyData(tableName="tablename", dbtype='pg')
        first = None
        second = np.array([1])
        third = np.array([1])
        d.add('deviceId', 'autoincrement', first, pk=True)
        d.add('segmentId', 'double', second)
        d.add('third', 'integer', third, fk=True, ftable='othertable')
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ;third           ')
        assert (d.getQueryString() == 'deviceId bigserial,segmentId double precision,third integer')
        df = pd.DataFrame()
        df['segmentId'] = second
        df['third'] = third
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        expect = 'drop table if exists tablename cascade ;create table tablename (deviceId bigserial,segmentId ' \
                 'double precision,third integer,'
        assert (val.split(' primary key')[0] == expect)
        assert (val.split(' primary key')[1] == "(deviceId), foreign key (third) references othertable(third));")

    def test_MyData_getTableCreation_with_one_pk_and_two_fk(self):
        """Test the table creation sql with one autoincrement primary key, two foreign key from the same table."""
        d = main.MyData(tableName="tablename", dbtype='pg')
        first = np.array([0])
        second = np.array([1])
        third = np.array([1])
        d.add('deviceId', 'autoincrement', first, pk=True)
        d.add('segmentId', 'double', second, fk=True, ftable='othertable')
        d.add('third', 'integer', third, fk=True, ftable='othertable')
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ;third           ')
        assert (d.getQueryString() == 'deviceId bigserial,segmentId double precision,third integer')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        df['third'] = third
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        expect = 'drop table if exists tablename cascade ;create table tablename (deviceId bigserial,segmentId ' \
                 'double precision,third integer,'
        assert (val.split(' primary key')[0] == expect)
        assert (val.split(' primary key')[1] == "(deviceId), foreign key (segmentId,third) references othertable(segmentId,third));")

    def test_MyData_getTableCreation_with_one_pk_and_two_fk_from_different_tables(self):
        """Test the table creation sql with one autoincrement primary key, two foreign key from different tables."""
        d = main.MyData(tableName="tablename", dbtype='pg')
        first = np.array([0])
        second = np.array([1])
        third = np.array([1])
        d.add('deviceId', 'autoincrement', first, pk=True)
        d.add('segmentId', 'double', second, fk=True, ftable='othertable')
        d.add('third', 'integer', third, fk=True, ftable='zanothertable')
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ;third           ')
        assert (d.getQueryString() == 'deviceId bigserial,segmentId double precision,third integer')
        df = pd.DataFrame()
        df['deviceId'] = first
        df['segmentId'] = second
        df['third'] = third
        assert_frame_equal(d.getData(), df)
        val = d.getTableCreation()
        assert (d.tableName == "tablename")
        expect = 'drop table if exists tablename cascade ;create table tablename (deviceId bigserial,segmentId ' \
                 'double precision,third integer,'
        expect2 = "(deviceId), foreign key (segmentId) references othertable(segmentId), foreign key (third)" \
                  " references zanothertable(third));"
        assert (val.split(' primary key')[0] == expect)
        secondPart = val.split(' primary key')[1]
        if len(val.split(' primary key')[1]) != len(expect2):
            print(val.split(' primary key')[1])
            print(expect2)
        else:
            for i in range(len(expect2)):
                if expect2[i] != secondPart[i]:
                    print(expect2[max(i - 10, 0):min(i + 10, len(expect2))])
                    print(secondPart[max(i - 10, 0):min(i + 10, len(secondPart))])
                    assert (secondPart == expect2)  # this will fail

        assert (secondPart == expect2)

    def testMyData_None_Data(self):
        """Test the None value on data insertion."""
        d = main.MyData("tablename")
        first = np.array([0])
        second = None
        d.add('deviceId', 'double', first)
        d.add('segmentId', 'double', second)
        assert (d.getNamesString(16) == 'deviceId        ;segmentId       ')
        assert (d.getQueryString() == 'deviceId double precision,segmentId double precision')
        df = pd.DataFrame()
        df['deviceId'] = first
        assert_frame_equal(d.getData(), df)
        assert(d.tableName == "tablename")

    def testTypeLookUP(self):
        """Check for right type based on db key."""
        d = main.MyData('table', 'pg')
        assert (d.lookup('double') == 'double precision')
        assert (d.lookup('integer') == 'integer')
        assert (d.lookup('autoincrement') == 'bigserial')
        assert (d.lookup('point') == 'geometry(Point,4326)')
        d = main.MyData('table', 'oracle')
        assert (d.lookup('double') == 'number')
        assert (d.lookup('integer') == 'integer')
        assert (d.lookup('point') == 'sdo_geometry')
        assert (d.lookup('autoincrement') == 'TODO')

    def testDegreesToRadiansConversion(self):
        epsilon = 0.0000001
        assert (abs(main.degrees_to_radians(90) - math.pi / 2) < epsilon)
        assert (abs(main.degrees_to_radians(180) - math.pi) < epsilon)
        assert (abs(main.degrees_to_radians(270) - math.pi * 3 / 2) < epsilon)
        assert (abs(main.degrees_to_radians(360) - math.pi * 2) < epsilon)
        assert (abs(main.degrees_to_radians(45) - math.pi / 4) < epsilon)
        assert (abs(main.degrees_to_radians(30) - math.pi / 6) < epsilon)
        assert (abs(main.degrees_to_radians(60) - math.pi / 3) < epsilon)

    def testRadiansToDegreesConversion(self):
        epsilon = 0.0000001
        assert (abs(main.radians_to_degrees(math.pi / 2) - 90) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi) - 180) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi * 3 / 2) - 270) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi * 2) - 360) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi / 4) - 45) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi / 6) - 30) < epsilon)
        assert (abs(main.radians_to_degrees(math.pi / 3) - 60) < epsilon)

    def testBearing(self):
        epsilon = 0.0000001
        kansas = [39.099912, -94.581213]
        louis = [38.627089, -90.200203]
        kansasRadians = [main.degrees_to_radians(x) for x in kansas]
        louisRadians = [main.degrees_to_radians(x) for x in louis]
        result = main.radians_to_degrees(main.calculate_radians_bearing(kansasRadians, louisRadians))
        expectedResult = 96.51262423499931
        # assert(abs(result - expectedResult) < epsilon)

        bologna1 = [44.497752544040445, 11.339523323318417]
        bologna2 = [44.49774801397928, 11.342385922546327]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in bologna1],
                                                                        [main.degrees_to_radians(x) for x in bologna2]))
        expectedResult = 90.12611470423911
        assert (abs(result - expectedResult) < epsilon)

        epsilon = 0.01
        point1 = [44.4977, 11.33]
        point2 = [44.4977, 11.331]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in point1],
                                                                        [main.degrees_to_radians(x) for x in point2]))
        # east
        expectedResult = 90
        assert (abs(result - expectedResult) < epsilon)

        point1 = [44.4977, 11.33]
        point2 = [44.4977, 11.329]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in point1],
                                                                        [main.degrees_to_radians(x) for x in point2]))
        # west
        expectedResult = 270
        assert (abs(result - expectedResult) < epsilon)

        point1 = [44.4976, 11.33]
        point2 = [44.4977, 11.33]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in point1],
                                                                        [main.degrees_to_radians(x) for x in point2]))
        # north
        expectedResult = 0
        assert (abs(result - expectedResult) < epsilon)

        point1 = [44.4977, 11.33]
        point2 = [44.4967, 11.33]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in point1],
                                                                        [main.degrees_to_radians(x) for x in point2]))
        # south
        expectedResult = 180
        assert (abs(result - expectedResult) < epsilon)

        point1 = [44.5017339, 11.3963919]
        point2 = [44.5045439, 11.3727299]
        result = main.radians_to_degrees(main.calculate_radians_bearing([main.degrees_to_radians(x) for x in point1],
                                                                        [main.degrees_to_radians(x) for x in point2]))
        epsilon = 0.5
        expectedResult = 279.5
        assert (abs(result - expectedResult) < epsilon)

        # filename = '../../../data/sampleMostSignificative.csv-enriched.txt'
        # df = pd.read_csv(filename, delimiter=';', skiprows=1)
        # df.columns = get_names(filename)
        # lat = df['latitude'][38167:38169]
        # lon = df['longitude'][38167:38169]
        # bearing = df['bearing'][38167:38169]
        # print([x for x in zip(lat, lon, compute_bearings(lon.values, lat.values), bearing)])

    def testPrevious(self):
        data = np.array([0, 1, 2, 3])
        assert (main.shift_data(data) == [0, 0, 1, 2]).all()


if __name__ == "__main__":
    unittest.main()  # run all tests
