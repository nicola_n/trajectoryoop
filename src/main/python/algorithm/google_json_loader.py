"""Convert and load the location history from the google json to the db."""
import sys
from datetime import datetime
from os import listdir
from os.path import isfile, join

import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras

from src.main.python.mydata import main


def to_time_stamp(value):
    """Trasform an integer millis timestamp to a date."""
    return datetime.fromtimestamp(to_int(value) / 1000).strftime("%Y-%m-%d %H:%M:%S")


def el7(value):
    """Double * 10^7 to double."""
    return to_int(value) / (10000000)


def activity_c(value):
    """Get the activity name."""
    return "act-{}".format(value)


def to_int(value):
    """Convert to int or print the value if it can't."""
    if type(value) is float:
        return int(value)
    elif all([c.isdigit() for c in value]):
        return int(value)
    elif "," == value[-1]:
        return int(value[:-1])
    else:
        print("'{}'".format(value))
    return 0


activityFileNameSuffix = "activity.json"
exportedcsvSuffix = "exported.csv"


def extractDataFrameFromFile(filename, deviceid, tablename, createTable, credentials):
    """Extract and upload positions."""
    func_association = {
        "timestampMs": to_time_stamp,
        "latitudeE7": el7,
        "longitudeE7": el7,
        "accuracy": to_int,
        "activity": None,
        "velocity": None,
        "altitude": None,
        "heading": None,
        "verticalAccuracy": None
    }
    key_dict = {
        "timestampMs": "timest",
        "latitudeE7": "latitude",
        "longitudeE7": "longitude",
        "accuracy": "accuracy",
        "activity": "activity",
        "velocity": "speed",
        "altitude": "altitude",
        "heading": "heading",
        "verticalAccuracy": "verticalAccuracy"
    }
    with open(filename, 'r') as input:
        activityFileName = filename + activityFileNameSuffix
        with open(activityFileName, 'w') as activityOutput:
            activityCounter = 0
            activitystarted = False
            l = []
            d = {}
            l.append(d)
            input.readline()
            input.readline()
            for line in input:
                activityended = False
                if line == '    "activity" : [ {\n':
                    activityCounter += 1
                    activitystarted = True
                    line = line.replace('    "activity" : [ {\n', '"act-{}": [{{'.format(activityCounter))
                    # d['activity'] = "act-{}".format(activityCounter)
                elif line == '    } ]\n':
                    activitystarted = False
                    activityended = True
                    line = line.replace('    } ]\n', ' }], \n')
                else:
                    line = line.replace('\n', '').replace(" ", "").replace("\"", "")
                if activitystarted or activityended:
                    o = activityOutput.write(line)  # noqa=F841
                else:
                    if line == '},{':
                        d = {}
                        l.append(d)
                    else:
                        x = line.split(":")
                        if len(x) == 2:
                            key, value = x
                            func = func_association[key]
                            if func is not None:
                                d[key_dict[key]] = func(value)
                        else:
                            print("line: {}".format(line))

    df = pd.DataFrame(l)
    df['id'] = df.index
    exportedcsv = filename + exportedcsvSuffix

    stayPointsList = main.MyData(tablename)
    stayPointsList.add('timest', 'timestamp', df["timest"])
    stayPointsList.add('latitude', 'double', df["latitude"])
    stayPointsList.add('longitude', 'double', df["longitude"])
    stayPointsList.add('accuracy', 'integer', df["accuracy"])
    stayPointsList.add('deviceid', 'varchar(10)', np.repeat(deviceid, len(df)))
    stayPointsList.getData().to_csv(path_or_buf=exportedcsv, sep=';', header=False, index=False)
    with open(credentials, 'r') as file:
        with psycopg2.connect(file.read()) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                if createTable:
                    cur.execute(stayPointsList.getTableCreation())
                with open(exportedcsv, 'r') as f:
                    cur.copy_from(f, stayPointsList.tableName, sep=';', columns=('timest', 'latitude', 'longitude', 'accuracy', 'deviceid'))


if __name__ == '__main__':
    rootDir = r"C:\trajectory"
    credential = '../../../credentials_postgress.txt'
    if len(sys.argv) > 2:
        rootDir = sys.argv[1]
        credential = sys.argv[2]
    files = [f for f in listdir(rootDir) if isfile(join(rootDir, f)) and f.endswith("_location.json")]

    createBigData = "drop table if exists bigdata; create table bigdata ( deviceid varchar(10) default ('none'), timest timestamp, latitude double " \
                    "precision, longitude double precision, accuracy integer);"
    insertBigData = "insert into bigdata(timest, latitude, longitude, accuracy, deviceid) select timest, latitude, longitude, accuracy, deviceid from " \
                    "tmp group by timest, latitude, longitude, accuracy, deviceid;"
    createTempBestAccuracy = "create table temp_bestaccuracy as (select deviceid, timest, min(accuracy) accuracy from bigdata group by deviceid, timest);"
    tempBestAccuracyPK = "alter table temp_bestaccuracy add primary key (deviceid, timest);"
    dropBestAccuracy = "drop table if exists temp_bestaccuracy;"

    createBestAccuracyctid = "create table temp_bestaccuracyctid as (select min(ad.ctid) rownumber from bigdata ad, temp_bestaccuracy x where  x.deviceid = " \
                             "ad.deviceid and x.timest = ad.timest and x.accuracy = ad.accuracy group by x.deviceid, x.timest);"
    dropBestAccuracyctid = "drop table if exists temp_bestaccuracyctid;"

    dropBigDataFiltered = "drop table if exists bigdatafiltered;"

    createBigDataFiltered = "create table bigdatafiltered as (" \
                            "select ad.timest, ad.deviceid, ad.latitude, ad.longitude, ad.accuracy," \
                            "ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326) as geom," \
                            "ST_AsText(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as wkt4326," \
                            "ST_AsGeojson(ST_SetSRID(ST_MakePoint(ad.longitude, ad.latitude), 4326)) as geojson " \
                            "from bigdata ad, temp_bestaccuracyctid x " \
                            "where ad.ctid = x.rownumber);"
    bigDataFilteredPK = "alter table bigdatafiltered add primary key (deviceid, timest);"
    exportToCsvQuery = "copy (select * from bigdatafiltered) to stdout with header encoding 'utf8' delimiter ';' csv;"
    exportFile = join(rootDir, 'bigdatafiltered_UTF8_CRLF.csv')

    with open(credential, 'r') as credentials_file:
        with psycopg2.connect(credentials_file.read()) as conn:
            with conn.cursor(cursor_factory=psycopg2.extras.DictCursor) as cur:
                cur.execute(createBigData)
                for f in files:
                    filename = join(rootDir, f)
                    deviceid = f.split('_')[0]
                    extractDataFrameFromFile(filename=filename, deviceid=deviceid, tablename="tmp", createTable=True, credentials=credential)
                    cur.execute(insertBigData)
                    conn.commit()

                # cur.execute(bigDataPK)
                cur.execute(dropBestAccuracy)
                cur.execute("drop table tmp;")
                cur.execute(createTempBestAccuracy)
                cur.execute(tempBestAccuracyPK)
                cur.execute(dropBestAccuracyctid)
                cur.execute(createBestAccuracyctid)
                conn.commit()

                cur.execute(dropBigDataFiltered)
                cur.execute(createBigDataFiltered)
                cur.execute(dropBestAccuracy)
                cur.execute(dropBestAccuracyctid)
                cur.execute(bigDataFilteredPK)
                with open(exportFile, 'w') as f:
                    cur.copy_expert(exportToCsvQuery, f)
                conn.commit()


def export_for_dj(l, file, id):
    """Create the dataframe with latitude, longitude, id and repetition."""
    newD = {}
    for x in l:
        key = (x["latitude"], x["longitude"])
        value = newD.get(key, 0)
        newD[key] = value + 1

    newL = []
    for (latitude, longitude), repetition in newD.items():
        newL.append({"latitude": latitude, "longitude": longitude, "repetition": repetition, "significativeid": id})

    df = pd.DataFrame(newL)
    df.to_csv(path_or_buf=file, sep=';', header=False, index=False)
