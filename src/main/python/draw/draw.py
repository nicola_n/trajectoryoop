"""Draw segmented data."""
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.image as mpimg
from mpl_toolkits.mplot3d import Axes3D  # noqa=F401


THICKNESS = 30
CMAP = 'viridis'


class Draw:
    """Draw class contains base drawing methods."""

    @staticmethod
    def draw(x, y, v):
        """Draw points in empty map."""
        plt.scatter(x, y, THICKNESS, c=v, cmap='viridis')

    @staticmethod
    def draw_segments(x, y, id):
        """Draw segments in empty map."""
        Draw.draw_segments_with_minmax(x, y, id, 0, id[-1])

    @staticmethod
    def draw_segments_with_minmax(x, y, id, min=0, max=1000):
        """Draw segments in empty map."""
        cm = plt.get_cmap(CMAP)
        cNorm = colors.Normalize(vmin=0, vmax=id[-1])
        scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=cm)

        def inner_plot(res):
            plt.plot(res[:, 0], res[:, 1], c=scalarMap.to_rgba(res[:, 2][0]))

        prev = -1
        res = np.empty((0, 3))
        for a, b, c in zip(x, y, id):
            if c != prev:
                if res.size > 0:
                    inner_plot(res)
                prev = c
                res = np.array([[a, b, c]])
            else:
                res = np.append(res, [[a, b, c]], axis=0)
        inner_plot(res)

    @staticmethod
    def draw_connected_points(x, y, id, show=True):
        """Draw point and segments in empty map."""
        Draw.draw(x, y, id)
        Draw.draw_segments(x, y, id)
        if show:
            plt.show()

    @staticmethod
    def draw_img(file, xmin, xmax, ymin, ymax):
        """Draw an image as background."""
        img = mpimg.imread(file)
        plt.imshow(img, zorder=0, extent=[xmin, xmax, ymin, ymax])
        plt.show()

    @staticmethod
    def draw_3d_bar_chart(x, y, z, barcolor='#00FF00', title=None, xlabel=None, ylabel=None):
        """Draw a 3d bar chart."""
        assert(len(x) == len(y) == len(z))

        valuesNum = len(x)
        fig = plt.figure()
        ax1 = fig.add_subplot(111, projection='3d')
        if title is not None:
            ax1.set_title(title)
        if xlabel is not None:
            ax1.set_xlabel(xlabel)
        if ylabel is not None:
            ax1.set_ylabel(ylabel)

        zoffset = np.zeros(valuesNum)

        xthickness = np.ones(valuesNum) / 1.1
        ythickness = np.ones(valuesNum) / 1.1

        ax1.bar3d(x, y, zoffset, xthickness, ythickness, z, color=barcolor)
        plt.show()
