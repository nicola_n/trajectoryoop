drop directory ext_tab_dir;
create directory ext_tab_dir as 'C:\trajectory';

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN LINKEDGEODATA SPARQL
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table ext_linkedgeodata_sparql;
create table ext_linkedgeodata_sparql(
    nome clob,
    nodetype clob,
    typevalue clob,
    ontologylink clob,
    city clob,
    street clob,
    housenumber clob,
    country clob,
    wkt4326 clob
) organization external (
    type oracle_loader
    default directory ext_tab_dir
    access parameters (
        records delimited by '\n'
        skip 0
        fields terminated by '\t'
        lrtrim
        missing field values are null (
            nome char(99999), nodetype char(99999), typevalue char(99999),
            ontologylink char(99999), city char(99999), street char(99999),
            housenumber char(99999), country char(99999), wkt4326 char(99999)
        )
    ) location ('linkedgeodata_data.tsv')
) reject limit unlimited;
drop table linkedgeodata_sparql;
create table linkedgeodata_sparql
    as (select * from ext_linkedgeodata_sparql);
select * from linkedgeodata_sparql where rownum <= 10;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END LINKEDGEODATA SPARQL
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN AREE STATISTICHE
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table ext_aree_statistiche;
create table ext_aree_statistiche (
  areaid integer,
  nome clob,
  ncontribuenti integer,
  imponibileirpef number,
  imponibileaddizionaleirpef number,
  imponibileirpefprocapite number,
  imponibileirpefdesc integer,
  wkt4326 clob,
  geojson clob
) organization external (
    type oracle_loader
    default directory ext_tab_dir
    access parameters (
        records delimited by '\r\n'
        skip 1
        fields terminated by ';' optionally enclosed by '"'
        lrtrim
        missing field values are null (
            areaid char(99999), nome char(99999), ncontribuenti char(99999),
            imponibileirpef char(99999), imponibileaddizionaleirpef char(99999),
            imponibileirpefprocapite char(99999), imponibileirpefdesc char(99999),
            wkt4326 char(99999), geojson char(99999)
        )
    ) location ('aree_statistiche_UTF8_CRLF.csv')
) reject limit 0;
drop table aree_statistiche;
create table aree_statistiche
    as (select * from ext_aree_statistiche);
alter table aree_statistiche add (geom sdo_geometry);
update aree_statistiche set geom =
    (select sdo_cs.transform(geom, 4326) from tmp_aree_statistiche tmp where aree_statistiche.areaid = tmp.cod_areast)
    where geom is null;
alter table aree_statistiche add primary key (areaid);
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'aree_statistiche', 'geom', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(geom, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(geom, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 2)) as maxy
      from aree_statistiche
  ) x;
create index aree_statistiche_idx on aree_statistiche(geom) indextype is MDSYS.SPATIAL_INDEX;
select * from aree_statistiche where rownum <= 10;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END AREE STATISTICHE
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN COMUNI
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table ext_com2016_wgs84;
create table ext_com2016_wgs84 (
  gid integer,
  cod_reg integer,
  cod_cm integer,
  cod_pro integer,
  pro_com integer,
  comune varchar2(58),
  nome_ted varchar2(100),
  flag_cm integer,
  shape_leng integer,
  shape_area integer,
  uri varchar2(100),
  geojson clob
) organization external (
    type oracle_loader
    default directory ext_tab_dir
    access parameters (
        records delimited by '\r\n'
        skip 1
        fields terminated by ';' optionally enclosed by '"'
        lrtrim
        missing field values are null (
            gid char(99999), cod_reg char(99999), cod_cm char(99999),
            cod_pro char(99999), pro_com char(99999), comune char(99999),
            nome_ted char(99999), flag_cm char(99999), shape_leng char(99999),
            shape_area char(99999), uri char(99999), geojson char(99999)
        )
    ) location ('com2016_wgs84_UTF8_CRLF.csv')
) reject limit unlimited;
drop table com2016_wgs84;
create table com2016_wgs84
    as (select * from ext_com2016_wgs84);
alter table com2016_wgs84 add (geom sdo_geometry);
update com2016_wgs84 set geom =
    (select sdo_cs.transform(geom, 4326) from tmp_com2016_wgs84 tmp where tmp.comune = com2016_wgs84.comune and tmp.cod_reg = com2016_wgs84.cod_reg)
    where geom is null;
alter table com2016_wgs84 add primary key (gid);
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'com2016_wgs84', 'geom', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(geom, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(geom, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 2)) as maxy
      from com2016_wgs84
  ) x;
create index com2016_wgs84_idx on com2016_wgs84(geom) indextype is MDSYS.SPATIAL_INDEX;
select * from com2016_wgs84 where rownum <= 10;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END COMUNI
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN OSM POINT
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop table ext_planet_osm_point;
create table ext_planet_osm_point (
  osm_id integer,
  housename varchar(100),
  housenumber varchar(100),
  addrinterpolation varchar(100),
  amenity varchar(100),
  building varchar(100),
  denomination varchar(100),
  leisure varchar(100),
  office varchar(100),
  place varchar(100),
  shop varchar(100),
  sport varchar(100),
  tourism varchar(100),
  latitude number,
  longitude number,
  geojson varchar(100)
) organization external (
    type oracle_loader
    default directory ext_tab_dir
    access parameters (
        records delimited by '\n'
        skip 1
        fields terminated by ';' optionally enclosed by '"'
        lrtrim
        missing field values are null
    ) location ('planet_OSM_point_UTF8_CRLF.csv')
) reject limit 0;
drop table planet_osm_point;
create table planet_osm_point
    as (select * from ext_planet_osm_point);
alter table planet_osm_point add (way sdo_geometry);
update planet_osm_point set way = SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(longitude, latitude, null), null, null);
alter table planet_osm_point add primary key (osm_id);
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'planet_osm_point', 'way', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(way, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(way, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(way, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(way, 2)) as maxy
      from planet_osm_point
  ) x;
create index planet_osm_point_idx on planet_osm_point(way) indextype is MDSYS.SPATIAL_INDEX;
select * from planet_osm_point where rownum <= 10;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END OSM POINT
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- DATABASE CREATION, load alldatafiltered from csv file
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
drop directory ext_tab_dir;
create directory ext_tab_dir as 'C:\trajectory';
drop table ext_alldatafiltered;
create table ext_alldatafiltered (
  timest integer,
  deviceid integer,
  latitude number,
  longitude number,
  accuracy integer, -- noisy data: some accuracies are '\n'
  geom varchar(100),
  wkt4326 varchar(100),
  geojson varchar(100)
) organization external (
    type oracle_loader
    default directory ext_tab_dir
    access parameters (
        records delimited by '\r\n'
        skip 1
        fields terminated by ';' optionally enclosed by '"'
        lrtrim
        missing field values are null
    ) location ('alldatafiltered_UTF8_CRLF.csv')
) reject limit 0;
drop table alldatafiltered;
-- 8 min
create table alldatafiltered as (
    select timest, deviceid, latitude, longitude, accuracy,
             SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(longitude, latitude, null), null, null) as geom,
             wkt4326, geojson
    from ext_alldatafiltered
);
alter table alldatafiltered add constraint alldatafiltered_pkey primary key (deviceid, timest);
alter table alldatafiltered add (
  significativeid integer,
  points integer,
  distinctpoints integer
);
--create table pippo (
--    timest integer,
--    accuracy integer,
--    deviceid integer
--);
--insert into pippo(timest, deviceid, accuracy) values (1,1,1);
--insert into pippo(timest, deviceid, accuracy) values (1,1,1);
--insert into pippo(timest, deviceid, accuracy) values (1,1,2);
--insert into pippo(timest, deviceid, accuracy) values (1,2,1);
--select * from pippo;
--select pl.timest, pl.deviceid, pl.accuracy
--from pippo pl, (
--    select deviceid, timest, min(accuracy) accuracy, min(rowid) as rownumber
--    from pippo
--    group by deviceid, timest
--) x
--where pl.deviceid = x.deviceid
--    and pl.timest = x.timest
--    and pl.accuracy = x.accuracy
--    and pl.rowid = rownumber;
--drop table pippo;

--------------------------------------------------------
-- DO THIS IF YOU CREATED TABLES WITH SYS AS SYSDBA USER
--------------------------------------------------------
--grant all privileges on sys.alldata to mfrancia;
--grant all privileges on sys.alldatafiltered to mfrancia;
--grant all privileges on sys.aree_statistiche to mfrancia;
--grant all privileges on sys.linkedgeodata_sparql to mfrancia;
--alter table alldata move tablespace users;
--alter table alldatafiltered move tablespace users;
--alter table aree_statistiche move tablespace users;
--alter table linkedgeodata_sparql move tablespace users;
--select * from sys.alldata where rownum=1;
--drop synonym alldata;
--create synonym alldata for sys.alldata;
--select * from alldata where rownum=1;
--drop synonym alldatafiltered;
--create synonym alldatafiltered for sys.alldatafiltered;
--select * from alldatafiltered where rownum=1;
--drop synonym aree_statistiche;
--create synonym aree_statistiche for sys.aree_statistiche;
--select * from aree_statistiche where rownum=1;
--drop synonym linkedgeodata_sparq;
--create synonym linkedgeodata_sparql for sys.linkedgeodata_sparql;
--select * from linkedgeodata_sparql where rownum=1;

-- 25 mins
-- https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_9016.htm#SQLRF01606
merge into alldatafiltered ad
using (
    select deviceid, row_number() over (order by points desc) as rowindex, points, distinctpoints
    from (
        select deviceid, count (*) points, count (distinct latitude||longitude) distinctpoints
        from alldatafiltered
        group by deviceid
    )
) sd on (ad.deviceid = sd.deviceid)
when matched then update set ad.significativeid = rowindex, ad.points = sd.points, ad.distinctpoints = sd.distinctpoints;
-- create significative data, a table where only the top 10 people are added
drop table significativedata;
create table significativedata as (
    select *
    from alldatafiltered
    where significativeid < 11
    -- order by significativeid asc, timest asc
);
alter table significativedata add constraint significativedata_pkey primary key (significativeid, timest);
select min(significativeid), min(deviceid), min(points), min(distinctpoints) from significativedata group by significativeid order by significativeid;

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN - CREATE HIERARCHIES POINT
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
create table islocatedin_statarea2comune (
   comuneid integer references com2016_wgs84,
   areaid integer references aree_statistiche,
   primary key (comuneid, areaid)
);
insert into islocatedin_statarea2comune(areaid, comuneid)
    select a.areaid, c.gid as comuneid
    from com2016_wgs84 c, aree_statistiche a where sdo_anyinteract(c.geom, a.geom) = 'TRUE'
    order by areaid, comune;

drop table islocatedin_point;
create table islocatedin_point (
   osmid integer references planet_osm_point primary key,
   areaid integer references aree_statistiche,
   cityid integer references com2016_wgs84
);
-- 61.000 rows -> 500s
insert into islocatedin_point
  select osmid, areaid, cityid
  from (
    select x.*, gid as cityid
    from (
      select osm_id as osmid, areaid, way from planet_osm_point osm, aree_statistiche a where sdo_contains(a.geom, way) = 'TRUE'
    ) x, com2016_wgs84 c
    where sdo_contains(c.geom, way) = 'TRUE'
  ) y;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END - CREATE HIERARCHIES POINT
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- BEGIN - PROFILING
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- We are not considering the creation of a relational database at the moment
-- drop table entity;
-- create table entity (
--     entityid integer primary key,
--     edescription varchar2(4000)
-- );
-- drop table context;
-- create table context (
--     contextid integer primary key,
--     entityid integer references entity,
--     whenuri varchar2(4000),
--     whereuri varchar2(4000)
-- );
-- drop table features;
-- create table features (
--     featureid integer primary key,
--     fname varchar(100) not null,
--     fvalue varchar2(4000) not null,
--     contextid integer references context,
--     accuracy number default 1
-- );
-- insert into entity values (1, 'generated_user1');
-- insert into entity values (4, 'generated_user4');
-- insert into context values (1, 1, 'always', 'everywhere');
-- insert into context values (2, 4, 'always', 'everywhere');
-- insert into features values (1, 'livein', ST_PointFromText('POINT(11.372449 44.505005)', 4326), 1);
-- insert into features values (2, 'workin', ST_PointFromText('POINT(11.333137 44.499220)', 4326), 1);
-- insert into features values (3, 'workin', ST_PointFromText('POINT(11.310307 44.503271)', 4326), 2);
-- insert into features values (4, 'livein', ST_PointFromText('POINT(11.345219 44.491498)', 4326), 2);
-- insert into features values (5, 'livein', 'uriToEnrichedStayPoint', 2, 1);

drop table temporarypoints;
create table temporarypoints as
    select significativeid, latitude, longitude, geom, 'LiveIn' as fname
    from (
        select significativeid, latitude, longitude, geom, row_number() over (partition by significativeid order by significativeid) rown
        from significative_enriched_data
    )
    where rown = 1;
insert into temporarypoints values (11, 10, 10, sdo_geometry(2001, 4326, SDO_POINT_TYPE(10, 10, null), null, null), 'WorkIn');
insert into temporarypoints values (1, 11.3529589, 44.4989712, sdo_geometry(2001, 4326, SDO_POINT_TYPE(11.3529589, 44.4989712, null), null, null), 'WorkIn');
delete from user_sdo_geom_metadata where table_name = 'TEMPORARYPOINTS';
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'temporarypoints', 'geom', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(geom, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(geom, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 2)) as maxy
      from temporarypoints
  ) x;
drop index temporarypoints_idx;
create index temporarypoints_idx on temporarypoints(geom) indextype is MDSYS.SPATIAL_INDEX;
select * from temporarypoints points;

drop table staypoints_enriched;
create table staypoints_enriched as
    select z.significativeid, z.latitude, z.longitude, z.geom, z.osmid, z.osmgeom, z.fname, z.distance, z.areaid, coalesce (z.cityid, c2.gid) as cityid
    from (
        select y.significativeid, y.latitude, y.longitude, y.geom, y.osmid, y.osmgeom, y.fname, y.distance, coalesce (y.areaid, a2.areaid) as areaid, y.cityid
        from (
            select x.significativeid, x.latitude, x.longitude, x.geom, x.osmid, x.osmgeom, x.fname, x.distance, info.areaid, info.cityid
            from (
                select
                /*+ LEADING(t) INDEX(osm planet_osm_point_idx)  */
                -- significativeid, t.rowid as rid, sdo_nn_distance(1) as distance, geom, way as osmgeom
                t.significativeid, t.rowid as rid, t.latitude, t.longitude, osm.osm_id as osmid, sdo_nn_distance(1) as distance, t.fname, t.geom, osm.way as osmgeom
                from temporarypoints t left outer join planet_osm_point osm
                     on sdo_nn(osm.way, t.geom, 'sdo_num_res=3 distance=50 unit=meter', 1) = 'TRUE'
            ) x left outer join islocatedin_point info on info.osmid = x.osmid
        ) y left outer join aree_statistiche a2 on y.areaid is null and sdo_contains(a2.geom, y.geom) = 'TRUE'
    ) z left outer join com2016_wgs84 c2 on z.cityid is null and sdo_contains(c2.geom, z.geom) = 'TRUE';
delete from user_sdo_geom_metadata where table_name = 'STAYPOINTS_ENRICHED';
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'staypoints_enriched', 'geom', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(geom, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(geom, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 2)) as maxy
      from staypoints_enriched
  ) x;
create index staypoints_enriched_idx on staypoints_enriched(geom) indextype is MDSYS.SPATIAL_INDEX;
-- REALLY SLOOOOOOOWER than sdo_nn
-- select significativeid, featureid, osm_id as osmid, sdo_geom.sdo_distance(osm.way, t.geom, 0.005, 'unit=M') as distance, geom, way as osmgeom
-- from temporarypoints t, planet_osm_point osm
-- where sdo_geom.sdo_distance(osm.way, t.geom, 0.005, 'unit=meter') < 50;
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- END - PROFILING
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- ONTOLOGY CREATION
-- http://docs.oracle.com/cd/B28359_01/appdev.111/b28397/sdo_rdf_concepts.htm#RDFRM603
-- %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
-- grant sysdba to mfrancia;
create tablespace rdf_tblspace -- with sys permission
  datafile 'C:\APP\ADMINISTRATOR\ORADATA\TRAJECTORY\rdf_tblspace.dat' size 10M reuse
  autoextend on next 256M maxsize unlimited
  segment space management auto;
exec SEM_APIS.CREATE_SEM_NETWORK('rdf_tblspace'); -- with sys permission
execute sem_apis.drop_datatype_index('http://xmlns.oracle.com/rdf/geo/WKTLiteral',  TRUE); -- with sys permission
execute sem_apis.add_datatype_index('http://xmlns.oracle.com/rdf/geo/WKTLiteral',
            options=>'TOLERANCE=1 SRID=4326 DIMENSIONS=((LONGITUDE,-180,180) (LATITUDE,-90,90))');  -- with sys permission
create table profiling_rdf_data (triple SDO_RDF_TRIPLE_S);
execute SEM_APIS.CREATE_SEM_MODEL('profiles', 'profiling_rdf_data', 'triple');
create index profiles_sub_idx on profiling_rdf_data (triple.GET_SUBJECT());
create index profiles_prop_idx on profiling_rdf_data (triple.GET_PROPERTY());
create index profiles_obj_idx on profiling_rdf_data (TO_CHAR(triple.GET_OBJECT()));

create or replace function geom2orageo(geom sdo_geometry, srid integer)
   return varchar2 parallel_enable is orageom varchar2(4000);
   begin
      if geom is null then return null; end if;
      return('"<http://xmlns.oracle.com/rdf/geo/srid/' || srid || '> ' || SDO_UTIL.TO_WKTGEOMETRY(geom) || '"^^<http://xmlns.oracle.com/rdf/geo/WKTLiteral>');
   end;
-- select geom2orageo(SDO_GEOMETRY(2001, 4326, SDO_POINT_TYPE(11, 44, null), null, null), 4326) from dual;
create or replace function decuri(uri varchar)
   return varchar2 parallel_enable is shorteneduri varchar2(4000);
   begin
      return substr(uri, 59, length(uri));
   end;
-- select decuri('http://www.semantic.unibo.it/trajectory/profilingontology#MySubject5') from dual;

truncate table profiling_rdf_data;
set serveroutput on;
call load2ont('profiles', 'profiling_rdf_data');
-- from: http://docs.oracle.com/cd/B19306_01/appdev.102/b19307/sdo_rdf_concepts.htm
select SDO_RDF_TRIPLE_S.GET_TRIPLE(triple),     -- returns triple object
       SDO_RDF_TRIPLE_S.GET_SUBJECT(triple),    -- returns varchar2
       SDO_RDF_TRIPLE_S.GET_PROPERTY(triple),   -- returns varchar2
       SDO_RDF_TRIPLE_S.GET_OBJECT(triple)      -- returns clob2
from PROFILING_RDF_DATA
where SDO_RDF_TRIPLE_S.GET_SUBJECT(triple) like '%1>';


select geom
from table(
    sem_match(
        'SELECT ?entity ?context ?feature ?fname ?geom ?latitude
         WHERE
         {
           # HINT0={LEADING(?geom)}
           ?entity profiling:isSituatedIn ?context.
           ?context profiling:hasFeature ?feature.
           ?feature dbp:latitude ?latitude;
                    dbp:name ?fname;
                    orageo:hasExactGeometry ?geom.
           filter (orageo:withinDistance(?geom, "<http://xmlns.oracle.com/rdf/geo/srid/4326> POINT (11.3105237 44.5033503)"^^orageo:WKTLiteral, 1, "KM"))
         }',
        sem_models('profiles'), null,
        sem_aliases(
            sem_alias('profiling', 'http://www.semantic.unibo.it/trajectory/profilingontology#'),
            sem_alias('dbp', 'http://dbpedia.org/property/')
        ), null, null, 'ALLOW_DUP=T'))
order by sem$rownum;