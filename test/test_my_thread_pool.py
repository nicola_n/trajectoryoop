"""MyThreadPoolTestCase test module."""
import logging
import threading
import time
import unittest

from src.main.python.algorithm.my_thread_pool import MyThreadPool


def do_work(thread_input, params):
    """Thread work function."""
    id = thread_input[0]
    val = thread_input[1]
    time.sleep(val)  # simulate variable working time for testing
    # with lock_obj:
    #     print(str(id) + " - " + str(val))
    return str(id) + " - " + str(val)


def do_work_with_log(thread_input, lock_obj):
    """Thread work function."""
    id = thread_input[0]
    val = thread_input[1]
    time.sleep(val)  # simulate variable working time for testing
    with lock_obj:
        logging.info('val {} id : {}'.format(val, id))
    return str(id) + " - " + str(val)


class MyThreadPoolTestCase(unittest.TestCase):
    """MyThreadPoolTestCase class."""

    def test_my_thread_pool_execution(self):
        """Test result of MyThreadPool execution."""
        ids = [x for x in range(1, 11)]
        values = [0.003, 0.002, 0.003, 0.002, 0.003, 0.002, 0.003, 0.002, 0.003, 0.002]
        token = "aa"
        inputList = (zip(ids, values), token)
        compareList = zip(ids, values)
        mtp = MyThreadPool(do_work, 10)
        results = mtp.do_work(inputList)
        expectedResult = str([str(x) + " - " + str(y) for x, y in compareList])
        stringResult = str([x for x in results])
        assert(expectedResult == stringResult)

    def disabled_test_my_thread_pool_execution_with_loggin(self):
        """Test result of MyThreadPool execution with logging."""
        # TODO this test should be used for allowing logging through different processes
        logging.basicConfig(level=logging.DEBUG)
        ids = [x for x in range(1, 11)]
        values = [3, 2, 3, 2, 3, 2, 3, 2, 3, 2]
        lock_obj = threading.Lock()  # prevent overlapped printing from threads
        inputList = (zip(ids, values), lock_obj)
        compareList = zip(ids, values)
        mtp = MyThreadPool(do_work, 10)
        results = mtp.do_work(inputList)
        expectedResult = str([str(x) + " - " + str(y) for x, y in compareList])
        stringResult = str([x for x in results])
        assert(expectedResult == stringResult)
