"""Module for algorthtm runner configuration."""
import numpy as np

from os.path import join
from src.main.python.algorithm.algorithm_common import AlgorithmUtils
from src.main.python.algorithm.runner import Runner

# changing the input table name and the serial running parameter your ready to launch the algorithm with default values
input_table_name = 'significative_enriched_data'
serial_running = True

relative_path = r"./"
eps_tuple = []
min_pts_tuple = []
dist_thr_tuple = []
window_size_tuple = []
# significative ids to eval
ids = []
output_table_name = ""
log_file = ""
params_value = []
# with dist func None will be used haversine
dist_function = None
# where to store running parameters reference
runentry_table = AlgorithmUtils.dj_runentry_tableName

# select the algorithm to run
algorithm_to_run = Runner.algorithm_to_run_DRAW_CE

lookup_dict = {
    Runner.algorithm_to_run_DJC: "dj",
    Runner.algorithm_to_run_CE: "ce"
}
if algorithm_to_run == Runner.algorithm_to_run_DJC or algorithm_to_run == Runner.algorithm_to_run_CE:
    if input_table_name == 'significative_enriched_data':
        ids = [x for x in range(1, 11)]
        output_table_name = '{}_cluster_stay_points_significative'.format(lookup_dict[algorithm_to_run])
        log_file = "logs/significative_log.txt"
        # logarithmic values between 1 and 100
        # [1, 2, 3, 5, 7, 10, 12, 16, 21, 27, 35, 46, 59, 77, 100]
        params_value = [int(x) for x in np.logspace(0.2, 0.9, num=5)]
        params_value.extend([int(x) for x in np.logspace(1, 2, num=10)])
        cluster_reference_table = None
    elif input_table_name == 'bigsignificativedata_enriched':
        ids = [x for x in range(1, 8)]
        output_table_name = '{}_cluster_stay_points_bigdata'.format(lookup_dict[algorithm_to_run])
        log_file = "logs/bigdata_log.txt"
        params_value = [20, ]
        cluster_reference_table = None
    elif input_table_name == 'allsignificativedata_enriched':
        # TODO how many ids? should we retrieve them from db?
        ids = [x for x in range(1, 100)]
        output_table_name = '{}_cluster_stay_points_allsignificative'.format(lookup_dict[algorithm_to_run])
        log_file = "logs/allsignificative_log.txt"
        params_value = [20, ]
        cluster_reference_table = None
    else:
        print("set the right input table first")
        exit(1)
else:
    pass
significativeid_tuple = []
runs = len(params_value) * len(params_value) * len(ids)
id_name_tuple = ["timest" for x in range(runs)]
min_accuracy_tuple = [30 for x in range(runs)]
# 2016 to 2017
start_date_tuple = [1451606400 for x in range(runs)]
end_date_tuple = [1483228800 for x in range(runs)]


# TODO speed should change as well?
# default speed limit (~1km/h)
speed_tuple = [0.3 for x in range(runs)]
if serial_running:
    droptable_tuple = [True if x == 0 else False for x in range(runs)]
else:
    droptable_tuple = [False for x in range(runs)]

if algorithm_to_run == Runner.algorithm_to_run_DJC:
    # multiply every id for the params
    for x_id in ids:
        for eps in params_value:
            for minpts in params_value:
                eps_tuple.append(eps)
                min_pts_tuple.append(minpts)
                significativeid_tuple.append(x_id)
elif algorithm_to_run == Runner.algorithm_to_run_CE:
    min_value_tuple = [None for x in range(runs)]
    max_value_tuple = [None for x in range(runs)]
    parameter_name_tuple = [None for x in range(runs)]
    type_id_tuple = [1000 for x in range(runs)]
    for x_id in ids:
        for eps in params_value:
            for minpts in params_value:
                dist_thr_tuple.append(eps)
                window_size_tuple.append(minpts)
                significativeid_tuple.append(x_id)
elif algorithm_to_run == Runner.algorithm_to_run_DRAW_DJ:
    credential_file = relative_path + "credentials_postgress.txt"
    significativeid_tuple = [1, 2, 3]
    draw_input_table = 'dj_cluster_stay_points_significative'
    runs = 1
    log_file = r"logs/drawdjlog.txt"
elif algorithm_to_run == Runner.algorithm_to_run_DRAW_CE:
    log_file = r"logs/drawcelog.txt"
    runs = 1
    dist_thr_tuple = [20, ]
    window_size_tuple = [20, ]
    min_value_tuple = [1 for x in range(runs)]
    max_value_tuple = [4 for x in range(runs)]
    parameter_name_tuple = ["dayhour" for x in range(runs)]
    speed_tuple = [0.3 for x in range(runs)]
    min_accuracy_tuple = [30 for x in range(runs)]
    csv_file_name = join(relative_path, r"data/sampleMostSignificative5000.csv-enriched.txt")
    png_file_name = join(relative_path, r"data/2017-07-02.png")
else:
    pass
