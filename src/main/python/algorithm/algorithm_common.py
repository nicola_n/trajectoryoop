"""Algorithm common methods."""
import logging

import numpy as np
import pandas as pd
import psycopg2
import psycopg2.extras
from haversine import haversine

from src.main.python.mydata import main

# TODO both columns names and types are sometimes duplicated, maybe it worth to create a dictionary with the pair name:type
ce_columns = ('id', 'significativeid', 'timest', 'timespent', 'latitude', 'longitude', 'distdiff', 'staypointhits', 'typeid')
ce_dtype = (np.dtype(np.int32), np.dtype(np.int32), np.dtype(np.int32), np.dtype(np.int32), np.dtype(np.float64), np.dtype(np.float64), np.dtype(np.float64),
            np.dtype(np.int32), np.dtype(np.int32))

ce_tableName = 'curve_extrema_stay_points'

dj_columns = ('cluster_id', 'latitude', 'longitude', 'dimension', 'relevance', 'significativeid')
dj_dtype = (np.dtype(np.int32), np.dtype(np.float64), np.dtype(np.float64), np.dtype(np.int32), np.dtype(np.int32), np.dtype(np.int32))
dj_tableName = 'dj_cluster_stay_points'

dj_cluster_references_columns = ('cluster_id', 'latitude', 'longitude', 'p_number')
dj_cluster_references_dtype = (np.dtype(np.int32), np.dtype(np.float64), np.dtype(np.float64), np.dtype(np.int32))
dj_cluster_references_tableName = 'dj_cluster_references'

all_columns = ('timest', 'trajectoryid', 'segmentid', 'pointid', 'weekday', 'dayhour', 'latitude', 'longitude', 'accuracy', 'repetition',
               'significativeid', 'segmentbegin', 'segmentend', 'distinc', 'timeinc', 'distdiff', 'timediff', 'speed', 'still', 'slope', 'bearing', 'geom',
               'geomprevious', 'home', 'workmorning', 'workafternoon')
dj_columns_to_select = ('timest', 'dayhour', 'latitude', 'longitude', 'significativeid')


class AlgorithmUtils:
    """Algorithm Utils class."""

    runentry_columns = ('significativeid', 'accuracy', 'speed', 'startdate', 'enddate', 'inputtablename', 'idname', 'totalpoints', 'executiontime')
    dj_runentry_tableName = 'dj_cluster_runs'
    dj_runentry_columns = ('eps', 'minpts')

    ce_runentry_tableName = 'ce_cluster_runs'
    ce_runentry_columns = ('distthr', 'windowsize', 'minvalue', 'maxvalue', 'parametername', 'typeid')

    @staticmethod
    def get_dj_runentry_columns():
        """Return the columns of dj cluster run entry."""
        return AlgorithmUtils.runentry_columns + AlgorithmUtils.dj_runentry_columns

    @staticmethod
    def get_ce_runentry_columns():
        """Return the columns of dj cluster run entry."""
        return AlgorithmUtils.runentry_columns + AlgorithmUtils.ce_runentry_columns


def filter_function(min_value, max_value, parameter, rows):
    """
    Filter points between the given parameters.

    Return a new sequence each time the point does not respect filters (for the parameter and the significativeid).
    """
    sequence = []
    # significativeid cannot be negative
    lastdeviceid = -1
    for row in rows:
        if row["significativeid"] != lastdeviceid:
            if sequence:
                yield sequence
                sequence = []
            lastdeviceid = row["significativeid"]
        if parameter is None or min_value <= row[parameter] <= max_value:
            sequence.append(row)
        else:
            if sequence:
                yield sequence
                sequence = []
    if sequence:
        yield sequence


def calculate_centroid(points, previous_centroid, only_geo=False):
    """Calculate centroid given a set of points and the previous centroid."""
    centroid = points[0].copy()
    centroid_lat = 0
    centroid_lon = 0
    centroid_hits = 0

    for point in points:
        centroid_hits += 1
        centroid_lat += point["latitude"]
        centroid_lon += point["longitude"]

    centroid["latitude"] = centroid_lat / centroid_hits
    centroid["longitude"] = centroid_lon / centroid_hits
    if not only_geo:
        if previous_centroid.any():
            centroid["distdiff"] = haversine((centroid["latitude"], centroid["longitude"]),
                                             (previous_centroid["latitude"], previous_centroid["longitude"])) * 1000
            centroid["distinc"] = centroid["distdiff"] + previous_centroid["distinc"]
            centroid["timediff"] = centroid["timest"] - previous_centroid["timest"]
            centroid["timeinc"] = centroid["timediff"] + previous_centroid["timeinc"]
            centroid["speed"] = main.compute_speed(pd.Series([centroid["distdiff"], ]), pd.Series([centroid["timediff"], ]))[0]
            centroid["still"] = main.below_threshold(pd.Series([centroid["speed"], ]), main.stay_thr)[0]
            centroid["slope"] = main.compute_slope(previous_centroid["longitude"], previous_centroid["latitude"], centroid["longitude"], centroid["latitude"])
            centroid["bearing"] = main.compute_bearings(pd.Series([previous_centroid["longitude"], centroid["longitude"]]),
                                                        pd.Series([previous_centroid["latitude"], centroid["latitude"]]))[1]
        else:
            centroid["distdiff"] = 0
            centroid["distinc"] = 0
            centroid["timeinc"] = 0
            centroid["timediff"] = 0
            centroid["speed"] = 0
            centroid["still"] = 0
            centroid["slope"] = 0
            centroid["bearing"] = 0
    return centroid


def haversine_distance(x, y):
    """Calculare haversine distance in meters, given two tuple of (lat,long)."""
    return haversine(x, y) * 1000


def upload_my_data_object_to_db(my_data, csvfile_name, droptable, cur, columns):
    """Upload a MyData object to the db."""
    my_data.getData().to_csv(path_or_buf=csvfile_name, sep=';', header=False, index=False)
    if droptable:
        cur.execute(my_data.getTableCreation())
    with open(csvfile_name, 'r') as f:
        logging.info('Copy to table {}'.format(my_data.tableName))
        cur.copy_from(f, my_data.tableName, sep=';', columns=columns)


class Cluster:
    """Handle data, table creation and headers."""

    def __init__(self, points=None, id_name=None, dimension_key='repetition'):
        """Create the cluster."""
        if points is not None:
            if isinstance(points, pd.DataFrame):
                self.points = points
            else:
                self.points = pd.DataFrame(points)
        else:
            self.points = pd.DataFrame()
        self.idName = id_name
        self.dimensionKey = dimension_key

    def join(self, cluster_to_join):
        """Join this cluster with the one passed as parameter."""
        self.points = self.points.append(cluster_to_join.get_points().loc[~cluster_to_join.get_points()[self.idName].isin(self.points[self.idName])],
                                         ignore_index=True)

    def get_points(self):
        """Return the dataframe containing the points of the cluster."""
        return self.points

    def is_density_joinable(self, cluster_to_check):
        """Check if the parameter cluster has common points with this one."""
        return True in set(cluster_to_check.get_points()[self.idName].isin(self.points[self.idName]))

    def get_centroid(self):
        """Calculate the centroid of the staypoint."""
        return calculate_centroid([self.get_points().iloc[i] for i in range(len(self.get_points()))], pd.Series(), True)

    def get_dimension(self):
        """Get the number of points in the Cluster."""
        if self.dimensionKey in self.get_points().keys():
            return sum([self.get_points().iloc[i][self.dimensionKey] for i in range(len(self.get_points()))])
        else:
            return len(self.get_points())

    @staticmethod
    def join_clusters(clusters, c):
        """Join density joinable cluster to existing clusters when possible."""
        for index in clusters.index:
            if clusters[index].is_density_joinable(c):
                c.join(clusters[index])
                del clusters[index]
        if len(clusters) > 0:
            clusters = clusters.reset_index(drop=True)
        clusters.loc[len(clusters)] = c
        return clusters


class SimpleCluster:
    """Cluster object that contains cluster operation complexity."""

    def __init__(self, points=None, idName=None, columnsNames=None, dimensionKey='repetition'):
        """Create the cluster."""
        self.idName = idName
        if columnsNames is not None:
            l = []
            columnsNumber = len(columnsNames)
            for row in points:
                d = {}
                l.append(d)
                for i in range(columnsNumber):
                    d[columnsNames[i]] = row[columnsNames[i]]
            self.points = l
        else:
            self.points = points
        self.dimensionKey = dimensionKey

    def get_points(self):
        """Return the list containing the points of the cluster."""
        return self.points

    def get_centroid(self):
        """Calculate the centroid of the staypoint."""
        return calculate_centroid(self.get_points(), pd.Series(), True)

    def get_dimension(self):
        """Get the number of points in the Cluster."""
        return sum([x[self.dimensionKey] for x in self.get_points()])


class ConnectionControlledExecution:
    """Controlled Execution class, used for connect to the db and extract a cursor."""

    def __init__(self, credential_file):
        """Class init."""
        self.credential_file = credential_file
        self.file = None
        self.conn = None

    def __enter__(self):
        """Enter function, executed at the start of the with statement."""
        logging.debug('Trying to connect...')
        self.file = open(self.credential_file, 'r')
        connectionstring = self.file.read()
        self.conn = psycopg2.connect(connectionstring)
        self.cur = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        logging.debug('Connected!')
        return self

    def __exit__(self, type, value, traceback):
        """Exit function, executed at the end of the with statement."""
        self.cur.close()
        self.conn.close()
        self.file.close()


# TODO make this class abstract
class BaseAlgorithm:
    """BaseAlgorithm class."""

    def run_algortihm(self):
        """Run algorithm method."""
        raise NotImplemented


class SpatialAlgorithm(BaseAlgorithm):
    """SpatialAlgorithm base class."""

    def __init__(self, id_name, min_accuracy, max_speed, droptable, significative_id, start_date, end_date, input_table_name, output_table_name,
                 cluster_reference_table, runentry_table, relativepath, dist_function):
        """Spatial Algorithm parameters initialization."""
        self.id_name = id_name
        self.min_accuracy = min_accuracy
        self.max_speed = max_speed
        self.droptable = droptable
        self.significative_id = significative_id
        self.start_date = start_date
        self.end_date = end_date
        self.input_table_name = input_table_name
        self.output_table_name = output_table_name
        self.cluster_reference_table = cluster_reference_table
        self.runentry_table = runentry_table
        self.relativepath = relativepath
        if dist_function is None:
            self.dist_function = haversine_distance
        else:
            self.dist_function = dist_function
        self.create_cluster_reference = (self.cluster_reference_table is not None)
        if self.relativepath is None:
            self.relativepath = "./"
        # TODO make credential file a class parameter instead of this hack-ish way of create the path
        self.credential_file = self.relativepath + 'credentials_postgress.txt'

        # it will contain the run data in a MyData obj
        self.run_entry_my_data = None
        # it will contain the clusters in a dataframe
        self.clusters_dataframe = None
        # it will contain the reference of every cluster in a dataframe
        self.cluster_references_dataframe = None

    def create_run_entry(self, total_points, execution_time):
        """
        Create the run MyData object.

        :param total_points: total points
        :param execution_time: total elapsed time
        :return: run
        """
        run = main.MyData(self.runentry_table)
        run.add('runid', 'autoincrement', None, pk=True)
        run.add('significativeid', 'integer', [self.significative_id, ], unique=True)
        run.add('accuracy', 'integer', [self.min_accuracy, ], unique=True)
        run.add('speed', 'double', [self.max_speed, ], unique=True)
        run.add('startdate', 'integer', [self.start_date, ], unique=True)
        run.add('enddate', 'integer', [self.end_date, ], unique=True)
        run.add('inputtablename', 'varchar', [self.input_table_name, ], unique=True)
        run.add('idname', 'varchar', [self.id_name, ], unique=True)
        run.add('totalpoints', 'integer', [total_points, ])
        run.add('executiontime', 'double', [execution_time, ])
        return run

    def get_spatial_search_query_where_clause(self):
        """Return spatial algorithm runentry base search query where clause."""
        return 'significativeid = {} and accuracy = {} and speed = {}' \
               ' and startdate = {} and enddate = {} and inputtablename = \'{}\' and idname = \'{}\''\
            .format(self.significative_id, self.min_accuracy, self.max_speed,
                    self.start_date, self.end_date, self.input_table_name, self.id_name)

    def get_spatial_search_query(self):
        """Return spatial algorithm runentry base search query."""
        return 'select * from {} where {};'.format(self.runentry_table, self.get_spatial_search_query_where_clause())

    def update_geom_column(self, conn_ce):
        """Update the geom column."""
        if self.droptable:
            conn_ce.cur.execute('alter table {} add column geom geometry(Point,4326);'.format(self.output_table_name))
        sql_query = "update {} set geom = ST_SetSRID(ST_MakePoint(longitude, latitude),4326)" \
                    "WHERE geom is null;".format(self.output_table_name)
        # TODO add wkt column
        conn_ce.cur.execute(sql_query)
