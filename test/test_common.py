"""Common test module."""
import unittest

import pandas as pd

from src.main.python.algorithm import algorithm_common


class CommonTestCase(unittest.TestCase):
    """Test class."""

    @staticmethod
    def build_df_from_strings(a, b):
        """
        Create a DataFrame given string parameters.

        a: first line of csv (headers)
        b: multiline string of csv

        Return the pandas DataFrame.
        """
        a = a[2:].split(';')

        intValues = ('timest', 'trajectoryid', 'segmentid', 'pointid', 'weekday', 'dayhour', 'accuracy', 'repetition', 'significativeid',
                     'segmentbegin', 'segmentend', 'timeinc', 'timediff', 'still', 'home', 'workmorning', 'workafternoon')
        doubleValues = ('latitude', 'longitude', 'distinc', 'distdiff', 'speed', 'slope', 'bearing')

        l = []
        for x in b.split('\n'):
            d = {}
            y = x.split(';')
            if len(y) == len(a):
                for i in range(len(y)):
                    if a[i] in intValues:
                        d[a[i]] = int(y[i])
                    elif a[i] in doubleValues:
                        d[a[i]] = float(y[i])
                    else:
                        d[a[i]] = y[i]
                l.append(d)
        return pd.DataFrame(l)

    headers = "# significativeid;timest;trajectoryid;segmentid;pointid;weekday;dayhour;latitude;longitude;accuracy;repetition;significativeidjustfortest;" \
              "segmentbegin;segmentend;distinc;timeinc;distdiff;timediff;speed;still;slope;bearing;geominverted;geomprevious;home;workmorning;workafternoon"
    # geominverted and geomprevious have been truncated
    c = ("2;1478308645;0;0;66;5;2;44.5045606;11.3727348;19;0;1;0;0;0.0;6031;0.0;1;0.0;1;0.0;0.0;0101000020E61000008;0101000020E60D157195404640;1;0;0\n"
         "2;1478308832;0;0;67;5;2;44.5045606;11.3727348;19;0;1;0;0;0.0;6218;0.0;187;0.0;1;0.0;0.0;0101000020E8018D7BE26400;0100088010D1EC157195404640;1;0;01\n"
         "2;1478308833;0;0;68;5;2;44.5045606;11.3727348;19;0;1;0;0;0.0;6219;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n"
         "3;1478308645;0;0;66;5;2;44.5115606;11.3727348;19;0;1;0;0;0.0;6031;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n"
         "3;1478308832;0;0;67;5;2;44.5115606;11.3727348;19;0;1;0;0;0.0;6218;0.0;187;0.0;1;0.0;0.0;0101002640D1EC1571954040;0101002640D1EC157195404640;1;0;0\n"
         "3;1478308833;0;0;68;5;2;44.5115606;11.3727348;19;0;1;0;0;0.0;6219;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n")

    c2 = ("2;1478308645;0;0;66;5;2;44.5045406;11.3727348;19;0;1;0;0;0.0;6031;0.0;1;0.0;1;0.0;0.0;0101000020E61000008;0101000020E60D157195404640;1;0;0\n"
          "2;1478308832;0;0;67;5;2;44.5045606;11.3727348;19;0;1;0;0;0.0;6218;0.0;187;0.0;1;0.0;0.0;0101000020E8018D7BE26400;0100088010D1EC157195404640;1;0;01\n"
          "2;1478308833;0;0;68;5;2;44.5045806;11.3727348;19;0;1;0;0;0.0;6219;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n"
          "3;1478308645;0;0;66;5;2;44.5115406;11.3727348;19;0;1;0;0;0.0;6031;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n"
          "3;1478308832;0;0;67;5;2;44.5115606;11.3727348;19;0;1;0;0;0.0;6218;0.0;187;0.0;1;0.0;0.0;0101002640D1EC1571954040;0101002640D1EC157195404640;1;0;0\n"
          "3;1478308833;0;0;68;5;2;44.5115806;11.3727348;19;0;1;0;0;0.0;6219;0.0;1;0.0;1;0.0;0.0;0101002640D1EC157195404640;0101002640D1EC157195404640;1;0;0\n")

    c3 = ("2;1478316761;0;0;184;5;4;44.504560;11.3727348;19;185;1;0;0;0.0;14147;0.0;2;0.0;1;0.0;0.0;0101000020E610000;0101000020E610000;1;0;0\n"
          "2;1478316920;0;0;185;5;4;44.504625;11.3727545;19;1;1;0;0;7.30766463;14306;7.30766463;159;0.04596015;1;17.058819359;12.34434122696;01010;0101;1;0;0\n"
          "2;1478317372;0;0;207;5;4;44.504560;11.3727348;19;1;1;0;0;14.6153292;14758;7.3076646;77;0.09490474;1;197.058819359;192.34435503514;01010;0101;1;0;0\n"
          "2;1478317742;0;0;218;5;4;44.504625;11.3727545;19;1;1;0;0;21.9229939;15128;7.30766463;80;0.09134581;1;17.058819359;12.344341226965;01010;0101;1;0;0\n"
          "2;1478317938;0;0;227;5;4;44.504649;11.372766;18;1;1;0;0;24.73268164;15324;2.80968774;91;0.03087569;1;25.695536115;18.940603302638;01010;0101;1;0;0\n"
          "2;1478318368;0;0;240;5;4;44.504597;11.3727746;19;1;1;0;0;39.97803816;15754;5.14144718;130;0.03954959;1;47.476517821;37.8713012983;0101;0101;1;0;0\n")

    def test_build_df_from_strings(self):
        """Test of build_df method."""
        df = self.build_df_from_strings(self.headers, self.c)
        assert(len(df) == 6)
        assert(df.significativeid[0] == 2)
        assert(df.significativeid[4] == 3)

    def test_filter_function_with_df(self):
        """Test class."""
        dfL = self.build_df_from_strings(self.headers, self.c)

        def rowsGen():
            for i in range(len(dfL)):
                yield dfL.iloc[i]

        generator = rowsGen()
        result = [x for x in algorithm_common.filter_function(1, 4, "dayhour", generator)]
        assert (len(result) == 2)
        assert (len(result[0]) == 3)
        assert (len(result[1]) == 3)
        assert (result[0][0]["significativeid"] == 2)
        assert (result[1][0]["significativeid"] == 3)
        assert (result[0][1]["dayhour"] == 2)
        assert (result[1][1]["dayhour"] == 2)
        assert (result[0][0]["latitude"] == 44.5045606)
        assert (result[0][2]["latitude"] == 44.5045606)
        assert (result[1][0]["latitude"] == 44.5115606)
        assert (result[1][2]["latitude"] == 44.5115606)

    def test_filter_function_with_df_and_null_parameter(self):
        """Test class."""
        dfL = self.build_df_from_strings(self.headers, self.c)
        dfL2 = self.build_df_from_strings(self.headers, self.c)

        def rowsGen():
            for i in range(len(dfL)):
                yield dfL.iloc[i]

        generator = rowsGen()
        result = [x for x in algorithm_common.filter_function(1, 4, "dayhour", generator)]

        def rowsGen():
            for i in range(len(dfL)):
                yield dfL2.iloc[i]

        generator2 = rowsGen()
        result2 = [x for x in algorithm_common.filter_function(None, None, None, generator2)]

        assert (len(result) == 2)
        assert (len(result[0]) == len(result2[0]) == 3)
        assert (len(result[1]) == len(result2[1]) == 3)
        assert (result[0][0]["significativeid"] == result2[0][0]["significativeid"] == 2)
        assert (result[1][0]["significativeid"] == result2[1][0]["significativeid"] == 3)
        assert (result[0][1]["dayhour"] == result2[0][1]["dayhour"] == 2)
        assert (result[1][1]["dayhour"] == result2[1][1]["dayhour"] == 2)
        assert (result[0][0]["latitude"] == result2[0][0]["latitude"] == 44.5045606)
        assert (result[0][2]["latitude"] == result2[0][2]["latitude"] == 44.5045606)
        assert (result[1][0]["latitude"] == result2[1][0]["latitude"] == 44.5115606)
        assert (result[1][2]["latitude"] == result2[1][2]["latitude"] == 44.5115606)

    def test_columns_lenght_and_types(self):
        """Test length of algorithm columns."""
        assert(len(algorithm_common.dj_columns) == len(algorithm_common.dj_dtype))
        assert(len(algorithm_common.ce_columns) == len(algorithm_common.ce_dtype))
        assert(len(algorithm_common.dj_cluster_references_columns) == len(algorithm_common.dj_cluster_references_columns))

    def test_Spatial_get_spatial_search_query(self):
        """Test the spatial search query string."""
        spa = algorithm_common.SpatialAlgorithm(id_name='1', min_accuracy=2, max_speed=3, droptable=None, significative_id=4,
                                                start_date=5, end_date=6, input_table_name='7', output_table_name='8',
                                                cluster_reference_table=None, runentry_table='10', relativepath="", dist_function=None)
        expected = 'select * from 10 where significativeid = 4 and accuracy = 2 and speed = 3' \
                   ' and startdate = 5 and enddate = 6 and inputtablename = \'7\' and idname = \'1\';'
        assert (spa.get_spatial_search_query() == expected)
