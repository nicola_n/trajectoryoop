"""Draw Dj Clsuter results data."""
from src.main.python.draw.draw import Draw
from src.main.python.algorithm.algorithm_common import AlgorithmUtils
from src.main.python.algorithm.algorithm_common import ConnectionControlledExecution


class DrawDensityJoinCluster:
    """Draw density join cluster."""

    def __init__(self, credential_file, significativeid, table_name):
        """Params initialization."""
        self.credential_file = credential_file
        self.significativeid = significativeid
        self.table_name = table_name

    def draw_max_cluster_dimension(self):
        """Draw max cluster dimension by eps and minpts."""
        x = []
        y = []
        z = []
        with ConnectionControlledExecution(self.credential_file) as conn_ce:
            sqlQuery = 'select eps, minpts, dimension from {0} inner join {1} on {0}.runid = {1}.runid ' \
                       'where {0}.significativeid = {2}  and relevance = 0 ;' \
                        .format(self.table_name, AlgorithmUtils.dj_runentry_tableName, self.significativeid)
            conn_ce.cur.execute(sqlQuery)
            for res in conn_ce.cur:
                x.append(res["eps"])
                y.append(res["minpts"])
                z.append(res["dimension"])
        Draw.draw_3d_bar_chart(x=x, y=y, z=z, barcolor='#00FF00', title="Max cluster dimension", xlabel="eps", ylabel="minpts")

    def draw_cluster_number(self):
        """Draw cluster number by eps and minpts."""
        x = []
        y = []
        z = []
        with ConnectionControlledExecution(self.credential_file) as conn_ce:
            sqlQuery = 'select eps, minpts, count(*) as c_num from {0} inner join {1} on {0}.runid = {1}.runid ' \
                       'where {0}.significativeid = {2} group by eps, minpts' \
                .format(self.table_name, AlgorithmUtils.dj_runentry_tableName, self.significativeid)

            conn_ce.cur.execute(sqlQuery)
            for res in conn_ce.cur:
                x.append(res["eps"])
                y.append(res["minpts"])
                z.append(res["c_num"])
        Draw.draw_3d_bar_chart(x=x, y=y, z=z, barcolor='#20AFA0', title="Cluster number", xlabel="eps", ylabel="minpts")


if __name__ == "__main__":
    credentialFile = '../../../../credentials_postgress.txt'
    ddjc = DrawDensityJoinCluster(credential_file=credentialFile, significativeid=1, table_name='dj_cluster_stay_points_significative')
    ddjc.draw_cluster_number()
    ddjc.draw_max_cluster_dimension()
