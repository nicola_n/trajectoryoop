-- Visualizza le sessioni che hanno un lock attivo
-- SID e SERIAL# sono i campi che servono per il KILL
select *
from v$session s, v$process p
where s.paddr = p.addr and s.sid in (select SESSION_ID from v$locked_object);

-- Visualizza gli oggetti lockati
SELECT LPAD(' ',DECODE(l.xidusn,0,3,0)) || l.oracle_username "MFRANCIA",
o.owner, o.object_name, o.object_type
FROM v$locked_object l, dba_objects o
WHERE l.object_id = o.object_id
ORDER BY o.object_id, 1 desc;

-- Killa sessione indicando 'SID,SERIAL#'
alter system kill session '173,10917' immediate;

-- check if table is recognized as spatial
select * from user_sdo_geom_metadata;
insert into user_sdo_geom_metadata (table_name, column_name, diminfo, srid)
  select 'aree_statistiche', 'geom', sdo_dim_array(sdo_dim_element('X', minx, maxx, 0.005), sdo_dim_element('Y', miny, maxy, 0.005)), 4326
  from (
      select min(sdo_geom.sdo_min_mbr_ordinate(geom, 1)) as minx,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 1)) as maxx,
             min(sdo_geom.sdo_min_mbr_ordinate(geom, 2)) as miny,
             max(sdo_geom.sdo_max_mbr_ordinate(geom, 2)) as maxy
      from aree_statistiche
  ) x;
update aree_statistiche aree set aree.geom.SDO_SRID = 4326; -- set srid but not transform
select sdo_cs.transform(geom, 4326) from aree_statistiche; -- trasnform srid