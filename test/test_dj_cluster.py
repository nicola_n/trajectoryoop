"""Curve Extrema test module."""
import unittest

import pandas as pd
import test_common as tc
from pandas.util.testing import assert_frame_equal

from src.main.python.algorithm import dj_cluster
from src.main.python.algorithm.algorithm_common import Cluster


class DjClusterTestCase(unittest.TestCase):
    """Test class."""

    @staticmethod
    def test_init():
        """Test the creation of Cluster class."""
        p = {"id": 0, "dist": 100, "time": 200, "bear": 1}
        p2 = {"id": 1, "dist": 300, "time": 1200, "bear": 0.1}
        p3 = {"id": 2, "dist": 400, "time": 2200, "bear": 0.5}
        l = [p, p2, p3]
        c1 = dj_cluster.Cluster(id_name="id", points=l)

        assert(isinstance(c1.get_points(), pd.DataFrame))
        assert(len(c1.get_points()) == 3)
        assert(c1.get_points()["dist"][2] == 400)

        c2 = dj_cluster.Cluster(id_name="id", points=None)
        assert (isinstance(c2.get_points(), pd.DataFrame))
        assert(len(c2.get_points()) == 0)

    @staticmethod
    def test_join():
        """Test join method of Cluster class."""
        idName = "id"
        a = [1, 2, 3]
        columns = (idName, "val1", "val2")
        df = pd.DataFrame(columns=columns)
        df2 = pd.DataFrame(columns=columns)

        lines = 5
        for i in range(lines):
            df.loc[i] = [x * i for x in a]
            df2.loc[i] = [x * i * 2 for x in a]

        c1 = dj_cluster.Cluster(id_name=idName, points=df)
        c2 = dj_cluster.Cluster(id_name=idName, points=df2)

        assert(len(c1.get_points()) == lines)
        assert(len(c2.get_points()) == lines)
        assert_frame_equal(c1.get_points(), df)
        assert_frame_equal(c2.get_points(), df2)

        diffLines = df2[df2[idName] > lines]
        addednum = len(diffLines)

        c1.join(c2)
        newDf = df.copy().append(diffLines, ignore_index=True)
        assert(len(c1.get_points()) == lines + addednum)

        assert_frame_equal(c1.get_points(), newDf)

    @staticmethod
    def test_is_density_joinable():
        """Test is_density_joinable of Cluster class."""
        idName = "id"
        a = [1, 2, 3]
        columns = (idName, "val1", "val2")
        df = pd.DataFrame(columns=columns)
        df2 = pd.DataFrame(columns=columns)
        df3 = pd.DataFrame(columns=columns)

        lines = 5
        for i in range(lines):
            df.loc[i] = [x * i for x in a]
            df2.loc[i] = [x * i * 2 for x in a]
            df3.loc[i] = [x * (i + 1) * 6 for x in a]

        c1 = dj_cluster.Cluster(id_name=idName, points=df)
        c2 = dj_cluster.Cluster(id_name=idName, points=df2)
        c3 = dj_cluster.Cluster(id_name=idName, points=df3)

        assert(c1.is_density_joinable(c2))
        assert(not c1.is_density_joinable(c3))
        assert(c2.is_density_joinable(c3))

    @staticmethod
    def test_join_clusters():
        """Test join_clusters method."""
        idName = "id"
        a = [1, 2, 3]
        columns = (idName, "val1", "val2")
        df = pd.DataFrame(columns=columns)
        df2 = pd.DataFrame(columns=columns)
        df3 = pd.DataFrame(columns=columns)

        lines = 5
        for i in range(lines):
            df.loc[i] = [x * i for x in a]
            df2.loc[i] = [x * i * 2 for x in a]
            df3.loc[i] = [x * (i + 1) * 6 for x in a]

        c1 = Cluster(id_name=idName, points=df)
        c2 = Cluster(id_name=idName, points=df2)
        c3 = Cluster(id_name=idName, points=df3)

        clusters = pd.Series([c1, c3])
        Cluster.join_clusters(clusters, c2)

        assert(len(clusters) == 1)
        assert(len(clusters[0].get_points()) == 11)

        clusters = pd.Series([c1])
        Cluster.join_clusters(clusters, c2)
        Cluster.join_clusters(clusters, c3)

        assert (len(clusters) == 1)
        assert (len(clusters[0].get_points()) == 11)

        clusters = pd.Series([c3])
        Cluster.join_clusters(clusters, c1)
        Cluster.join_clusters(clusters, c2)

        assert (len(clusters) == 1)
        assert (len(clusters[0].get_points()) == 11)

    @staticmethod
    def test_join_clusters2():
        """Test again join_clusters method."""
        idName = "id"
        a = [1, 2, 3]
        columns = (idName, "val1", "val2")
        df = pd.DataFrame(columns=columns)
        df2 = pd.DataFrame(columns=columns)
        df3 = pd.DataFrame(columns=columns)
        df4 = pd.DataFrame(columns=columns)

        lines = 3
        for i in range(lines):
            df.loc[i] = [x * i for x in a]
            df2.loc[i] = [x * i * 2 for x in a]
            df3.loc[i] = [x * (i + 1) * 6 for x in a]
            df4.loc[i] = [x * (i + 1) * 7 for x in a]

        c1 = Cluster(id_name=idName, points=df)
        c2 = Cluster(id_name=idName, points=df2)
        c3 = Cluster(id_name=idName, points=df3)
        c4 = Cluster(id_name=idName, points=df4)

        clusters = pd.Series([c1])
        clusters = Cluster.join_clusters(clusters, c2)
        clusters = Cluster.join_clusters(clusters, c3)
        clusters = Cluster.join_clusters(clusters, c4)

        assert (len(clusters) == 3)
        assert (len(clusters[0].get_points()) == 4)
        assert (sum([x.get_dimension() for x in clusters]) == 10)

    def test_join_clusters3(self):
        """Test again join_clusters method."""
        idName = "id"
        a = [1, 2, 3]
        columns = (idName, "val1", "val2")
        df = pd.DataFrame(columns=columns)
        df2 = pd.DataFrame(columns=columns)
        df3 = pd.DataFrame(columns=columns)
        df4 = pd.DataFrame(columns=columns)
        df5 = pd.DataFrame(columns=columns)

        lines = 3
        for i in range(lines):
            df.loc[i] = [x * i for x in a]
            df2.loc[i] = [x * i * 2 for x in a]
            df3.loc[i] = [x * (i + 1) * 6 for x in a]
            df4.loc[i] = [x * (i + 1) * 7 for x in a]
            df5.loc[i] = [x * (i + 1) * 3 for x in a]

        c1 = Cluster(id_name=idName, points=df)
        c2 = Cluster(id_name=idName, points=df2)
        c3 = Cluster(id_name=idName, points=df3)
        c4 = Cluster(id_name=idName, points=df4)
        c5 = Cluster(id_name=idName, points=df5)

        clusters = pd.Series([c1])
        clusters = Cluster.join_clusters(clusters, c3)
        clusters = Cluster.join_clusters(clusters, c4)
        clusters = Cluster.join_clusters(clusters, c2)

        # joining c2 after c4 should have changed the order of clusters
        assert (len(clusters) == 3)
        assert (len(clusters[0].get_points()) == 3)
        assert (len(clusters[2].get_points()) == 4)
        assert (sum([x.get_dimension() for x in clusters]) == 10)

        clusters = pd.Series([c1])
        clusters = Cluster.join_clusters(clusters, c3)
        clusters = Cluster.join_clusters(clusters, c4)
        clusters = Cluster.join_clusters(clusters, c2)
        clusters = Cluster.join_clusters(clusters, c5)

        assert (len(clusters) == 3)
        assert (len(clusters[0].get_points()) == 3)
        assert (len(clusters[2].get_points()) == 5)
        assert (sum([x.get_dimension() for x in clusters]) == 12)

    @staticmethod
    def test_get_centroid():
        """Test the centroid of a given cluster."""
        input = {"s": (tc.CommonTestCase.c3,)}
        output = {"lat": [44.5046027, ], "lon": [11.3727532, ], "timest": [1478316761, ]}
        for inputCounter in range(len(input["s"])):

            dfL = tc.CommonTestCase.build_df_from_strings(tc.CommonTestCase.headers, input["s"][inputCounter])
            c1 = Cluster(id_name="id", points=dfL)

            centroid = c1.get_centroid()
            epsilon = 0.0000001
            assert (abs(output["lat"][inputCounter] - centroid["latitude"]) < epsilon)
            assert (abs(output["lon"][inputCounter] - centroid["longitude"]) < epsilon)
            assert (output["timest"][inputCounter] == centroid["timest"])

    @staticmethod
    def test_recursive_dj_cluster():
        """Test the recursive implementation of the dj cluster algorithm."""
        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 1, 'neighborhoodset': set()},
        ])

        pointsLen = len(points)
        djca = dj_cluster.DJClusterAlgorithm(eps=400, min_pts=2, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        clustersResult, totalpoints = djca.recursive_dj_cluster(points=points)
        assert (totalpoints == pointsLen)
        assert (len(clustersResult) == 2)
        assert (clustersResult[0].get_dimension() + clustersResult[1].get_dimension() == 7)

        original = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 1.5, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 3, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 4.5, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 100, 'longitude': 100, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 200, 'longitude': 200, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 400, 'longitude': 400, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 500, 'longitude': 500, 'repetition': 1, 'neighborhoodset': set()},
        ])

        def euclideanDistance(x, y):
            return (x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2

        pointsLen = len(original)
        djca = dj_cluster.DJClusterAlgorithm(eps=4, min_pts=2, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=euclideanDistance)
        clustersResult, totalpoints = djca.recursive_dj_cluster(points=original)
        assert (totalpoints == pointsLen)
        assert (len(clustersResult) == 2)
        assert (clustersResult[0].get_dimension() + clustersResult[1].get_dimension() == 7)

    @staticmethod
    def test_compare_dj_cluster():
        """Compare the recursive implementation of the dj cluster algorithm with the old implementation."""
        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 1, 'neighborhoodset': set()},
        ])
        eps = 400
        minpts = 2
        pointsLen = len(points)
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        recursiveClustersResult, totalpoints = djca.recursive_dj_cluster(points=points)
        assert (totalpoints == pointsLen)

        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 1},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 1},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 1},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 1},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 1},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 1},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 1},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 1},
        ])
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        clustersResult = [x for x in djca.sequence_dj_cluster(points=points)]

        assert (len(recursiveClustersResult) == len(clustersResult))
        assert (sum([x.get_dimension() for x in recursiveClustersResult]) == sum([x.get_dimension() for x in clustersResult]))

    @staticmethod
    def test_recursive_dj_cluster_with_repetition():
        """
        Test the recursive implementation of the dj cluster algorithm.

        To calculate the cluster dimension is considered the 'repetition' parameter.
        """
        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 11, 'neighborhoodset': set()},
        ])
        pointsLen = len(points)
        djca = dj_cluster.DJClusterAlgorithm(eps=400, min_pts=11, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        clustersResult, totalpoints = djca.recursive_dj_cluster(points=points)
        assert(totalpoints == pointsLen)
        assert (len(clustersResult) == 3)
        assert (sum([x.get_dimension() for x in clustersResult]) == 43)

        original = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 1.5, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 3, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 4.5, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 100, 'longitude': 100, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 200, 'longitude': 200, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 400, 'longitude': 400, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 500, 'longitude': 500, 'repetition': 11, 'neighborhoodset': set()},
        ])

        def euclideanDistance(x, y):
            return (x[0] - y[0]) ** 2 + (x[1] - y[1]) ** 2

        pointsLen = len(original)
        djca = dj_cluster.DJClusterAlgorithm(eps=400, min_pts=11, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=euclideanDistance)
        clustersResult, totalpoints = djca.recursive_dj_cluster(points=original)
        assert(totalpoints == pointsLen)
        assert (len(clustersResult) == 3)
        assert (sum([x.get_dimension() for x in clustersResult]) == 43)

    @staticmethod
    def test_compare_dj_cluster_with_repetition():
        """
        Compare the recursive implementation of the dj cluster algorithm with the old implementation.

        To calculate the cluster dimension is considered the 'repetition' parameter.
        """
        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 11, 'neighborhoodset': set()},
        ])
        eps = 400
        minpts = 11
        pointsLen = len(points)
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        recursiveClustersResult, totalpoints = djca.recursive_dj_cluster(points=points)
        assert(totalpoints == pointsLen)

        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 5},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 5},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 5},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 10},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 5},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 10},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 10},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 10},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 11},
        ])
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        clustersResult = [x for x in djca.sequence_dj_cluster(points=points)]

        assert (len(recursiveClustersResult) == len(clustersResult))
        assert (sum([x.get_dimension() for x in recursiveClustersResult]) == sum([x.get_dimension() for x in clustersResult]))

    @staticmethod
    def test_compare_dj_cluster_with_repetition_and_cluster_min_points():
        """
        Compare the recursive implementation of the dj cluster algorithm with the old implementation.

        To calculate the cluster dimension is considered the 'repetition' parameter.
        The number of points in each cluster is equal to the minpts parameter.
        """
        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 2, 'neighborhoodset': set()},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 2, 'neighborhoodset': set()},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 2, 'neighborhoodset': set()},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 9, 'neighborhoodset': set()},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 5, 'neighborhoodset': set()},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1, 'neighborhoodset': set()},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 10, 'neighborhoodset': set()},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 11, 'neighborhoodset': set()},
        ])
        eps = 400
        minpts = 11
        pointsLen = len(points)
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        recursiveClustersResult, totalpoints = djca.recursive_dj_cluster(points=points)
        assert(totalpoints == pointsLen)

        points = list([
            {'timest': 'adas', 'significativeid': 1, 'latitude': 0, 'longitude': 0, 'repetition': 2},
            {'timest': 'basd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0005, 'repetition': 2},
            {'timest': 'zasd', 'significativeid': 1, 'latitude': 0, 'longitude': 0.0003, 'repetition': 2},
            {'timest': '1000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 9},
            {'timest': 'cass', 'significativeid': 1, 'latitude': 0, 'longitude': 0.001, 'repetition': 5},
            {'timest': '2000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo1', 'significativeid': 1, 'latitude': 10, 'longitude': 10, 'repetition': 10},
            {'timest': '3000', 'significativeid': 1, 'latitude': 10, 'longitude': 44, 'repetition': 1},
            {'timest': 'foo2', 'significativeid': 1, 'latitude': 20, 'longitude': 20, 'repetition': 10},
            {'timest': 'foo3', 'significativeid': 1, 'latitude': 40, 'longitude': 40, 'repetition': 10},
            {'timest': 'foo4', 'significativeid': 1, 'latitude': 50, 'longitude': 50, 'repetition': 11},
        ])
        djca = dj_cluster.DJClusterAlgorithm(eps=eps, min_pts=minpts, id_name="timest", min_accuracy=None, max_speed=None, droptable=None,
                                             significative_id=None, start_date=None, end_date=None,
                                             input_table_name=None, output_table_name=None,
                                             cluster_reference_table=None, runentry_table=None, relativepath=None, dist_function=None)
        clustersResult = [x for x in djca.sequence_dj_cluster(points=points)]

        assert (len(recursiveClustersResult) == len(clustersResult) == 3)
        assert (sum([x.get_dimension() for x in recursiveClustersResult]) == sum([x.get_dimension() for x in recursiveClustersResult]) == 33)

    @staticmethod
    def test_djcluster_get_spatial_search_query():
        """Test the spatial search query string."""
        djca = dj_cluster.DJClusterAlgorithm(eps=12, min_pts=11, id_name='1', min_accuracy=2, max_speed=3, droptable=None, significative_id=4,
                                             start_date=5, end_date=6, input_table_name='7', output_table_name='8',
                                             cluster_reference_table=None, runentry_table='10', relativepath="", dist_function=None)
        expected = 'select * from 10 where significativeid = 4 and accuracy = 2 and speed = 3' \
                   ' and startdate = 5 and enddate = 6 and inputtablename = \'7\' and idname = \'1\' and eps = 12 and minpts = 11;'
        assert (djca.get_runentry_search_query() == expected)
