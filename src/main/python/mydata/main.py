"""Segment end enrich data."""
import datetime
from functools import reduce
from haversine import haversine
import logging
import math
import numpy as np
import pandas as pd
import pytz

ACCURACY = 'accuracy'
SIGNIFICATIVEID = 'significativeid'
LAT = 'latitude'
LON = 'longitude'
GEOMETRY = 'geom'
TIME = 'timest'
HOUR = 'dayhour'

# TODO this should be moved to algorithm_common,
# but due to relative path in bitbucket pipeline we can't import "src.main.python.algorithm_common" without changing
# csv path
main_table_name = "significative_enriched_data"


class MyData():
    """Handle data, table creation and headers."""

    def __init__(self, tableName, dbtype='pg'):
        """Initialize data."""
        self.content = {'headers': [], 'query': [], 'data': pd.DataFrame(), 'pks': [], 'uniques': [], 'fks': {}}
        self.dbtype = dbtype
        self.mydict = {
            'double': {'pg': 'double precision', 'oracle': 'number'},
            'integer': {'pg': 'integer', 'oracle': 'integer'},
            # TODO autoincrement for oracle?
            'autoincrement': {'pg': 'bigserial', 'oracle': 'TODO'},
            'point': {'pg': 'geometry(Point,4326)', 'oracle': 'sdo_geometry'},
            'pkey': {},
            # TODO check if the cascade deletion is the best way to remove constrained tables or instead we should remove tables in order
            'droptable': {'pg': 'drop table if exists {} cascade', 'oracle': 'drop table {}'}
        }
        self.tableName = tableName

    def lookup(self, key):
        """Find right type depending on the database."""
        try:
            return self.mydict[key][self.dbtype]
        except:
            return key

    def add(self, name, query, data, k='foo', pk=False, unique=False, fk=False, ftable=None):
        """Add new stuff."""
        self.content.get('headers').append(name)
        queryValue = name + ' ' + self.lookup(query)
        if fk:
            self.content.get('fks').setdefault(ftable, []).append(name)
        self.content.get('query').append(queryValue)
        if data is not None:
            self.content["data"][name] = data
        if pk:
            self.content.get('pks').append(name if k == 'foo' else k)
        if unique:
            self.content.get('uniques').append(name if k == 'foo' else k)

    def getNamesString(self, tab):
        """Get names."""
        def __mstr(n, s):
            return ('{:' + str(n) + '}').format(s)

        return reduce((lambda a, b: __mstr(tab, a) + ';' + __mstr(tab, b)), self.content.get('headers'))

    def getNamesList(self):
        """Get names as list."""
        return self.content.get('headers')

    def getQueryString(self):
        """Query."""
        return reduce(lambda a, b: a + ',' + b, self.content['query'])

    def getPkString(self):
        """Query."""
        s = reduce((lambda a, b: a + ',' + b), self.content.get('pks'))
        logging.debug('Pimary key: ({})'.format(s))
        return s

    def getTableCreation(self):
        """Query."""
        s = ' '.join([self.lookup('droptable').format(self.tableName), ';create table', self.tableName,
                      '({}'.format(self.getQueryString())])

        if self.content.get('pks'):
            s = s + ', primary key({})'.format(self.getPkString())

        if self.content.get('uniques'):
            if self.dbtype == 'pg':
                s = s + ', unique({})'.format(",".join(self.content.get('uniques')))
            else:
                raise NotImplemented("Not implemented uniques for dbtype {}".format(self.dbtype))

        if self.content.get('fks'):
            if self.dbtype == 'pg':
                for table in sorted(self.content.get('fks').keys()):
                    s = s + ', foreign key ({0}) references {1}({0})'.format(",".join(self.content.get('fks')[table]), table)
            else:
                raise NotImplemented("Not implemented external reference for dbtype {}".format(self.dbtype))
        s = s + ');'
        logging.debug(s.encode(encoding='UTF-8', errors='strict'))
        return s

    def getData(self):
        """Get data."""
        return self.content.get('data')


def sum_over_segment(data):
    """Sum elements over `data`.

    :param data: data
    :return: an array which ith-element is the sum of the previous ones
    """
    def foo(data):
        res = 0
        for x in data:
            res += x
            yield res

    return [x for x in foo(data)]


def above_threshold(data, thr):
    """Return points above threshold.

    :param data: data to be checked
    :param thr:  threshold
    :return: a boolean array which `True` elements are points above threshold
    """
    return [1 if x else 0 for x in (data > thr)]


def below_threshold(data, thr):
    """Return points below threshold.

    :param data: data to be checked
    :param thr:  threshold
    :return: a boolean array which `True` elements are points below threshold
    """
    return [1 if x else 0 for x in (data < thr)]


def compute_first(data):
    """Return an array which first element is 1."""
    num_rows = len(data)
    res = np.zeros(num_rows)
    res[0] = 1
    return res.astype(int)


def compute_last(data):
    """Return an array which last element is 1."""
    num_rows = len(data)
    res = np.zeros(num_rows)
    res[-1] = 1
    return res.astype(int)


def compute_id(data):
    """Map each `True` to a new id.

    Return a new array with ids.

    Keyword arguments:
    data -- array of booleans
    """
    def compute(data):
        id = -1
        for x in data:
            if x:
                id += 1
            yield id

    return np.array([x for x in compute(data)])


def compute_haversine_distance(lat, lon, km=False):
    """Estimate the distance between two consequent coordinates.

    Return a new array with positive distances.

    Keyword arguments:
    lat -- array of latitudes
    lon -- array of longitudes
    """
    def foo(lat, lon):
        num_rows = lat.size
        for index in range(0, num_rows):
            if index > 0:
                yield haversine((lat[index], lon[index]), (lat[index - 1], lon[index - 1]))
            else:
                yield 0

    # logging.debug('Haversine distance returns ' + ('km' if km else 'm'))
    res = [x for x in foo(lat, lon)]
    if km:
        return np.array(res)
    else:
        return np.multiply(res, 1000)


def compute_index(data):
    """Return an array of indexes."""
    num_rows = len(data)
    return np.arange(num_rows)


def compute_repeat(lat, lon):
    """Check if (lat, long) is repeated over two contiguous coordinates."""
    def foo(lat, lon):
        count = 0
        start = 1
        num_rows = lat.size
        for index in range(0, num_rows - 1):
            cur = (lat[index], lon[index])
            next = (lat[index + 1], lon[index + 1])
            count += 1
            if next != cur:
                yield count
                start = 1
                count = 0
            else:
                yield start
                start = 0
        yield count + 1

    return [x for x in foo(lat, lon)]


def compute_slope(lon1, lat1, lon2, lat2):
    """Compute the slope of a segment from (x1, y1) to (x2, y2)."""
    lon2 = lon2 - lon1
    lat2 = lat2 - lat1
    d = math.degrees(math.atan2(lon2, lat2))
    if lon2 < 0:
        return 360 + d
    else:
        return d


def compute_slopes(lon, lat):
    """Compute the slope of a segment from (x1, y1) to (x2, y2)."""
    def foo(lon, lat):
        num_rows = lat.size
        for index in range(0, num_rows):
            if index > 0:
                yield compute_slope(lon[index - 1], lat[index - 1], lon[index], lat[index])
            else:
                yield 0

    return np.array([x for x in foo(lon, lat)])


def compute_speed(distance, time):
    """Return speed array."""
    with np.errstate(divide='ignore', invalid='ignore'):
        return np.nan_to_num(np.divide(distance, time))


def compute_time(time, m=False):
    """Estimate the time between two pings.

    Return a new array with time distances.

    Keyword arguments:
    time -- array of timestamps
    """
    def foo(time):
        num_rows = time.size
        for index in range(0, num_rows):
            if index > 0:
                yield time[index] - time[index - 1]
            else:
                yield 0

    res = [x for x in foo(time)]
    if m:
        # logging.debug('Time distance returns minutes')
        return np.divide(res, 60)
    else:
        # logging.debug('Time distance returns seconds')
        return np.array(res)


def get_names(filename):
    """Get column names from csv comment."""
    with open(filename, 'r') as f:
        return f.readline().rstrip().split('# ')[1].split(';')


def get_segments(data, id, asnumpyarray=False):
    """Get a list of segments.

    Return a new list containing each segment

    Keyword arguments:
    data -- array to be segmented
    id -- segment array according to this discriminator
    """
    def foo(data, id):
        prev = -1
        res = []
        for a, b in zip(data if asnumpyarray else data.values, id):
            if b != prev:
                if len(res) > 0:
                    yield np.array(res) if asnumpyarray else pd.DataFrame(np.array(res), columns=list(data.columns))
                prev = b
                res = [a]
            else:
                res.append(a)
        if len(res) > 0:
            yield np.array(res) if asnumpyarray else pd.DataFrame(np.array(res), columns=list(data.columns))

    return [x for x in foo(data, id)]


def millis_to_hour(data, tz='Europe/Madrid'):
    """Return hour from s."""
    return np.array([x.strftime('%H') for x in millis_to_date(data, tz)]).astype(int)


def millis_to_day(data, tz='Europe/Madrid'):
    """Return day from ms."""
    return np.array([x.weekday() for x in millis_to_date(data, tz)])


def millis_to_date(data, tz='Europe/Madrid'):
    """Return date from ms."""
    for x in data:
        yield datetime.datetime.fromtimestamp(x, tz=pytz.timezone(tz))


def getMorningWork(hours, accuracy):
    """Get working place (morning)."""
    return getStayPoints(9, 11, hours, accuracy)


def getAfternoonWork(hours, accuracy):
    """Get working place (afternoon)."""
    return getStayPoints(15, 17, hours, accuracy)


def getHome(hours, accuracy):
    """Get home."""
    return getStayPoints(2, 4, hours, accuracy)


def getStayPoints(startHour, endHour, hours, accuracy, accuracyThr=40):
    """Return an array which elements are 1s if points are within the specified interval and below the given accuracy."""
    return np.array(
        [1 if h >= startHour and h <= endHour and a <= accuracyThr else 0 for (h, a) in zip(hours, accuracy)])


def segment_contiguous_time(data, interval):
    """Segment trajectories over time.

    Return a new array where each `True` is the beginning of a new segment.

    Keyword arguments:
    data -- data to be segmented
    interval -- interval in seconds
    """
    def segment(data, thr):
        num_rows = data.size
        for index in range(0, num_rows):
            if index > 0:
                diff = data[index] - data[index - 1]
                if diff < 0:
                    logging.warning('Time diff is negative. Are you segmenting unsorted data?')
                yield diff < 0 or diff > thr
            else:
                yield True

    return [x for x in segment(data, interval)]


def segment_user(data):
    """Segment trajectories according to users.

    Return a new array where each `True` is the beginning of a new segment.

    Keyword arguments:
    data -- data to be segmented
    """
    def segment(data):
        num_rows = data.size
        for index in range(0, num_rows):
            if index > 0:
                yield data[index] != data[index - 1]
            else:
                yield True

    return [x for x in segment(data)]


trajectory_interval = 60 * 60 * 1
segment_interval = 20 * 60
stay_thr = 0.28  # 1km/h


def shift_data(data):
    """Shift elements by one."""
    l = [data[0]]
    l.extend([data[i - 1] for i in range(1, len(data))])
    return np.array(l)


def calculate_radians_bearing(point1, point2):
    """Calculate radians."""
    deltaLongitude = point2[1] - point1[1]
    # x = math.cos(point2[0]) * math.sin(deltaLongitude)
    # y = math.cos(point1[0]) * math.sin(point2[0]) - math.sin(point1[0]) * math.cos(point2[0]) * math.cos(deltaLongitude)
    x = math.sin(deltaLongitude)  # see https://en.wikipedia.org/wiki/Azimuth
    y = math.cos(point1[0]) * math.tan(point2[0]) - math.sin(point1[0]) * math.cos(deltaLongitude)
    value = math.atan2(x, y)

    # codomain adjustment
    if value < 0:
        value += 2 * math.pi  # 360 + degrees
    return value


def degrees_to_radians(degreesValue):
    """Convert degrees to radians."""
    return degreesValue * math.pi / 180


def radians_to_degrees(radiansValue):
    """Convert radians to degrees."""
    return radiansValue * 180 / math.pi


def compute_bearings(lon, lat):
    """Compute the bearing angle of a segment from (x1, y1) to (x2, y2)."""
    def generator(lon, lat):
        num_rows = lat.size
        yield 0
        for index in range(1, num_rows):
            yield radians_to_degrees(
                calculate_radians_bearing([degrees_to_radians(lat[index - 1]), degrees_to_radians(lon[index - 1])],
                                          [degrees_to_radians(lat[index]), degrees_to_radians(lon[index])]))

    return np.array([x for x in generator(lon, lat)])


def enrich(data, table_name, trajectory_interval=3600, segment_interval=1200, stay_thr=0.28, dbtype='pg'):
    """Enrich segmented data."""
    days = millis_to_day(data[TIME])
    hours = millis_to_hour(data[TIME])

    trajectory_segmented_data = segment_contiguous_time(data[TIME], trajectory_interval)
    trajectory_ids = compute_id(trajectory_segmented_data)

    segment_segmented_data = np.logical_or(segment_contiguous_time(data[TIME], segment_interval),
                                           segment_user(data[SIGNIFICATIVEID]))
    # segment_contiguous_time(data[TIME], segment_interval)
    segment_ids = compute_id(segment_segmented_data)
    segments = get_segments(data, segment_ids)
    segment_indexes = np.concatenate([compute_index(segment) for segment in segments]).ravel()
    repeat_locations = np.concatenate([compute_repeat(segment[LAT], segment[LON]) for segment in segments]).ravel()

    segments_begin = np.concatenate([compute_first(segment) for segment in segments]).ravel()
    segments_end = np.concatenate([compute_last(segment) for segment in segments]).ravel()

    time_diff_segments = [compute_time(segment[TIME]) for segment in segments]
    time_incremental = np.concatenate([sum_over_segment(segment) for segment in time_diff_segments]).ravel()
    time_diff = np.concatenate(np.array(time_diff_segments)).ravel()

    dist_diff_segments = [compute_haversine_distance(segment[LAT], segment[LON]) for segment in segments]
    dist_incremental = np.concatenate([sum_over_segment(segment) for segment in dist_diff_segments]).ravel()
    dist_diff = np.concatenate(np.array(dist_diff_segments)).ravel()

    speed = compute_speed(dist_diff, time_diff)

    # slopes = np.concatenate([compute_slopes(segment[LON], segment[LAT]) for segment in segments]).ravel()
    # still = below_threshold(speed, stay_thr)

    bearings = np.concatenate([compute_bearings(segment[LON], segment[LAT]) for segment in segments]).ravel()

    r = MyData(tableName=table_name, dbtype=dbtype)
    r.add('significativeid', 'integer', data[SIGNIFICATIVEID], pk=True)
    r.add('timest', 'integer', data[TIME], pk=True)
    r.add('trajectoryid', 'integer', trajectory_ids)
    r.add('segmentid', 'integer', segment_ids)
    r.add('pointid', 'integer', segment_indexes)
    r.add('weekday', 'integer', days)
    r.add('dayhour', 'integer', hours)
    r.add('latitude', 'double', data[LAT])
    r.add('longitude', 'double', data[LON])
    r.add('accuracy', 'integer', data[ACCURACY])
    r.add('repetition', 'integer', repeat_locations)
    r.add('segmentbegin', 'integer', segments_begin)
    r.add('segmentend', 'integer', segments_end)
    r.add('distinc', 'double', dist_incremental.round(8))
    r.add('timeinc', 'integer', time_incremental.round(8))
    r.add('distdiff', 'double', dist_diff.round(8))
    r.add('timediff', 'integer', time_diff.round(8))
    r.add('speed', 'double', speed.round(8))
    # r.add('still', 'integer', still)
    # r.add('slope', 'double', slopes)
    r.add('bearing', 'double', bearings)
    r.add('geojson', 'varchar(200)', data['geojson'])
    if dbtype == 'pg':
        r.add('geom', 'point', data[GEOMETRY])  # geometry(lon,lat)
        r.add('geomprevious', 'point', np.concatenate([shift_data(segment[GEOMETRY]) for segment in segments]).ravel())
    elif dbtype == 'oracle':
        r.add('plon', 'double', np.concatenate([shift_data(segment[LON]) for segment in segments]).ravel())
        r.add('plat', 'double', np.concatenate([shift_data(segment[LAT]) for segment in segments]).ravel())
    return r
